<?php
namespace isqr\scms\controllers;

use Yii;
use isqr\scms\components\SController;
use isqr\scms\components\SGlobal;
use isqr\scms\jobs\SEmailJob;
use isqr\scms\models\IUser;
use isqr\scms\models\SPage;
use isqr\scms\models\SUser;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\helpers\Url;

class DefaultController extends SController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    array(
                        'actions' => ['init', 'index', 'maintenance', 'error', 'check75080', 'change-language'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ),
                    array(
                        'actions' => ['test-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ),
                ],
            ]
        ];
    }
        
    public function actionIndex()
    {      
        $page = SPage::find()
        ->andWhere(['t.id'=>SGlobal::getSetting("home_page_id")])
        ->one();
        
        if($page == null){
            $this->view->title = SGlobal::getSetting("title");
            $this->layout = "@scms/views/layouts/home";
            return $this->render($this->getViewName()."-home", array(
            ));
        }
        else{
            $this->view->title = $page->name;
            return $this->render($this->getViewName()."-page", array(
                'page' => $page
            ));
        }
    }

    public function actionInit() {
        $user = SUser::find()
        ->where(['id' => 1])
        ->one();
        if($user == null) {
            $user = new IUser();
            $user->id = 1;
            $user->name = "Admin";
            $user->username = "admin";
            $user->email = "admin@example.com";
            $security = new Security();        
            $user->password_enc = $security->generatePasswordHash("123456");
            $user->force_change_password = 1;
            $user->super_user = 1;
            if($user->save()) {
                echo "Super user created with email 'admin@example.com' and password '123456'";
            }
            else {
                print_r($user->getErrors());
            }
        }
        
    }
    
    public function actionMaintenance() {
    	return $this->render($this->getViewName());
    }
    
    public function actionCheck75080($on = 1)
    {
        if($on == 1)
            Yii::$app->session['checking'] = 1;
        else
            Yii::$app->session['checking'] = null;
        $this->redirect(URL::base(true));
    }

    public function actionTestEmail($on = 1)
    {
        $user = SGlobal::getCurrentUser();
        $from = SGlobal::getSetting("sender_email");

        Yii::$app->queue->push(new SEmailJob([
            'to_email' => $user->email,
            'to_name' => $user->getFullName(),
            'from_email' => SGlobal::getSetting("sender_email"),
            'from_name' => SGlobal::getSetting("sender_name"),
            'subject' => 'Test Email',
            'content' => 'This is a test email.'
        ]));
        echo "Email added to queue.";
    }
    
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        $this->layout = "error";
        
        if ($exception !== null) {
            if(!isset($exception->statusCode)){
                $statusCode = 500;
                $name = "Internal Server Error";
                $message = $exception->getMessage();
            }
            else {
                $statusCode = $exception->statusCode;
                $name = $exception->getName();
                $message = $exception->getMessage();
            }
            
            if(SGlobal::isApiMode()) {
                return parent::json($message, [], parent::JSON_ERROR);
            }
            else {
                return $this->render($this->getViewName(), [
                    'exception' => $exception,
                    'statusCode' => $statusCode,
                    'name' => $name,
                    'message' => $message
                ]);
            }
        }
    }

    public function actionChangeLanguage($code)
    {
        Yii::$app->session->set("language", $code);
        if(Yii::$app->request->referrer == null)
            return $this->redirect(Url::base());
        return $this->redirect(Yii::$app->request->referrer);
    }
}