<div class="box <?=$boxCss?>">
	<?php if(trim($title) !== ""):?>
	<div class="box-header">
		<h3 class="box-title">Filters</h3>
	</div>
	<?php endif;?>
	<form action="" method="GET">
		<div class="box-body">
			<?=$content;?>
			<button type="submit" class="btn btn-sm btn-primary">Apply</button>
			<button type="button" class="btn btn-sm btn-defaultc closeSB">Close</button>
			<button type="button" class="btn btn-sm btn-defaultc closeSBClear">Close & Clear</button>
		</div>
	</form>
<div>