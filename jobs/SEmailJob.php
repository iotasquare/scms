<?php 

namespace isqr\scms\jobs;

use isqr\scms\models\SEmailTemplate;
use Yii;
use yii\base\BaseObject;

class SEmailJob extends BaseObject implements \yii\queue\JobInterface
{
    public $to_name;
    public $to_email;
    public $from_name;
    public $from_email;
    public $subject;
    public $content;

    public function execute($queue)
    {
        $sent = Yii::$app->mailer->compose()
        ->setFrom([$this->from_email => $this->from_name])
        ->setTo([$this->to_email => $this->to_name])
        ->setSubject($this->subject)
        ->setHtmlBody($this->content)
        ->send();
        if($sent){
            echo "Message Sent.\n";
        }
    }
}
?>