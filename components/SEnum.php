<?php
namespace isqr\scms\components;

abstract class SEnum
{
    const SITE_STATUS_MAINTENANCE = 0;
    const SITE_STATUS_LIVE = 1;

    public static $statuses = [
        self::SITE_STATUS_MAINTENANCE => 'Maintenance',
        self::SITE_STATUS_LIVE => 'Live'
    ];
    
    const ASSETTYPE_THEME = "ThemeAsset";
    const ASSETTYPE_PLUGIN = "PluginAsset";
    
    public static $assetType = array(
        self::ASSETTYPE_THEME => 'ThemeAsset',
        self::ASSETTYPE_PLUGIN => 'PluginAsset',
    );
    
    const ASSETVIEW_ADMIN = "AdminAsset";
    const ASSETVIEW_FRONT = "FrontAsset";
    
    public static $assetView = array(
        self::ASSETVIEW_ADMIN => 'Admin',
        self::ASSETVIEW_FRONT => 'Front',
    );
}