<?php use yii\helpers\StringHelper;
use yii\helpers\Url;
$size = isset($size)?$size:12;?>
<div class="col-md-<?=$size?>">
	<div class="box <?=$boxCss?>">
		<div class="box-body">
			<?=$content;?>
		</div>
	</div>
</div>