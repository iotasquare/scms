<?php

use isqr\scms\models\SPage;
use isqr\scms\widgets\jsregister\JSRegister;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

?>
<style>
    #paramtable {
        overflow: auto;
        max-height: 100px;
        padding: 4px;
        border: 1px solid #EEE;
        margin: 10px 0px 20px;
    }

    #paramtable .paramblock {
        display: inline-block;
        width: 24%;
        height: 70px;
        background: #eeeeee;
        padding: 4px;
        vertical-align: top;
        text-align: center;
        margin: 2px;
    }

    #paramtable .paramblock .code{
        margin: 0px;
        font-size: 11px;
        width: 100%;
        padding: 4px;
        display: block;
        background: grey;
        color: white;
        font-weight: bold;
    }

    #paramtable .paramblock .details{
        margin-top: 6px;
        font-size: 12px;
    }
    
</style>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'page-update-form',
        'options' => [
            'method' => 'post',
            "autocomplete" => "off"
        ]
    ]); ?>
    
    <?php $template_codes = Yii::$app->scmsApp->getConfig("email_template_codes"); ?>
    <?php 
        foreach($template_codes as $k=>$v)
            $template_codes[$k]['code'] = $k;
    ?>
    <?= $form->field($model, 'code')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($template_codes, 'code', 'name'), 
        'options' => ['placeholder' => 'Template Code', 'multiple' => false, 'disabled' => $model->code != null],
        'pluginOptions' => [
            'tags' => false
        ],
        'pluginEvents' => [
            'select2:select'=>'function(){
                showEmailTemplateParams($(this).find("option:selected").val());
            }'
        ]
    ]);?>
    <div id="paramtable_container">
        <p class="hint text-info">Please use following codes in the subject and content.</p>
        <div id="paramtable" >
        </div>
    </div>
    <?= $form->field($model, 'subject')->textInput(); ?>
    <?= $form->field($model, 'content')->textarea(['rows' => 6 , 'class'=>'editor']) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'from_email')->textInput(); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'from_name')->textInput(); ?>
        </div>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
    <?= Html::a('Cancel', '#', ['class' => 'btn btn-sm btn-default', 'data-dismiss'=>'modal']); ?>
    <?php ActiveForm::end(); ?>
</div>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
    var template_codes = <?=json_encode($template_codes, true)?>;
    function showEmailTemplateParams(code) {
        var template = template_codes[code];
        var templateparams = template['params'];
        if(templateparams == undefined)
            templateparams = {};
        templateparams['to_email'] = "Email address of the receiver.";
        templateparams['to_name'] = "Name of the receiver.";
        templateparams['from_email'] = "Email address of the sender.";
        templateparams['from_name'] = "Name of the sender.";
        templateparams['signature'] = "Sender Signature.";
        console.log(templateparams);
        $("#paramtable").empty();

        for(var k in templateparams) {
            var html = `<div class="paramblock"><div class="code">{{` + k + `}}</div> <div class="details">` + templateparams[k] + `</div></div>`;
            $("#paramtable").append(html);
        }
        $("#paramtable_container").show();
    }
</script>
<?php JSRegister::end();?>

<?php JSRegister::begin(["position" => View::POS_READY]);?>
<script>
    $("#paramtable_container").hide();
    showEmailTemplateParams('<?=$model->code;?>');
</script>
<?php JSRegister::end();?>