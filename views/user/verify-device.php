<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use isqr\scms\components\SGlobal;
use isqr\scms\components\SEnum;
use yii\base\Event;
?>

<div class="row">
<div class="col-md-6">
<?php
$form = ActiveForm::begin([
    'id' => 'verify-device-form',
    'enableAjaxValidation' => false,
    'options' => [
    ]
])?>
<p>Please check your email. The verification code has been sent to your email address to verify this device.</p>
<?= $form->field($model, 'code')->passwordInput(["placeholder"=>"Verification Code"])->label(true)?>
<p>
<?= Html::submitButton('Continue', ['class' => 'btn  btn-primary'])?> 
</p>
<?php ActiveForm::end()?>
</div>
</div>