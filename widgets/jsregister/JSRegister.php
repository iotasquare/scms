<?php

namespace isqr\scms\widgets\jsregister;

use yii\base\Widget;
use yii\web\View;

/**
 * Widget for registering script from view file
 * 
 * Getting script in between this widget and register it by \yii\web\View::registerJs()
 */
class JSRegister extends Widget {
	//variables to be passed to \yii\base\View::registerScript()
	public $key = null;
	public $position = View::POS_READY;
	public $type = self::TYPE_SCRIPT;
	public $var = "";
	
	const TYPE_SCRIPT = 1;
	const TYPE_HTML = 2;
	
	/**
	 * Start widget by calling ob_start(), caching all output to output buffer
	 * @see \yii\base\Widget::begin()
	 */
	public static function begin($config = []){
		$widget = parent::begin($config);
		
		ob_start();
		
		return $widget;
	}
	
	
	/**
	 * Get script from output buffer, and register by \yii\web\View::registerJs()
	 * @see \yii\base\Widget::end()
	 */
	public static function end(){
		$content = ob_get_clean();
		$widget = parent::end();
		
		if($widget->type == self::TYPE_SCRIPT){
			if(preg_match("/^\\s*\\<script\\>(.*)\\<\\/script\\>\\s*$/s", $content, $matches)){
				$content = $matches[1];
			}
			$widget->view->registerJs($content, $widget->position, $widget->key);
		}
		elseif($widget->type == self::TYPE_HTML){
			$data = json_encode($content);
			$data = "var $widget->var = $data;";
			$widget->view->registerJs($data, $widget->position, $widget->key);
		}
		
	}
}