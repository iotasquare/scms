<?php
namespace isqr\scms\components;

use isqr\scms\components\SGlobal;
use yii\data\ActiveDataProvider;
use isqr\scms\components\SEnum;
use isqr\scms\models\SUser;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

abstract class SActiveRecord extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static $defaultStatuses = array(
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'Inactive'
    );
    
	const SOP_ADD = 1;
    const SOP_DELETE = 2;
    const SOP_UPDATE = 0;

    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_DELETE = 'delete';

    public $filter_status;
    public $filter_created_user_id;
    public $filter_created_time;
    public $filter_last_updated_user_id;
    public $filter_last_updated_time;
    public $filter_deleted;
    public $filter_deleted_user_id;
    public $filter_deleted_time;
    
    private $_configuration = [
        'rules' => [],
        'attributeLabels' => [],
        'hints' => []
    ];

    public function init()
    {
        if (method_exists($this, 'configure'))
            $this->_configuration = $this->configure();
        parent::init();
    }

    public function addConfiguration($configuration) {
        $this->_configuration = ArrayHelper::merge($this->_configuration, $configuration);
    }

    public function configure() {
        $rules =  [];
        if($this->hasAttribute("status")) {
            $rules[] = [['status'], 'integer'];
            $rules[] = [['status'], 'default', 'value' => 1];
            $rules[] = [['filter_status'], 'safe'];
        }
        if($this->hasAttribute("created_time")) {
            $rules[] = [['created_time'], 'safe'];
            $rules[] = [['filter_created_time'], 'safe'];
        }
        if($this->hasAttribute("last_updated_time")) {
            $rules[] = [['last_updated_time'], 'safe'];
            $rules[] = [['filter_last_updated_time'], 'safe'];
        }
        if($this->hasAttribute("deleted_time")) {
            $rules[] = [['deleted_time'], 'safe'];
            $rules[] = [['filter_deleted_time'], 'safe'];
        }
        if($this->hasAttribute("created_user_id")) {
            $rules[] = [['created_user_id'], 'integer'];
            $rules[] = [['created_user_id'], 'default', 'value' => 0];
            $rules[] = [['filter_created_user_id'], 'safe'];
        }
        if($this->hasAttribute("last_updated_user_id")) {
            $rules[] = [['last_updated_user_id'], 'integer'];
            $rules[] = [['last_updated_user_id'], 'default', 'value' => 0];
            $rules[] = [['filter_last_updated_user_id'], 'safe'];
        }
        if($this->hasAttribute("deleted")) {
            $rules[] = [['deleted'], 'integer'];
            $rules[] = [['deleted'], 'default', 'value' => 0];
            $rules[] = [['filter_deleted'], 'safe'];
        }
        if($this->hasAttribute("deleted_user_id")) {
            $rules[] = [['deleted_user_id'], 'safe'];
            $rules[] = [['deleted_user_id'], 'default', 'value' => 0];
            $rules[] = [['filter_deleted_user_id'], 'safe'];
        }
        
    	if($this->hasAttribute("site_id"))
            $rules[] = [['site_id'], 'integer'];

        $attributeLabels =  [
            'last_updated_time' => 'Last Updated Time',
            'created_time' => 'Created Time',
            'created_user_id' => 'Created By',
            'status' => 'Status',
            'filter_last_updated_user_id' => 'Updated By',
            'filter_last_updated_time' => 'Last Updated Time',
            'filter_created_time' => 'Created Time',
            'filter_created_user_id' => 'Created By',
            'filter_deleted' => 'Deleted',
            'filter_deleted_user_id' => 'Deleted By',
            'filter_deleted_time' => 'Deleted Time',
            'filter_status' => 'Status'
        ];
        
        $hints =  [
        ];

        return [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ];
    }

    public function rules()
    {
        return $this->_configuration['rules'];
    }
    
    public function attributeLabels()
    {
        return $this->_configuration['attributeLabels'];
    }
    
    public function hints()
    {
        return $this->_configuration['hints'];
    }  

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
    	parent::afterSave($insert, $changedAttributes);
    }
    
	public function beforeValidate()
	{
        if($this->isNewRecord && $this->hasAttribute("created_user_id")) {
			if(!\Yii::$app->request->isConsoleRequest) {
				$user = SGlobal::getCurrentUser();
				if($user != null)
					$this->created_user_id = SGlobal::getCurrentUser()->id;
				else
					$this->created_user_id = 0;
			}
            else 
                $this->created_user_id = 0;
        }

		if($this->isNewRecord && $this->hasAttribute("created_time"))
        	$this->created_time = date("Y-m-d H:i:s");
		
        if($this->hasAttribute("last_updated_time"))
            $this->last_updated_time = date("Y-m-d H:i:s");

        if($this->hasAttribute("last_updated_user_id")){
            if(!\Yii::$app->request->isConsoleRequest) {
                $user = SGlobal::getCurrentUser();
                if($user != null)
                    $this->last_updated_user_id = SGlobal::getCurrentUser()->id;
                else
                    $this->last_updated_user_id = 0;
            }
            else 
                $this->last_updated_user_id = 0;
        }
            
	    return parent::beforeValidate();
    }
    
    public function delete()
    {
        if($this->hasAttribute("deleted")) {
            if (!$this->beforeDelete()) {
                return false;
            }
            $this->deleted =  1;
            if($this->hasAttribute("deleted_time"))
                $this->deleted_time = date("Y-m-d H:i:s");

            if($this->hasAttribute("deleted_user_id")){
				if(!\Yii::$app->request->isConsoleRequest) {
					$user = SGlobal::getCurrentUser();
					if($user != null)
						$this->deleted_user_id = SGlobal::getCurrentUser()->id;
					else
						$this->deleted_user_id = 0;
				} 
				else
					$this->deleted_user_id = 0;
            }
            if(!$this->update()) {
                return false;
            }
            $this->afterDelete();
            return true;
        }
        else {
            return parent::delete();
        }
    }
    
	public static function find()
    {
        $query = parent::find()
        ->from(static::tableName() . ' t');
        
        $columns = self::getTableSchema()->columns;
        
		if(isset($columns['deleted'])) {
        	$query->andFilterWhere(['=','t.deleted',0]);
        }
        return $query;
    }

    public function scenarios() {

        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_INSERT] = $scenarios[self::SCENARIO_DEFAULT];
        $scenarios[self::SCENARIO_UPDATE] = $scenarios[self::SCENARIO_DEFAULT];
        $scenarios[self::SCENARIO_DELETE] = ['id'];
	    return $scenarios;
    }

    public function getStatus()
    {
        return self::$defaultStatuses[$this->status];
    }

    public function getCreatedByUser()
    {
		return $this->hasOne(SUser::className(), ['id' => 'created_user_id']);;
    }

    public function getLastUpdatedByUser()
    {
		return $this->hasOne(SUser::className(), ['id' => 'last_updated_user_id']);;
    }
    
	protected function searchQuery(){
        $query = self::find();
        	
        $query->addSelect("t.*");
        
        $idKey = isset($this->_configuration['primary_key']) ? $this->_configuration['primary_key'] : "id";

        $query->andFilterWhere([
            't.'.$idKey => $this->{$idKey},
        ]);
        
        if($this->hasAttribute("last_updated_time"))
            $query->andFilterWhere(['like', 't.last_updated_time', $this->filter_created_time]);
            
        if($this->hasAttribute("created_time"))
        	$query->andFilterWhere(['like', 't.created_time', $this->filter_created_time]);
        
        if($this->hasAttribute("status"))
            $query->andFilterWhere(['like', 't.status', $this->filter_status]);
            
        if($this->hasAttribute("created_user_id"))
            $query->andFilterWhere(['=', 't.created_user_id', $this->filter_created_user_id]);
        
        if($this->hasAttribute("deleted"))
            $query->andFilterWhere(['=', 't.deleted', 0]);
        
		return $query;
	}
	
	public function search($params)
    {
        $this->load($params);
		$query = $this->searchQuery();
        if(isset($this->featured))
            $query->andFilterWhere(['like', 't.featured', $this->featured]);
            
        $arr = ['query' => $query];
        if($this->hasAttribute('sort')) {
            $arr['sort'] = ['defaultOrder' => ['sort'=>SORT_ASC]];
        }
        $dataProvider = new ActiveDataProvider($arr);
        return $dataProvider;
    }
    
	public function prepareRelatedData($fieldname, $newDataArr, $refKey = 'id'){
	    if(!is_array($newDataArr)) $newDataArr = [];
	    $old = [];
	    $new = [];
	    $union = [];
	    foreach ($this->{$fieldname} as $k=>$v) {
            $old[$v->{$refKey}] = $v;
        }
        
        $tmpC = 1;
	    foreach ($newDataArr as $k=>$v) {
            if(is_array($v)) {
                if(isset($v[$refKey]) && $v[$refKey] != "")
                    $new[$v[$refKey]] = $v;
                else {
                    $new["t".($tmpC++)] = $v;
                }
            }
            else {
                if(trim($v) == "") continue;
                $new[$v] = $v;   
            }
        }

        /*
            1. If new & old have same ids exists, update it.
            2. If new has entry id but old don't have, add it.
            3. If old has entry id but new have not, delete it.
        */
        foreach ($newDataArr as $k=>$v) {
            if(!is_array($v)) {
                if(trim($v) == "") continue;
            }
            if(isset($v[$refKey]) && isset($old[$v[$refKey]])) { // Case 1
                $union[] = [
                    'action' => self::SOP_UPDATE,
                    'data' => $v
                ];
            }
            elseif(!isset($v[$refKey]) || !isset($old[$v[$refKey]])) { // Case 2
                $union[] = [
                    'action' => self::SOP_ADD,
                    'data' => $v
                ];
            }
        }

        //\var_dump($newDataArr); \var_dump($old); \var_dump($new); 
        foreach ($old as $k=>$v) {
            if(!isset($new[$v[$refKey]])) { // Case 3
                $union[] = [
                    'action' => self::SOP_DELETE,
                    'data' => $v
                ];
            }
        }
        
        return $union;
    }
    

    public function getTableName(){
        return static::tableName();
    }
}