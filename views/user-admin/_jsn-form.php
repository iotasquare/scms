<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use isqr\scms\models\SUserGroup;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SGlobal;
use isqr\scms\models\SUser;
use kartik\select2\Select2;
?>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'user-update-form',
        'options' => [
            'method' => 'post',
            "autocomplete" => "off"
        ]
    ]); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'surname')->textInput(); ?>
        </div>
    </div>

    <?= $form->field($model, 'gender')->inline()->radioList(SUser::$genders);?>
    <?php $groups = SUserGroup::find()
        ->andWhere(['status' => SUserGroup::STATUS_ACTIVE])
        ->all();
    ?>

    <fieldset>
        <legend>Contact Information</legend>
    <?= $form->field($model, 'address')->textarea(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'city')->textInput(); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'country')->textInput(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'phone_number')->textInput(); ?>
        </div>
    </div>
    </fieldset>
    
    <fieldset>
        <legend>Access Information</legend>
        
        <?php if(SGlobal::isAdminUser()):?>
            <?php if(SGlobal::getCurrentUser()->id != $model->id):?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'groups')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($groups,'id','name'),
                        'options' => ['placeholder' => 'Groups', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                        ]
                    ]);?>
                </div>
                <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList(SUser::$defaultStatuses); ?>
                </div>
            </div>
            <?= $form->field($model, 'super_user')->checkbox(); ?>
            <?php else:?>
                <p class="hint"><strong>Note:</strong> You cannot change your own role & status.</p>
            <?php endif;?>
        <?php endif;?>
    </fieldset>

    <fieldset>
        <legend>Credentials</legend>
        <?= $form->field($model, 'email')->textInput(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'password')->passwordInput()->hint(!$model->isNewRecord?"Leave the password empty if you don't need to change password.":""); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'confirm_password')->passwordInput()->hint(""); ?>
            </div>
        </div>
    </fieldset>

    <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
    <?= Html::a('Cancel', '#', ['class' => 'btn btn-sm btn-default', 'data-dismiss'=>'modal']); ?>
    <?php ActiveForm::end(); ?>
</div>