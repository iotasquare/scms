<?php
use yii\helpers\Html;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?= $this->head() ?>
</head>
<body class="<?=isset($scope)? $scope: ""; ?>">
<?php $this->beginBody() ?>
	<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>