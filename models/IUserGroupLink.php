<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_users_groups_link".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $user_group_id
 */
class IUserGroupLink extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_users_groups_link';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['user_id', 'user_group_id'], 'required'],
            [['user_id', 'user_group_id'], 'integer'],
            [['user_id', 'user_group_id'], 'unique', 'targetAttribute' => ['user_id', 'user_group_id'], 'message' => 'The combination of User ID and User Group ID has already been taken.'],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_group_id' => 'User Group ID',
        ];
        $hints = [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_group_id' => 'User Group ID',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }
    
}
