<?php $this->beginContent("@scms/views/layouts/main.php"); ?>
	<?= $this->render('@scms/views/layouts/childhtml/header');?>    
    <div class="container mt-4 mb-4">
        <div class="row">
            <div class="col-12">
                <div><?= $content;?></div>
            </div>
        </div>
    </div>
    <?= $this->render('@scms/views/layouts/childhtml/footer');?>
<?php $this->endContent(); ?>