
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use isqr\scms\components\SGlobal;
?>
<div class="row">
<div class="col-md-6">
	<?php if(Yii::$app->session->hasFlash('submit')): ?>
		<?php echo Yii::$app->session->getFlash('submit'); ?><br/><br/>
		<?php echo Html::a('Ok', Url::base(true), array('class'=>'btn btn-primary')); ?>
	<?php else: ?>
		<?php
		$form = ActiveForm::begin([
		    'id' => 'resetpassword-form',
		    'enableAjaxValidation' => false,
		    'options' => [
		        'class' => ''
		    ]
		])?>
	    <?= $form->field($model, 'password')->passwordInput();?>
	    <?= $form->field($model, 'confirmpassword')->passwordInput();?>
        <?= $form->field($model, 'email')->hiddenInput(); ?>
		<?= $form->field($model, 'activate_key')->hiddenInput(); ?>
	    <?= Html::submitButton('Change', ['class' => 'btn btn-primary'])?>
		<?php ActiveForm::end()?>
	<?php endif; ?>
</div>
</div>