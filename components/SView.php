<?php
namespace isqr\scms\components;

use yii\helpers\Url;
use yii\bootstrap\Html;

class SView extends \yii\web\View
{
    public $pageTitle='';
    
    public function getParam($name){
        if(isset($this->params[$name]))
            return $this->params[$name];
        else return "";
    }
    
    public function setParam($name, $value){
        $this->params[$name] = $value;
    }
    
    public function setMetaTag($name, $content, $type="name", $override = true){
        if(trim($content)  =="") return;
        $this->registerMetaTag([
		    $type => $name,
		    "content" => $content,
		], $name);
    }
    
	public function unsetMetaTag($name){
        unset($this->metaTags[$name]);
    }

    private $_buttons = [];

    protected function renderActionButtons() {
		foreach($this->_buttons as $button){
            echo $button . " ";
        }
    }

    protected function addActionButton($title, $route, $faIcon = false) {
        $this->_buttons[] = SGlobal::getActionButton($title, $route, $faIcon);
    }

    protected function addActionPopupButton($title, $route, $popupId, $popupTitle, $onSuccess, $faIcon = false) {
		$this->_buttons[] = SGlobal::getActionPopupButton($title, $route, $popupId, $popupTitle, $onSuccess, $faIcon);
    }

    protected function addActionCustomButton($content) {
		$this->_buttons[] = $content;
    }

}

?>