<?php

use isqr\scms\components\SEnum;
use isqr\scms\components\SGlobal;
use isqr\scms\models\SPage;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use isqr\scms\widgets\adminui\box\Box;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Box::begin(['size'=>12, 'title'=>'Website'])?>
	<div class="row">
        <div class="col-md-8">
        <?= $form->field($model, 'title')->textInput() ?> 
        </div>
        <div class="col-md-4">
        <?= $form->field($model, 'status')->dropDownList(SEnum::$statuses); ?>
        </div>
    </div>
    
    <?= $form->field($model, 'tagline')->textInput() ?>
    
    <?= $form->field($model, 'description')->textarea() ?>
            
    <?php $pages = SPage::find()->all();?>
    <?= $form->field($model, 'home_page_id')->dropDownList(ArrayHelper::map($pages,'id','name'), ['prompt'=>'Default Home', 'defaultValue'=>0]);
    ?>                    
    <div class="row">
        <div class="col-md-4">
			<?= $form->field($model, 'logo')->textInput([
			    'class'=>'fileselector form-control', 
			    'data-image-field'=>'',
				'data-text-field' => strtolower(StringHelper::basename($model->className()))."-image",
			]) ?>
		</div>
		<div class="col-md-4">
		<?= $form->field($model, 'logo_inverted')->textInput([
		    'class'=>'fileselector form-control', 
		    'data-image-field'=>'',
			'data-text-field' => strtolower(StringHelper::basename($model->className()))."-image",
		]) ?>
		</div>
		<div class="col-md-4">
		<?= $form->field($model, 'favicon')->textInput([
		    'class'=>'fileselector form-control', 
		    'data-image-field'=>'test',
		    'data-text-field' => strtolower(StringHelper::basename($model->className()))."-image"
		]) ?>
		</div>
	</div>
<?php Box::end();?>

<?php Box::begin(['size'=>12, 'title'=>'Contact Details'])?>
	<div class="row">
        <div class="col-md-4">
        	<?= $form->field($model, 'company_name')->textInput() ?>
        </div>
        <div class="col-md-4">
        	<?= $form->field($model, 'company_email')->textInput() ?>
        </div>
        <div class="col-md-4">
        	<?= $form->field($model, 'company_phone_number')->textInput() ?>
        </div>
    </div>
	<?= $form->field($model, 'company_address')->textarea(["row"=>3]) ?> 
<?php Box::end();?>

<?php Box::begin(['size'=>12, 'title'=>'Mail Setting'])?>
	<div class="row">
        <div class="col-md-6">
        	<?= $form->field($model, 'sender_name')->textInput() ?>
        </div>
        <div class="col-md-6">
        	<?= $form->field($model, 'sender_email')->textInput() ?>
        </div>
        <div class="col-md-12">
        	<?= $form->field($model, 'sender_signature')->textarea(["row"=>2, 'class'=>"editor"]) ?> 
        </div>
    </div>
	
<?php Box::end();?>

<?php Box::begin(['size'=>12, 'title'=>'Search Engine Optimization'])?>
		<?= $form->field($model, 'analytic_code')->textarea(["rows"=>5]) ?>
		<?= $form->field($model, 'meta_description')->textarea(["rows"=>2]) ?>
		<?= $form->field($model, 'meta_keywords')->textInput(); ?>
		<?= $form->field($model, 'meta_image')->textInput([
		    'class'=>'fileselector form-control', 
		    'data-image-field'=>'test',
		    'data-text-field' => strtolower(StringHelper::basename($model->className()))."-image"    			    
		]) ?>
<?php Box::end();?>

<?php Box::begin(['size'=>12, 'title'=>'Timezone'])?>
	<?php $timezones = SGlobal::getTimezoneList();?>
    <?= $form->field($model, 'timezone')->dropDownList($timezones);?>
<?php Box::end();?>

<?php Box::begin(['size'=>12, 'title'=>'User Sessions'])?>
	<div class="row">
        <div class="col-md-6">
    		<?= $form->field($model, 'autologin_enable')->dropDownList(array("1" => "Yes", "0" => "No"))->hint("User will be auto login if he reopen the browser and access the site.");?>
    	</div>
    	<div class="col-md-6">
    		<?= $form->field($model, 'autologin_cookieduration')->input("number")->hint("Time in minutes.");?>
    	</div>
    </div>
<?php Box::end();?>
<?php /*
<?php Box::begin(['size'=>12, 'title'=>'Theme'])?>
<?php $themes = SGlobal::getThemes();?>
<?php $themeid = SGlobal::getSetting("theme", "basic");?>
<?php $theme = $themes[$themeid];?>
<div class="row">
	<div class="col-md-4">
			<img alt="" src="<?=$theme['preview_default']?>" class="img-responsive thumbnail"/>
		</div>
		<div class="col-md-8">
			<h1><?=$theme['name'];?></h1>
			<p><?=$theme['description'];?></p>
		</div>
		<div class="col-md-12">
			<?= Html::button('<i class="fa fa-th"></i> Select Other Theme', ['class' => 'btn btn-warning btn-sm', 'onclick'=>'openThemePopup();']); ?>
		</div>
	</div>
	<?php foreach($themes as $k=>$v):?>
		<div class="col-md-6">
			<div class="thumbnail">
				<img alt="" src="<?=$v['preview_default']?>" class="img-responsive "/>
				<div class="themename"><?=$v['name'];?></div>
				<a href="<?= Url::toRoute(['/scms/theme-admin/index','change'=>$v['id']]);?>" class="btn btn-block btn-primary btn-sm">Select</a>
			</div>
		</div>
	<?php endforeach;?>
<?php Box::end();?>
*/?>
<?php /*
<?php Box::begin(['size'=>12, 'title'=>'First Login Conformity Dialog'])?>
	<?= $form->field($model, 'conformity_message')->textarea(["row"=>2, 'class'=>"editor"])->label('Message'); ?>
<?php Box::end();?>
*/?>