<?php

namespace isqr\scms;

use yii\web\AssetBundle;
use isqr\scms\components\SGlobal;
use isqr\scms\components\SEnum;

abstract class SAsset extends AssetBundle
{
    public abstract function assetType();
    
    public $depends = [
    		
    ];
    
    public function init()
    {
        $path=null;
        $assetType = $this->assetType();
        if($assetType == SEnum::ASSETTYPE_PLUGIN)
            $path = \Yii::getAlias("@scms/plugin");
        elseif($assetType == SEnum::ASSETTYPE_THEME)
            $path = \Yii::getAlias("@scms/theme"); 
        
        $this->sourcePath = $path . '/assets';        
        if(!is_dir($this->sourcePath))
            mkdir($this->sourcePath, 0775, true);
        parent::init();
    }
}