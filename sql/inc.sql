ALTER TABLE `cms_user_devices` ADD COLUMN `auth_code` varchar(100) DEFAULT NULL AFTER _h;

CREATE UNIQUE INDEX user_id
ON cms_users_groups_link (`user_id`,`user_group_id`);

ALTER TABLE `cms_user_devices` ADD COLUMN `last_ping_time` datetime DEFAULT NULL AFTER last_login_time;