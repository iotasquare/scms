<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_email_templates".
 *
 * @property string $id
 * @property string $code
 * @property string $from_email
 * @property string $from_name
 * @property string $subject
 * @property string $content
 */
class IEmailTemplate extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_email_templates';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['code', 'subject', 'content'], 'required'],
            [['content'], 'string'],
            [['code'], 'string', 'max' => 50],
            [['from_email', 'from_name'], 'string', 'max' => 100],
            [['subject'], 'string', 'max' => 200],
            [['code'], 'unique'],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'code' => 'Code',
            'from_email' => 'From Email',
            'from_name' => 'From Name',
            'subject' => 'Subject',
            'content' => 'Content',
        ];
        $hints = [
            'id' => 'ID',
            'code' => 'Code',
            'from_email' => 'From Email',
            'from_name' => 'From Name',
            'subject' => 'Subject',
            'content' => 'Content',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
    }
    
}
