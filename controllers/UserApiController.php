<?php
namespace isqr\scms\controllers;

use Yii;
use isqr\scms\models\SUser;
use isqr\scms\components\SGlobal;
use isqr\scms\components\SEnum;
use isqr\scms\models\SUserDevice;
use isqr\scms\models\SLoginForm;
use  isqr\scms\components\SApiController;
use isqr\scms\jobs\SEmailJob;
use isqr\scms\models\SEmailTemplate;
use yii\authclient\OAuthToken;
use isqr\scms\models\SNotification;
use yii\helpers\ArrayHelper;

class UserApiController extends  SApiController
{
	public function behaviors()
    {
        $arr = [
            'access' => [
                'rules' => [	
					array(
						'actions' => ['login', 'verify', 'get-access-key', 'register', 'recovery','change-password', 'validate-email'],
						'allow' => true,
                        'roles' => ['?', '@'],
					),
                ],
            ]
		];
		
        return ArrayHelper::merge(parent::behaviors(), $arr);
    }
	
	public function actionRegister()
	{
		$name = \Yii::$app->request->post("name", "");
		$surname = \Yii::$app->request->post("surname", "");
		$email = \Yii::$app->request->post("email", "");
		$password = \Yii::$app->request->post("password", "");

		if (!$this->validateEmail($email)) {
			return $this->json("Email is not valid please write valid Email.", [], parent::JSON_ERROR);
		}
		if (strlen($password) < 6) {
			return $this->json("Password must contain alteast 6 characters.", [], parent::JSON_ERROR);
		}

		$user = SUser::find()->where(['email' => $email])->one();

		if ($user != null && $user->deleted != "1")
			return $this->json("A user with this email is already registered.", [], parent::JSON_ERROR);

		if ($user == null)
			$user = new SUser();

		$user->name = $name;
		$user->surname = $surname;
		$user->email = $email;
		$user->setPassword($password);
		if (SGlobal::getAppParam("userAutoApproveOnSignup") === true)
			$user->status = SUser::USER_STATUS_ACTIVE;
		else
			$user->status = SUser::USER_STATUS_INACTIVE;
		if ($user->save())
			return $this->json("User is successfully registered.", [], parent::JSON_SUCCESS);

		return $this->json("Unable to register user.", $user->getErrors(), parent::JSON_ERROR);
	}

	public function actionLogin()
	{
		$udid = \Yii::$app->request->post("udid", "Unknown");
		$_d = \Yii::$app->request->post("_d", "");
		$email = \Yii::$app->request->post("email", "");
		$password = \Yii::$app->request->post("password", "");
		$source = \Yii::$app->request->post("source", "");
		$access_token = \Yii::$app->request->post("access_token", "");
		$user = null;

		if (trim($source) == "") {
			$model = new SLoginForm();
			$model->email = $email;
			$model->password = $password;
			$user = $model->isValid();
			if ($user === false) {
				$errors = $model->getErrors();
				return $this->json($errors['password'][0], [], parent::JSON_ERROR);
			}
		} else {
			try {
				$client = Yii::$app->authClientCollection->getClient($source);
				$token = new OAuthToken();
				$token->setToken($access_token);
				$client->setAccessToken($token);
				$user = SUser::getUserUsingAuth($client);
			} catch (\Exception $ex) {
				return $this->json($ex->getMessage(), [], parent::JSON_ERROR);
			}
		}

		$device = SUserDevice::find()->andWhere(['id' => $_d])->one();
		$first_login = false;
		if ($device == null) {
			$first_login = true;
			$prev_devices = SUserDevice::find()->andWhere(['user_id' => $user->id])->count();
			if($prev_devices > 0){
				$first_login = false;
			}

			$device = new SUserDevice();
			$device->id = SGlobal::createUniqueId();
			$device->name = $udid;
			$device->created_time = date("Y-m-d H:i:s");
			$device->ip = "";
			$device->pin = SGlobal::generatePIN(6);
			$device->user_id = $user->id;
			$device->active = 1;
			// $device->remember_me = 1;
		}
		$device->_h = \Yii::$app->security->generatePasswordHash($device->id . time() . "scmsx" . rand(111, 999));
		if ($device->save()) {
			return $this->json('Success', [
				'_d' => $device->id,
				'_h' => $device->_h,
				'user_id' => $user->id,
				'email' => $user->email,
				'name' => $user->getFullName(),
				'firstname' => $user->name,
				'lastname' => $user->surname,
				'first_login'=> $first_login,
				'super_user' => $user->super_user
			], parent::JSON_SUCCESS);
		} else {
			return $this->json('Authentication Failed.', $device->getErrors(), parent::JSON_ERROR);
		}
	}

	public function actionGetAccessKey()
	{
		$code = \Yii::$app->request->post("code", "");
		$device = SUserDevice::find()
		->andWhere(['auth_code' => $code])
		->andWhere(['IS NOT', 'auth_code', null])
		->one();
		
		if ($device == null) {
			return $this->json('Auth Code is not valid.', [], parent::JSON_ERROR);
		}

		if (!$device->verifyAuthCode()) {
			return $this->json('Auth Code is expired.', [], parent::JSON_ERROR);
		}

		$user = SUser::findOne($device->user_id);

		return $this->json('Auth Code is valid.', [
			'_d' => $device->id,
			'_h' => $device->_h
		], parent::JSON_SUCCESS);
	}

	public function actionVerify()
	{
		$user = SGlobal::getCurrentUser();
		if ($user == null) {
			return $this->json('Access key is not valid.', [], parent::JSON_ERROR);
		}
		return $this->json('Access key is valid.', [
			'id' => $user->id,
			'user_id' => $user->id,
			'name' => $user->getFullName(),
			'email' => $user->email,
			'firstname' => $user->name,
			'lastname' => $user->surname
		], parent::JSON_SUCCESS);
	}

	public function actionRecovery()
	{
		$email = \Yii::$app->request->post("email", "");
		if ($this->validateEmail($email)) {
			$user = SUser::find()->andWhere(['email' => $email])->one();
			if ($user == null)
				return $this->json("Any user with this email is not registered.", [], parent::JSON_ERROR);

			$pin = $user->generateResetPin();
			if ($user->save()) {
				
				$subject = "Reset your password";
				$body = 'You have requested a new Password. To set your new Password, please enter this pin with new password. <br/> Pin: {{pin}} <br/> {{signature}}';
			
				$template = SEmailTemplate::get("user_password_reset_pin", [
					'to_email' => $user->email,
					'to_name' => $user->getFullName(),
					'subject' => $subject,
					'content' => $body,
					'pin' => $pin,
				]);
				Yii::$app->queue->push(new SEmailJob([
					'to_email' => $user->email,
					'to_name' => $user->getFullName(),
					'from_email' => $template->from_email,
					'from_name' => $template->from_name,
					'subject' => $template->subject,
					'content' => $template->content,
				]));

				return $this->json("Email containing the instructions to change password is sent.", [], parent::JSON_SUCCESS);
			} else {
				return $this->json("Unable to save pin code.", $user->getErrors(), parent::JSON_ERROR);
			}
		}
		return $this->json("Email is not valid please write valid Email", [], parent::JSON_ERROR);
	}

	public function actionChangePassword()
	{
		$user = SGlobal::getCurrentUser();
		if ($user == null) {
			$email = \Yii::$app->request->post("email", "");
			$pin = \Yii::$app->request->post("pin", "");
			$user = SUser::find()
				->andWhere(['email' => $email])
				->andWhere(['activation_key' => $pin])
				->one();
			if ($user == null)
				return $this->json("Pin code is not valid.", [], parent::JSON_ERROR);
		}

		$password = \Yii::$app->request->post("password", "");
		if (trim($password) != "") {
			$user->setPassword($password);
			$user->removeActivationToken();
			if ($user->save())
				return $this->json("Password updated.", [], parent::JSON_SUCCESS);
		}

		return $this->json("Unable to update password.", $user->getErrors(), parent::JSON_ERROR);
	}


	public function validateEmail($email)
	{
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
}
