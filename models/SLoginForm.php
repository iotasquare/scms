<?php
namespace isqr\scms\models;
use yii;
use isqr\scms\models\SUser;
use isqr\scms\components\SFormModel;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for Login Form.
 *
 * @property SUser $_identity
 */
class SLoginForm extends SFormModel
{
	public $email; 
	public $password;
	public $rememberMe;
	private  $_identity;

	public function configure() {
        $rules =  [
			[['email', 'password'], 'required', 'message' => '{attribute} is required.'],
            ['email', 'string', 'max' => 255],
            ['password', 'string', 'max' => 128],
            ['rememberMe', 'number', 'integerOnly' => true]
        ];

        $attributeLabels =  [
			'email' => Yii::t('app', 'Email'),
			'password' => Yii::t('app', 'Password'),
			'rememberMe' => Yii::t('app', 'Remember me')
        ];
        
        $hints =  [
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
	}

	public function login($forceLogin = false)
	{
	    $user = SUser::findByEmail($this->email);
	    
	    if($user != null){
			if($user->status != SUser::USER_STATUS_ACTIVE){
				$this->addError('password','Your account is not active. Please contact to helpdesk.');
				return false;
			}
    	    if($forceLogin || $user->validatePassword($this->password))
    	    {
    	    	if(trim($user->auth_key) == "") {
    	    		$user->auth_key = \Yii::$app->security->generateRandomString(50);
    	    		$user->save();
    	    	}
				$user->login();
    			return $user;
    		}
	    }
		$this->addError('password','Please enter a valid email and password.');
		return false;
	}

	public function isValid()
	{
	    $user = SUser::findByEmail($this->email);
	    if($user != null){
			if($user->status != SUser::USER_STATUS_ACTIVE){
				$this->addError('password','Your account is not active. Please contact to helpdesk.');
				return false;
			}
    	    if($user->validatePassword($this->password))
    	    {
    			return $user;
    		}
	    }
		$this->addError('password','Please enter a valid email and password.');
		return false;
	}
}