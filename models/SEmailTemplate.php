<?php
namespace isqr\scms\models;

use isqr\scms\components\SGlobal;
use Yii;
use yii\helpers\ArrayHelper;

class SEmailTemplate extends IEmailTemplate
{
    public function configure()
    {
        $rules = [
        ];
        $attributeLabels = [
            'code' => 'Template Code'
        ];
        $hints = [
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
    }
    
    /**
     * 
     * SEmailTemplate::apply("news", [
     *  'to_name' => 'Example User',
     *  'to_email' => 'example@isqr.io',
     *  'title' => 'Test News',
     *  'content' => 'This is just a news.',
     * ]);
     */
    public static function get($code, $params = []) 
    {
		$template = SEmailTemplate::find()
        ->andWhere(['code' => $code])
		->one();
        if ($template != null)
            $template->content = $template->content;
        else {
            $template = new SEmailTemplate();
            $template->content = '{{content}}';
            $template->subject = '{{subject}}';
        }
        if(trim($template->from_email) == "") {
            $template->from_name = SGlobal::getSetting("sender_name", "Admin");
            $template->from_email = SGlobal::getSetting("sender_email", "admin@example.com");
        }
        
        $params['from_name'] = $template->from_name;
        $params['from_email'] = $template->from_email;
        $params['signature'] = SGlobal::getSetting("sender_signature", "");
        
        if(isset($params['subject']))
            $template->subject = str_ireplace("{{subject}}", $params['subject'], $template->subject); //setting subject first so, if content is like template, it can processed later.

        if(isset($params['content']))
            $template->content = str_ireplace("{{content}}", $params['content'], $template->content); //setting content first so, if content is like template, it can processed later.

        foreach($params as $k=>$v) {
            $template->subject = str_ireplace("{{".$k."}}", $v, $template->subject);
            $template->content = str_ireplace("{{".$k."}}", $v, $template->content);
        }
        return $template;
	}
}
