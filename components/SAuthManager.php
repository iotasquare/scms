<?php
namespace isqr\scms\components;

use Yii;
use yii\base\Component;
use yii\rbac\CheckAccessInterface;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

class SAuthManager extends Component implements CheckAccessInterface
{
    private static $_permissions = [];
    private static $_restrictedAccess = null;

    private static function loadPermissions($user) {
        if(isset(self::$_permissions[$user->id]))
            return self::$_permissions[$user->id];
        else 
            self::$_permissions[$user->id] = [];

        if($user->super_user == 1) { // Allowing everything for Super Admin User which has id = 1
            return self::$_permissions[$user->id]['*'] = 0;
        }
        $allPermissions = [];
        foreach($user->groups as $group) {
            $permissions = ArrayHelper::getColumn($group->permissions, "action");
            $allPermissions = ArrayHelper::merge($permissions, $allPermissions);
        }
        return self::$_permissions[$user->id] = \array_flip($allPermissions);
    }

    private static function loadrestrictedAccess() {
        if(self::$_restrictedAccess != null)
            return self::$_restrictedAccess;
        $restrictedAccess = Yii::$app->scmsApp->getConfig("restricted_access");
        $tmp = [];
        foreach($restrictedAccess as $k=>$v) {
            $perms = [];
            if(isset($v['permissions'])) {
                foreach($v['permissions'] as $pk=>$pv) {
                    $perms[$pk] = $pv ." in " . $v['resource'];
                }
                $tmp = ArrayHelper::merge($tmp, $perms);
            }
        }
        return self::$_restrictedAccess = $tmp;
    }

    public function init()
    {
        parent::init();
        self::loadrestrictedAccess();
        $user = SGlobal::getCurrentUser();
        if($user != null)
            self::loadPermissions($user);
        //\var_dump(self::$_restrictedAccess);
        //\var_dump(self::$_permissions);
    }
    
    /*
        1. Restricted Role can be allowed via Admin Panel only.
        2. Other wild card roles are disallowed by default.
        3. Other non wild card roles are allowed by default.
        4. If user is not allowed scms::DefaultAdmin::*, he will not be allowed any Admin function.
    
    */
    public function checkAccess($userId, $permissionName, $params = [])
    {
        if(!isset(self::$_permissions[$userId])) {
            return false;
        }

        $allAllowed = isset(self::$_permissions[$userId]['*']);
        if($allAllowed) return true;

        $adminAccessAllowed = isset(self::$_permissions[$userId]['scms::DefaultAdmin::*']);
        $isAdminAction = stripos($permissionName, "Admin::") !== false;
        if($isAdminAction && !$adminAccessAllowed) // Comply Point 4
            return false;

        $allowed = isset(self::$_permissions[$userId][$permissionName]);
        $restricted = isset(self::$_restrictedAccess[$permissionName]) || stripos($permissionName, "*") !== false;
        
        //\var_dump($permissionName . " => " . ($restricted ? "Yes" : "No" ) . ", " . ($allowed ? "Yes" : "No" ));

        if($restricted)
            return $allowed; // Comply Point 1, 2
        else
            return true; // Comply Point 3
    }
}
