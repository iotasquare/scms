<?php

use isqr\scms\components\SGridFieldView;
use isqr\scms\widgets\adminui\actionbar\ActionBar;
use isqr\scms\widgets\adminui\block\Block;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\scms\plugins\qltslms\models\CourseModuleLink;
use kartik\sortable\Sortable;
use richardfan\sortable\SortableGridView;
use app\scms\plugins\qltslms\models\CourseUserLink;
use isqr\scms\models\SUserGroup;
use isqr\scms\models\SUserGroupLink;
use yii\widgets\DetailView;
use isqr\scms\widgets\adminui\box\Box;
use isqr\scms\components\SGridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php $this->addActionButton("Back", ["index"], 'reply');?>
<?php $this->addActionPopupButton("Edit Group", ["jsn-update", 'id'=>$model->id], 'popup_create', "Edit Group", 'updateOnSuccess', 'pencil');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List'])?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:html',
            [
                'label' => 'Created At',
                'value' => function($model){
                    $html = "";
                    $userName = '(not set)';
                    if($model->createdByUser != null)
                        $userName = $model->createdByUser->name;
                    $html .= date("d-m-Y H:i:s", strtotime($model->created_time)) . " by <strong>" . $userName ."</strong>";
                    return $html;
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Last Updated',
                'value' => function($model){
                    $html = "";
                    $userName = '(not set)';
                    if($model->lastUpdatedByUser != null)
                        $userName = $model->lastUpdatedByUser->name;
                    $html .= date("d-m-Y H:i:s", strtotime($model->last_updated_time)) . " by <strong>" . $userName ."</strong>";
                    return $html;
                },
                'format' => 'raw'
            ],
        ],
    ]);
    ?>
    <?php Box::end();?>
</div>

<div class="row">
    <?php Box::begin(['size'=>12, 'title'=>'Details', 'type'=>'tab'])?>
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="users">
            <?php Pjax::begin(array("id"=>"pjx_users"));?>
            <div class="box-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box-action-buttons">
                        <?= Html::a("Add User", "#", array(
                                "onclick"=> "return showPopup('popup_module_link', 'Add User', '".Url::toRoute(["jsn-add-user-link", 'id'=>$model->id])."', updateOnSuccess);",
                                "class" => "btn btn-sm btn-primary"
                            ))
                        ?>
                        </div>
                    </div>
                </div>
            </div>

            
            <?php $searchUserModel = new SUserGroupLink();?>
            <?php $searchUserModel->filter_group_id = $model->id; ?>
            <?php $userDataProvider = $searchUserModel->search(Yii::$app->request->queryParams); ?>
            
            <?= SGridView::widget([
                'dataProvider' => $userDataProvider,
                'sortUrl' => Url::to(['user-sort']),
                'columns' => [
                    [
                        'header' => 'Name',
                        'attribute' => 'filter_module_name',
                        'format'=>'raw',
                        'value' => function($model) {
                            $html = "";
                            if($model->user != null)
                                $html .= '<a href="'. Url::toRoute(["/admin/scms/user/view", 'id'=>$model->user->id]) . '" data-pjax="0">'.$model->user->name."</a> ";
                            return $html;
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                        'contentOptions' => ['style' => 'width:80px;text-align:center;', 'class'=>'actions'],
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', "#", array(
                                    'onclick' => 'return ajax_post("'.Url::toRoute(['jsn-delete-user-link', 'id' => $model->id]).'", {}, updateOnSuccess, "Are you sure to delete it?")',
                                    "class" => ""
                                ));
                            }
                        ]
                    ]
                ] 
            ]);
            ?>
            <?php Pjax::end();?>
        </div>
    </div>


<script>
function updateOnSuccess(){
	$.pjax.reload({container:"#pjx_users"}); 
}
</script>
<?php Box::end();?>
</div>