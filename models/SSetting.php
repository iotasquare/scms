<?php

namespace isqr\scms\models;

use isqr\scms\components\SActiveRecord;

/**
 * This is the model class for table "cms_settings".
 *
 * @property string $id
 * @property string $name
 * @property string $value
 */
class SSetting extends SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value'], 'string'],
            [['value'], 'default', 'value' => null],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }
}
