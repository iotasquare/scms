<?php
namespace isqr\scms\controllers;

use Yii;
use isqr\scms\components\SCrudController;
use yii\helpers\ArrayHelper;

class MediaLibraryAdminController extends SCrudController
{    
	public function getObjectTitles()
    {
		return array(
				"singular" => "Media Library",
				"plural" => "Media Library",
		);
    }
    
    public function getModelClass()
    {
        return 'isqr\scms\models\SFile';
    }

    public function actionJsnRename()
    {
    	$name = \Yii::$app->request->post("name", null);
    	$id = \Yii::$app->request->post("id", null);
    	if($name != null && $id != null){
	        $model = $this->findModel($id);
	        $model->original_name = $name;
	        $model->save();
    	}
        
        return $this->renderJsn(1, "Saved");
    }
}