<?php
use yii\base\Widget;
use isqr\scms\components\SGlobal;
use yii\helpers\Url;
?>
<div class="row" style="padding-bottom: 50px;">
	<?php $dashboardWidgets = SGlobal::getAppParam('dashboard_widgets');?>
	<?php if($dashboardWidgets != null):?>
		<?php $widgets = Yii::$app->scmsApp->getConfig("admin_dashboard_widgets"); ?>
		<?php foreach ($dashboardWidgets as $widgetId):?>
			<?php if(isset($widgets[$widgetId])):?>
				<?php $widget = $widgets[$widgetId]?>
				<?php $widgetClass = $widget['class'];?>
				<?=call_user_func_array([$widgetClass, 'widget'], [$widget['params']]);?>
			<?php endif;?>
		<?php endforeach;?>
	<?php endif?>
</div>