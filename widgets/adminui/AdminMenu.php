<?php
namespace isqr\scms\widgets\adminui;

use Yii;
use yii\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\helpers\Url;


class AdminMenu extends Menu
{
    public $linkTemplate = '<a href="{url}" target="{target}">{label}</a>';
    public $target;
    
    public function run()
    {
        $items = Yii::$app->scmsApp->getConfig("admin_menu");
        $sortedMenu = [];
        $c = 1;
        foreach ($items as $k=>$item)
        {
            if(isset($item['items']))
            {
                foreach ($item['items'] as $ksub =>$subItem){
                    if($subItem['url'] == Yii::$app->request->getAbsoluteUrl(true))
                        $items[$k]['items'][$ksub]['active'] = true;
                }
            }
            else if($item['url'] == Yii::$app->request->getAbsoluteUrl(true))
                    $items[$k]['active'] = true;
            
            $i = $items[$k]['sort'];
            $sortedMenu[$i.".".$c] = $items[$k];
            $c++;
        }
        ksort($sortedMenu);
        $this->items = $sortedMenu; 

        parent::run();
    }

    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
                '{target}' => isset($item['target'])?$item['target']:'',
            ]);
        }

        $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

        return strtr($template, [
            '{label}' => $item['label'],
        ]);
    }
}
