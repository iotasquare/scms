# SQL Manager 2007 for MySQL 4.2.0.2
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : shozab_spms


SET FOREIGN_KEY_CHECKS=0;

#
# Structure for the `cms_activity_log` table : 
#

CREATE TABLE `cms_activity_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `activity_time` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `table_object_id` bigint(20) DEFAULT NULL,
  `json_params` text CHARACTER SET utf8 DEFAULT NULL,
  `deleted` smallint(1) NOT NULL DEFAULT 0,
  `deleted_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

#
# Structure for the `cms_auth` table : 
#

CREATE TABLE `cms_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `source` varchar(100) NOT NULL,
  `source_id` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Structure for the `cms_email_templates` table : 
#

CREATE TABLE `cms_email_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `from_email` varchar(100) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `subject` varchar(200) NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `cms_files` table : 
#

CREATE TABLE `cms_files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `original_name` varchar(100) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `filepath` varchar(100) NOT NULL,
  `mime_type` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `deleted_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  `deleted` smallint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `cms_pages` table : 
#

CREATE TABLE `cms_pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `content` text DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `last_updated_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `last_updated_time` datetime DEFAULT NULL,
  `deleted` smallint(1) NOT NULL DEFAULT 0,
  `deleted_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `cms_queue` table : 
#

CREATE TABLE `cms_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) NOT NULL,
  `job` blob NOT NULL,
  `pushed_at` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT 0,
  `priority` int(11) unsigned NOT NULL DEFAULT 1024,
  `reserved_at` int(11) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `done_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `reserved_at` (`reserved_at`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

#
# Structure for the `cms_settings` table : 
#

CREATE TABLE `cms_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

#
# Structure for the `cms_templates` table : 
#

CREATE TABLE `cms_templates` (
  `id` varchar(20) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `cms_user_devices` table : 
#

CREATE TABLE `cms_user_devices` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `pin` varchar(8) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `created_time` datetime NOT NULL,
  `activated_time` datetime DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT 0,
  `loggedin` int(11) unsigned DEFAULT 0,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `_h` varchar(100) DEFAULT NULL,
  `auth_code` varchar(100) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_h` (`_h`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `cms_user_group_permissions` table : 
#

CREATE TABLE `cms_user_group_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` bigint(20) unsigned NOT NULL,
  `action` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_group_id` (`user_group_id`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `cms_user_groups` table : 
#

CREATE TABLE `cms_user_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL,
  `last_updated_time` datetime NOT NULL,
  `deleted` smallint(1) NOT NULL DEFAULT 0,
  `last_updated_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `cms_users` table : 
#

CREATE TABLE `cms_users` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `gender` smallint(6) NOT NULL DEFAULT 0 COMMENT '0 is male and 1 is female',
  `password_enc` varchar(128) NOT NULL,
  `activation_key` varchar(128) NOT NULL DEFAULT '',
  `auth_key` text DEFAULT NULL,
  `image_profile` varchar(200) DEFAULT NULL,
  `image_cover` varchar(200) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `intro` varchar(250) DEFAULT NULL,
  `about` text DEFAULT NULL,
  `timezone` varchar(20) DEFAULT NULL,
  `terms_accepted` int(11) NOT NULL DEFAULT 0,
  `super_user` int(11) unsigned DEFAULT 0,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `created_time` datetime DEFAULT NULL,
  `last_updated_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `last_updated_time` datetime DEFAULT NULL,
  `deleted` smallint(1) NOT NULL DEFAULT 0,
  `deleted_user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `deleted_time` datetime DEFAULT NULL,
  `force_change_password` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `cms_users_groups_link` table : 
#

CREATE TABLE `cms_users_groups_link` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

