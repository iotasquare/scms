<?php
namespace isqr\scms\models;

use isqr\scms\models\SUser;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SGlobal;
use isqr\scms\jobs\SEmailJob;
use Yii;

class SPasswordResetForm extends SPasswordChangeForm
{
	public $email;
	public $activation_key;
	public $user;

	public function configure() {
        $rules =  [
			['email', 'required', 'message'=>'Email address is required.'],
			['email', 'email', 'message'=>'Email address is not valid.'],
			['email', 'isUserExists'],
			['activation_key', 'required', 'on' => self::SCENARIO_CHANGEPASSWORD]
        ];

        $attributeLabels =  [
			'email' => Yii::t('app', 'Email'),
        ];
        
        $hints =  [
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
	}

	public function isUserExists($attribute, $params) {
 		// we only want to authenticate when there are no input errors so far
		if(!$this->hasErrors()) {
			$this->user = SUser::find()->andWhere(['=','email', $this->email])->one();
			if($this->user == null){
			    // $this->addError("email", "We didn't recognize the email address you entered. Please try again.");
			    $this->addError("email", Yii::t('app', "The email address you've entered in not registered on our systems. Please try again."));
			}
		}
	}

	public function sendInstructions() {
		$this->user->generateActivationToken();
		if($this->user->save(false, array('activation_key'))) {
			$subject = "Reset your password";
			$recovery_link = '<a href="'.$this->user->getRecoveryUrl().'"> Reset Password</a>';
			$body = 'You have requested a new Password. To set your new Password, please visit at {{recovery_link}}. <br/> {{signature}}';
			
			$template = SEmailTemplate::get("user_password_reset", [
				'to_email' => $this->user->email,
				'to_name' => $this->user->getFullName(),
				'subject' => $subject,
				'content' => $body,
				'recovery_link' => '<a href="'.$this->user->getRecoveryUrl().'"> Reset Password</a>',
				'recovery_url' => $this->user->getRecoveryUrl(),
			]);
			Yii::$app->queue->push(new SEmailJob([
				'to_email' => $this->user->email,
				'to_name' => $this->user->getFullName(),
				'from_email' => $template->from_email,
				'from_name' => $template->from_name,
				'subject' => $template->subject,
				'content' => $template->content,
			]));
			return true;
		}
		return false;
	}

	public function updatePassword() {
		$this->user->setPassword($this->password);
		return $this->user->update();
	}
}