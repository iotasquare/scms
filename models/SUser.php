<?php

namespace isqr\scms\models;

use Exception;
use Yii;
use isqr\scms\components\SEnum;
use isqr\scms\components\SGlobal;
use isqr\scms\jobs\SEmailJob;
use yii\base\Security;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use isqr\scms\models\SUserDevice;
use yii\web\UnauthorizedHttpException;

class SUser extends IUser implements IdentityInterface
{   
    const USER_STATUS_INACTIVE = 0;
    const USER_STATUS_ACTIVE = 1;
    const USER_STATUS_BANNED = 2;

    public static $statuses = array(
        self::USER_STATUS_ACTIVE => 'Active',
        self::USER_STATUS_INACTIVE => 'Inactive',
        self::USER_STATUS_BANNED => 'Banned'
    );

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;
    const GENDER_UNSPECIFIED = 2;
    
    public static $genders = array(
        self::GENDER_MALE => 'Male',
        self::GENDER_FEMALE => 'Female',
    );

    public $password = '';
    public $confirm_password = '';
    public $file_profile;
    public $file_cover;
    public $_groups;
    
    public $filter_name;
    public $filter_email;
    public $filter_group_id;

    public function configure() {
        $rules = [
        	['username', 'unique', 'targetAttribute' => ['username'], "message"=>"Username is not available."],
			['email', 'unique', 'targetAttribute' => ['email'], "message"=>"A user is already registered with this email."],
            ['email', 'email'],
            [['status'], 'default', 'value' => 1],
            ['password', 'string', 'min'=>6, 'max'=>16, 'tooShort'=>'Password is too short (minimum is 6 characters)'],
            [['password', 'password_enc', 'confirm_password'], 'required', 'on' => self::SCENARIO_INSERT],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords does not match" ],        

            [['force_change_password'], 'default', 'value' => 1],
            [['file_profile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['file_cover'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['groups'], 'safe'],

            [['filter_name', 'filter_email', 'filter_group_id'], 'safe']
        ];

        $attributeLabels = [
            'id' => Yii::t('app', 'User Id'),
            'name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'surname' => Yii::t('app', 'Last Name'),
            'username' => Yii::t('app', 'Username'),
            'gender' => Yii::t('app', 'Gender'),
            'confirm_password' => Yii::t('app', 'Confirm Password'),
            'password_enc' => Yii::t('app', 'Password Hash'),
            'activation_key' => Yii::t('app', 'Activation Key'),
            'role' => Yii::t('app', 'Role'),
            'image_profile' => Yii::t('app', 'Profile Image'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'address' => Yii::t('app', 'Address'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'filter_group_id' => Yii::t('app', 'Group'),
            'filter_email' => Yii::t('app', 'Email'),
            'filter_name' => Yii::t('app', 'Name'),
            'groups' => Yii::t('app', 'Groups'),
            'force_change_password' => Yii::t('app', 'Force user to set password')
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
    }
    
    public function beforeValidate() {
        if(parent::beforeValidate()) {
            if($this->isNewRecord){
                $this->id = SGlobal::createUniqueId();
            }
            if($this->password != ""){
                if(trim($this->password) != trim($this->confirm_password)){
                    $this->addError("confirm_password", "Password does not match.");
                    return false;
                }
                $this->setPassword($this->password);
            }
            return true;
        }
        return false;
    }
    
    public function beforeSave($insert)
    {
        if($this->force_change_password == '1') {
            if(!$insert && $this->oldAttributes['force_change_password'] == '0')  // update user case where force_change_password is changed and become 1
                $this->generateActivationToken();
            elseif($insert) // New user case.
                $this->generateActivationToken();
        }
        return parent::beforeSave($insert);
    }

    public function sendWelcomeEmail() {
        $title = SGlobal::getSetting("title");
        $subject = "Welcome to " . $title;
        $body = "<h4>Dear {{to_name}}, </h4>";
        $body .= "<p>An online account has been created for you on $title.</p>
        <p>Please click on the following link to access your account. </p>
        {{quick_login_link}}
        <p>{{signature}}</p>";

        $quick_login_link = '<a href="'.$this->getLoginWithActivationKeyUrl().'"> Access Account</a>';

        $template = SEmailTemplate::get("user_welcome", [
            'to_email' => $this->email,
            'to_name' => $this->getFullName(),
            'subject' => $subject,
            'content' => $body,
            'quick_login_link' => $quick_login_link,
            'quick_login_url' => $this->getLoginWithActivationKeyUrl(),
        ]);

        Yii::$app->queue->push(new SEmailJob([
            'to_email' => $this->email,
            'to_name' => $this->getFullName(),
            'from_email' => $template->from_email,
            'from_name' => $template->from_name,
            'subject' => $template->subject,
            'content' => $template->content,
        ]));
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(!$insert && isset($changedAttributes['force_change_password']) && $this->force_change_password == '1' && $changedAttributes['force_change_password'] == '0') {
            
            $subject = "Reset your password";
            $body = 'You are required to update Password. To set your new Password, please visit at {{recovery_link}}. <br/> {signature}';
            
            $template = SEmailTemplate::get("user_force_password_reset", [
                'to_email' => $this->email,
                'to_name' => $this->getFullName(),
                'subject' => $subject,
                'content' => $body,
                'recovery_link' => '<a href="'.$this->getRecoveryUrl().'"> Reset Password</a>',
                'recovery_url' => $this->getRecoveryUrl(),
            ]);
            Yii::$app->queue->push(new SEmailJob([
                'to_email' => $this->email,
                'to_name' => $this->getFullName(),
                'from_email' => $template->from_email,
                'from_name' => $template->from_name,
                'subject' => $template->subject,
                'content' => $template->content,
            ]));
        }
        parent::afterSave($insert, $changedAttributes);
       
        $this->saveGroups();
    }

    /**
     * @return SUserGroup
     */
    public function getGroups()
    {
    	return $this->hasMany(SUserGroup::className(), ['id' => 'user_group_id'])
      	->viaTable(SUserGroupLink::tableName(), ['user_id' => 'id']);
    }

    protected function setGroups($newData){
        $this->_groups = parent::prepareRelatedData("groups", $newData);
    }
    
	private function saveGroups(){        
        if($this->_groups==null)
            return;

        foreach ($this->_groups as $k=>$v){
            if($v['action'] == self::SOP_ADD){
                $tag = new SUserGroupLink();
                $tag->user_group_id = $v['data'];
                $tag->user_id = $this->id;
                $tag->save();
            }
            else if($v['action'] == self::SOP_DELETE){
                $tag = SUserGroupLink::find()
                ->where(['user_group_id' => $v['data'], "user_id" => $this->id])
                ->one();
                $tag->delete();
            }
        }
    }

    public function hasGroup($group) {  
        foreach($this->groups as $v)
            if(strtolower($v->name) == $group) 
                return true;

        return false;
    }

    public function getFullName() {
        if(trim($this->middle_name) != "")
            return trim($this->name . " " . $this->middle_name . " " . $this->surname);
        return trim($this->name . " " . $this->surname);
    }
    
    public function getStatus()
    {
        return self::$statuses[$this->status];
    }
    
    public function getGender()
    {
        return $this->gender == 0?"Male":"Female";
    }
    
    private static $_loggedin = null;
    public static function getCurrentUser(){
        if(self::$_loggedin == null)
            self::$_loggedin = SUser::findIdentity(\Yii::$app->user->id);
        return self::$_loggedin;
    }
    
    private static $_loggedinDevice = null;
    public static function getCurrentDevice(){
        if(self::$_loggedinDevice == null) {
            $cookies = Yii::$app->request->cookies;
            $_d = $cookies->getValue('_d', '');
            $device = SUserDevice::find()
				->andWhere(['id'=>$_d])
				->one();
            self::$_loggedinDevice = $device;
        }
        return self::$_loggedinDevice;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $device = SUserDevice::find()
        ->andWhere(['_h' => $token])
        ->one();
        if($device == null) return null;
        return static::findOne([
            'id' => $device->user_id
        ]);
    }

    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username
        ]);
    }
    
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email
            ]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    public function validatePassword($password)
    {
        $security = new Security();
        if($security->validatePassword($password, $this->password_enc))
            return true;
        else return false;
    }

    public function setPassword($password)
    {
        $security = new Security();        
        $this->password_enc = $security->generatePasswordHash($password);
    }

    public function generateResetPin()
    {
        $security = new Security();
        $this->activation_key = SGlobal::generatePIN(4);
        return $this->activation_key;
    }

    public function generateActivationToken()
    {
        $security = new Security();
        $this->activation_key = $security->generateRandomString() . '_' . time();
        return $this->activation_key;
    }

    public function removeActivationToken()
    {
        $this->activation_key = "".rand();
    }
    
    public function getRecoveryUrl()
    {
        return Url::toRoute([
            '/scms/user/reset-password',
            'key' => $this->activation_key,
            'email' => $this->email,
            'scn' => 'changepassword',
        ], true);
    }

    public function getLoginWithActivationKeyUrl()
    {
        return Url::toRoute([
            '/scms/user/login-with-key',
            'key' => $this->activation_key,
            'email' => $this->email
        ], true);
    }
    
    /**
     * Cases:
     * 1. If login without device, it will be a simple login.
     * 2. If login with device and device is a new device, following scenerios will happen.
     *      a. If device change notification is enabled, it will send notification if location is not same as last one.
     *      b. If device single login at a time is enabled, it will logout all previous loggedin devices. Don't applies on admin users.
     *      c. 
     */
	public function login($device = false)
	{
        $autologin_enable = SGlobal::getSetting("autologin_enable");
        $autologin_cookieduration = SGlobal::getSetting("autologin_cookieduration", 30*24*60);

        Yii::$app->user->login($this, $autologin_cookieduration);
        SActivityLog::log("user_login");

        if($device !== false) {

            $deviceSingleLoginEnable = SGlobal::getAppParam("device_single_login_enable", false);
            if($deviceSingleLoginEnable) { // Logout from other devices
                $otherDevices = SUserDevice::find()
                ->andWhere(['user_id'=>$device->user_id])
                ->andWhere(['<>','id', $device->id])
                ->all();
                foreach($otherDevices as $otherDevice){
                    $otherDevice->loggedin = 0;
                    $otherDevice->save();
                }
            }
            
            $device->last_login_time = date("Y-m-d H:i:s");
            $device->loggedin = 1;
            $device->save();

            $deviceChangeNotificationEnable = SGlobal::getAppParam("device_location_change_notification_enable", false);
            if($deviceChangeNotificationEnable == true) {
                $lastLoggedinDevice = SUserDevice::find()
                ->andWhere(['user_id' => $device->user_id])
                ->andWhere(['<>','id', $device->id])
                ->orderBy("last_login_time DESC")
                ->limit(1)
                ->one();
                
                if($lastLoggedinDevice != null && $device->location != $lastLoggedinDevice->location){
                    $currentUser = SUser::find()->andWhere(['id'=>$device->user_id])->one();
                    $subject = "User Logged-in From Different City";
                    $body = "<p>The user {{user_profile}} is logging from different city.<br /> Current location is {{current_location}}</p>";
                    $fromName = SGlobal::getSetting("sender_name", "Admin");
                    $fromEmail = SGlobal::getSetting("sender_email", "admin@example.com");
                    $companyName = SGlobal::getSetting("company_name", "Company Name");
                    $companyEmail = SGlobal::getSetting("company_email", "admin@example.com");
                    $template = SEmailTemplate::get("user_login_different_location", [
                        'to_email' => $companyEmail,
                        'to_name' => $companyName,
                        'subject' => $subject,
                        'content' => $body,
                        'user_profile_link' => "<a href='".$currentUser->getProfileUrl()."'>".$currentUser->getFullName()."</a>",
                        'user_profile_url' => $currentUser->getProfileUrl(),
                        'current_location' => $device->location
                    ]);
                    Yii::$app->queue->push(new SEmailJob([
                        'to_email' => $companyEmail,
                        'to_name' => $companyName,
                        'from_email' => $template->from_email,
                        'from_name' => $template->from_name,
                        'subject' => $template->subject,
                        'content' => $template->content,
                    ]));
                    SActivityLog::log("user_login_other_location");
                }
            }
        }
        return true;
    }
    
    public function logout(){
        $device = SGlobal::getCurrentDevice();
        if($device != null) {
            $device->loggedin = 0;
            $device->save();
        }
        
        @Yii::$app->user->logout();
        SActivityLog::log("user_logout");
    }
    
    /*------------------------- User Profile Functions ------------------------------*/
    
	public function getLocation(){
		$countries = SGlobal::getCountryList();
		$country = "";
		if(isset($countries[$this->country])){
			$country = $countries[$this->country];
    		return $this->city.", " . $country;
		}
		else return $this->city;
    }
    
    public function getProfileUrl(){
    	if(trim($this->username) != "")
    		$url = Url::toRoute(['/scms/user/index', 'username'=>$this->username], true);
    	else
        	$url = Url::toRoute(['/scms/user/index', 'id'=>$this->id], true);
        return $url;
    }
    
    public function getProfileImageUrl(){
        $assetUrl = Yii::$app->assetManager->getPublishedUrl("@scms/assets");
        if(trim($this->image_profile) == "")
            $url = $assetUrl."/images/profile-image.jpg";
        else 
            $url = SGlobal::getUrl($this->image_profile);
        return $url;
    }
    
    public function saveProfileImage($fileData = false, $ext = false){
    	if($fileData === false){
            $fileInfo = SGlobal::prepareFile($this, 'file_profile','profiles');
            
	        if($fileInfo!=false){
                $url = $fileInfo['filepath'].$fileInfo['filename'];
		        $this->image_profile = $url;
		        SGlobal::saveFile($fileInfo['tag']);
                $this->image_profile = SGlobal::resizeAndSaveImage($url, 400, 400); 
		        SGlobal::saveFile($fileInfo['tag']);
	        }
    	}
    	else{
    		$fileInfo = SGlobal::saveFileByData($this, 'file_profile','profiles', "", $fileData, $ext); 
	        $this->image_profile = $fileInfo['filepath'].$fileInfo['filename'];
    	}
    }

    public function getProfileCoverImageUrl(){
        $assetUrl = Yii::$app->assetManager->getPublishedUrl("@scms/assets");
        if(trim($this->image_cover) == "")
            $url = $assetUrl."/images/profile-cover-image.jpg";
        else 
            $url = SGlobal::getUrl($this->image_cover);
        return $url;
    }
    
    public function saveProfileCoverImage($fileData = false, $ext = false){
    	if($fileData === false){
            $fileInfo = SGlobal::prepareFile($this, 'file_cover','profiles');
	        if($fileInfo!=false){
                $url = $fileInfo['filepath'].$fileInfo['filename'];
		        $this->image_cover = $url;
		        SGlobal::saveFile($fileInfo['tag']);
                $this->image_cover = SGlobal::resizeAndSaveImage($url, 600, 400); 
		        SGlobal::saveFile($fileInfo['tag']);
	        }
    	}
    	else{
    		$fileInfo = SGlobal::saveFileByData($this, 'file_cover','profiles', "", $fileData, $ext); 
	        $this->image_cover = $fileInfo['filepath'].$fileInfo['filename'];
    	}
    }
    
    protected function searchQuery(){
        $query = parent::searchQuery();
        $query->leftJoin(SUserGroupLink::tableName(), SUserGroupLink::tableName() . '.user_id = t.id');

        $query->andFilterWhere(['like', 't.name', $this->filter_name]);
        $query->andFilterWhere(['like', 't.email', $this->filter_email]);
        $query->andFilterWhere(['=', SUserGroupLink::tableName().'.user_group_id', $this->filter_group_id]);
		return $query;
    }
    
    public function search($params)
    {
        $dataProvider = parent::search($params);
        /*$dataProvider->sort->attributes['group'] = [
            'asc' => [SUserGroup::tableName().'.name' => SORT_ASC],
            'desc' => [SUserGroup::tableName().'.name' => SORT_DESC],
        ];*/
        
        return $dataProvider;
    }

    public static function getUserUsingAuth($client) {
        $attributes = $client->getUserAttributes();
		$email = ArrayHelper::getValue($attributes, 'email');
		$id = ArrayHelper::getValue($attributes, 'id');
        $nickname = ArrayHelper::getValue($attributes, 'name');
        if($email == null) {
            throw new Exception("Unable to get your email address from '" . $client->getId() . "'");
        }
		$auth = SAuth::find()->where([
            'source' => $client->getId(),
            'source_id' => $id,
		])->one();
		if ($auth == null) { // When user is not connected with via this platform
			$user = SUser::find()
			->andWhere(["email" => $email])
			->one();

			if($user != null) { // When user is already registered with other method.
				$auth = new SAuth ();
				$auth->user_id = $user->id;
				$auth->source = $client->getId();
				$auth->source_id = $id;
				$auth->save();
			}
			else {
				if(SGlobal::getAppParam("userAllowSignup") !== true) 
					throw new UnauthorizedHttpException("This website don't allow to register.");
				$user = new SUser();
				$user->id = SGlobal::createUniqueId();
				$user->email = $email;
				$user->name = $nickname;
				$user->setPassword(Yii::$app->security->generateRandomString(6));
				if(SGlobal::getAppParam("userAutoApproveOnSignup") === true)
					$user->status = SUser::USER_STATUS_ACTIVE;
				else 
					$user->status = SUser::USER_STATUS_INACTIVE;
			
				if($user->save())
				{				
					$auth = new SAuth ();
					$auth->user_id = $user->id;
					$auth->source = $client->getId();
					$auth->source_id = $id;
					$auth->save();
				}
			}
		}
		else {
			$user = SUser::find()
			->andWhere(["id" => $auth->user_id])
			->one();
		}
		return $user;
    }

    public static function getUserByEmail($email){
        $user_data = SUser::find()->andWhere(['email'=>$email])->one();
        return $user_data;
    }

    public static function disableForcePassword($email){
        $model = SUser::find()->andWhere(['email'=>$email])->one();
        $model->force_change_password = 0;
        $model->save();
        return true;
    }
    
}
