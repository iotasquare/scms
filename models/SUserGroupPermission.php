<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

class SUserGroupPermission extends IUserGroupPermission
{
    public function rules()
    {
        $arr = [
        ];
        return ArrayHelper::merge(parent::rules(), $arr);
    }

    public function attributeLabels()
    {
        $arr = [
        ];
        return ArrayHelper::merge(parent::attributeLabels(), $arr);
    }
    
    public function hints()
    {
        $arr = [
        ];
        return ArrayHelper::merge(parent::hints(), $arr);
    }
}
