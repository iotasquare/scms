<?php
namespace isqr\scms\models;
use yii;
use yii\base\Model;
use isqr\scms\models\SUser;
use isqr\scms\components\SGlobal;

/**
 * This is the model class for Login Form.
 *
 * @property SUser $_identity
 */
class SGeneralSettingForm extends SSettingForm
{
	public $title, $status, $tagline, $description, $home_page_id, $logo, $logo_inverted, $favicon, $conformity_message; 
	public $company_name, $company_email, $company_phone_number, $company_address;
	public $analytic_code, $meta_description, $meta_keywords, $meta_image, $sender_email, $sender_name, $sender_signature, $timezone, $autologin_cookieduration, $autologin_enable;
	
    public function rules()
    {
        return [
            [
                ['title', 'status'],
                'required',
                'message' => '{attribute} is required.'
            ],
            [
                ['title', 'status', 'tagline', 'description', 'home_page_id', 'logo', 'logo_inverted', 'favicon',
                'company_name', 'company_email', 'company_phone_number', 'company_address',
                'analytic_code', 'meta_description', 'meta_keywords', 'meta_image', "sender_name", "sender_email", "sender_signature", "timezone",
                'autologin_cookieduration', 'autologin_enable','conformity_message'
                ]
                ,'safe'
            ],
        ];
    }

	public function attributeLabels()
	{
		return [
		    'home_page_id' => 'Home Page',
			'autologin_cookieduration' => 'Validity Period',
			'autologin_enable' => 'Automatic Login'
		];
	}
}