ALTER TABLE `cms_pages` ADD COLUMN `status` smallint(6) NOT NULL;
ALTER TABLE `cms_pages` ADD COLUMN `created_user_id` bigint(20) unsigned NOT NULL DEFAULT '0';
ALTER TABLE `cms_pages` ADD COLUMN `created_time` datetime DEFAULT NULL;
ALTER TABLE `cms_pages` ADD COLUMN `last_updated_user_id` bigint(20) unsigned NOT NULL DEFAULT '0';
ALTER TABLE `cms_pages` ADD COLUMN `last_updated_time` datetime DEFAULT NULL;
ALTER TABLE `cms_pages` ADD COLUMN `deleted` smallint(1) NOT NULL DEFAULT '0';
ALTER TABLE `cms_pages` ADD COLUMN `deleted_user_id` bigint(20) unsigned NOT NULL DEFAULT '0';
ALTER TABLE `cms_pages` ADD COLUMN `deleted_time` datetime DEFAULT NULL;