<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use isqr\scms\widgets\adminui\actionbar\ActionBar;
use isqr\scms\widgets\adminui\block\Block;
use isqr\scms\widgets\adminui\box\Box;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $this->addActionPopupButton("Add Page", ["jsn-create"], 'popup_create', "Add Page", 'pageUpdateOnSuccess', 'plus');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List', 'type'=>'tabular'])?>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'page-update-form',
        'options' => [
            'method' => 'post',
            "autocomplete" => "off"
        ]
    ]); ?>
    
	<div class="row">
		<div class="col-md-4">
			<img alt="" src="<?=$theme['preview_default']?>" class="img-responsive thumbnail"/>
		</div>
		<div class="col-md-8">
			<h1><?=$theme['name'];?></h1>
			<p><?=$theme['description'];?></p>
		</div>
		<div class="col-md-12">
			<?= Html::button('<i class="fa fa-th"></i> Select Other Theme', ['class' => 'btn btn-warning btn-sm', 'onclick'=>'openThemePopup();']); ?>
		</div>
	</div>
</div>
<?php Box::end();?>

<?php Modal::begin([
    "id" => "themelistPopup",
    'header'=>'Select Theme',
    'footer'=>'<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>'
])?>
	<div class="row">
		<?php foreach ($themelist as $k=>$v):?>
		<div class="col-md-6">
			<div class="thumbnail">
				<img alt="" src="<?=$v['preview_default']?>" class="img-responsive "/>
				<div class="themename"><?=$v['name'];?></div>
				<a href="<?= Url::toRoute(['/scms/theme-admin/index','change'=>$v['id']]);?>" class="btn btn-block btn-primary btn-sm">Select</a>
			</div>
		</div>
		<?php endforeach;?>
	</div>
<?php Modal::end();?>
<?php JSRegister::begin([ 'position' => \yii\web\View::POS_BEGIN ]);?>
<script>
	function openThemePopup(){
		$('#themelistPopup').modal('show');
	}
</script>
<?php JSRegister::end();?>