<?php

use yii\helpers\Html;
use yii\helpers\Url;
use isqr\scms\widgets\adminui\actionbar\ActionBar;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->params['breadcrumbs'][] = "Configurations";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">
	<div class="ssite-form">
        <?php $form = ActiveForm::begin([
            'id'=>'setting-form',
            'options' => [
            ]]); ?>
            <?php $this->addActionCustomButton(
                Html::a("Save", "#", array("class" => "btn btn-light action_button", 'onclick'=>'jQuery("#setting-form").submit();'))
            );?>
        	<div class="row">
            <?= $this->render($view, [
                'model' => $model,
                'form' => $form
            ]) ?>
            </div>
        <?php ActiveForm::end(); ?>
	</div>
</div>
