<?php

namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;
use isqr\scms\models\SUser;
use isqr\scms\components\SEnum;

class SUserGroup extends IUserGroup
{
    public $_permissions;
    public $filter_name;
    public $filter_description;

    public function configure()
    {
        $rules = [];
        $rules[] = [['filter_name'], 'safe'];
        $rules[] = [['filter_description'], 'safe'];
        $rules[] = [['permissions'], 'safe'];

        $attributeLabels = [];
        $attributeLabels["filter_name"] = "Name";
        $attributeLabels["filter_description"] = "Description";
        
        $hints = [];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }

    protected function searchQuery(){
		$query = parent::searchQuery();
		
        $query->andFilterWhere(['LIKE','t.name', $this->filter_name]);
        $query->andFilterWhere(['LIKE','t.description', $this->filter_description]);
        
		return $query;
	}
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($insert == false)
            $this->savePermissions();
    }

    public function setPermissions($newData) {
        $this->_permissions = parent::prepareRelatedData("permissions", $newData, 'action');
    }

    public function getPermissions() {
        return $this->hasMany(SUserGroupPermission::className(), ['user_group_id' => 'id']);
    }

    private function savePermissions(){
        if($this->_permissions == null) 
            return;
        foreach ($this->_permissions as $k=>$v){
            if($v['action'] == self::SOP_ADD){
                $tag = new SUserGroupPermission();
                $tag->user_group_id = $this->id;
                $tag->action = $v['data'];
                $tag->save();
            }
            else if($v['action'] == self::SOP_DELETE){
                SUserGroupPermission::deleteAll([
                    'AND',
                    ['action' => $v['data']],
                    ['user_group_id' => $this->id],
                ]);
            }
        }
    }

    public function getUsers()
    {
        return $this->hasMany(SUser::className(), ['id' => 'user_group_id']);;
    }
}
