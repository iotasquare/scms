<?php use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>
<div class="row userprofile">
	<div class="col-md-3">
		<img class="thumbnail img-responsive" src="<?php echo $user->getProfileImageUrl($this->context->appAsset); ?>" alt="<?php echo $user->getFullName(); ?>"/>
	</div>
	<div class="col-md-9 description">
		<h1><?= $user->getFullName(); ?></h1>
		<p><i class="fa fa-envelope"></i> <strong>Email:</strong> <?php echo $user->email?></p>
		<p><i class="fa fa-user"></i> <strong>Gender:</strong> <?php echo $user->getGender();?></p>
		<p><i class="fa fa-calendar"></i> <strong>Member since:</strong> <?php echo date('F Y',strtotime($user->created_time));?></p>
	</div>
</div>




