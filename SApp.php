<?php
namespace isqr\scms;

use Exception;
use Yii;
use isqr\scms\components\SGlobal;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SEnum;

/**
 * @author shozab.hasan
 * @description 
 * Alias are created as follows
 * @scms => Extension path
 * @scms/plugins => Plugins
 * @scms/theme => Current selected theme path
 * @scms/media => media folder in web
 */
final class SApp implements BootstrapInterface
{
    private $_config = [];
    
    /**
     * Returns the value of a param in main config file in bootstrap array.
     * @param string $name
     */
    public function getParam($name, $default = null){
        return isset($this->{$name})?$this->{$name}:$default;
    }

    /**
     * Returns the value of a merged configurations provided by all plugins.
     * @param string $name
     */
    public function getConfig($name){
        return isset($this->_config[$name])?$this->_config[$name]:null;
    }
    
    public function bootstrap($app)
    {
        date_default_timezone_set("UTC");
        $app->set("scmsApp", $this);
        Yii::setAlias('@isqr', '@vendor/isqr');
        Yii::setAlias('@scms', '@isqr/scms');
        Yii::setAlias('@scms/plugins', '@app/scms/plugins');
        Yii::setAlias('@scms/themes', '@app/scms/themes');
        
        //Registering SCMS as module
        $app->setModule('scms', ['class' => '\isqr\scms\Plugin']);
        
        if(!Yii::$app->request->isConsoleRequest) 
        	Yii::$app->errorHandler->errorAction='scms/default/error';
        
		try {
	        $gii = $app->getModule('gii');
	        if($gii!=null){
	            $gii->generators['scms-model'] = [
	                'class' => 'isqr\scms\giitemplates\model\Generator',
	            ];
	        }
    	}
    	catch (\ReflectionException $ex){}
    	
        $autologin_enable = SGlobal::getSetting("autologin_enable");
        \Yii::$app->setComponents([
            'user' => [
                'class' => 'yii\web\User',
                'identityClass' => '\isqr\scms\models\SUser',
                'enableAutoLogin' => $autologin_enable == 1 ? true : false,
                'loginUrl' => ['/scms/user/login']
            ],
            'view' => [
                'class' => 'isqr\scms\components\SView'
            ],
            'authManager' => [
                'class' => 'isqr\scms\components\SAuthManager'
            ]
        ]);
        $config = $app->getModule('scms')->load($app->scmsApp);
        $this->_config = ArrayHelper::merge($this->_config, $config);
        
        if(isset($this->plugins)){
            foreach ($this->plugins as $id=>$params){
                $pluginClass = "app\\scms\\plugins\\$id\\Plugin";
                $app->setModule($id, [
                    'class' => $pluginClass,
                	'params' => $params
                ]);
                Yii::setAlias("@$id", "@scms/plugins/$id");
                $config = $app->getModule($id)->load($app->scmsApp);
                $this->_config = ArrayHelper::merge($this->_config, $config);
            }
        }
        try{
            $assetDir = Yii::getAlias("@webroot/assets");
            if(!is_dir($assetDir)){
                mkdir($assetDir);
            }
        }
        catch(Exception $ex) {
            
        }
        
        // we want to have domain param loaded to prepare menu items.
        $app->on(Application::EVENT_BEFORE_ACTION, function () {
        	if(!Yii::$app->request->isConsoleRequest) {
				Yii::setAlias('@scms/url/media', '/media/');
                $theme = SGlobal::getSetting("theme");
                Yii::setAlias('@scms/theme', '@app/scms/themes/'.$theme);
        
                Yii::$app->view->theme = new \yii\base\Theme([
                    'basePath' => "@scms/theme",
                    'baseUrl' => "@web/themes/$theme",
                    'pathMap' => [
                        '@scms/views' => [
                            "@scms/theme/views",
                            "@scms/basetheme/views"
                        ],
                        '@scms/plugins' => [
                            "@scms/theme/plugins",
                            "@scms/basetheme/plugins"
                        ],
                        '@scms/widgets' => [
                            "@scms/theme/widgets",
                            "@scms/basetheme/widgets"
                        ]
                    ]
                ]);
                
                $isAdmin = (isset($_GET['view'])?$_GET['view']:"") == "admin";
                \Yii::$app->assetManager->linkAssets = true;

                if(!SGlobal::isAdminMode()) {
                    Yii::$app->params['bsVersion'] = self::getParam("bootstrap_version_front", 4);
                }
                else {
                    Yii::$app->params['bsVersion'] = self::getParam("bootstrap_version_admin", 3);
                }
                     
                if(SGlobal::isBootstrap4()) {
                    \Yii::$container->set('yii\widgets\LinkPager', [
                        'class' => \yii\bootstrap4\LinkPager::class,
                    ]);
                }
        	}
        	else {
        		Yii::setAlias("@webroot", "@app/web"); // Because there is no web root defined in console app.
        		Yii::setAlias('@scms/media', '@webroot/media');
            }
            if(!Yii::$app->request->isConsoleRequest) {
                $language = Yii::$app->session->get("language", "en-Us");
                \Yii::$app->language = $language;
            }
            \Yii::$app->formatter->currencyCode = SGlobal::getSetting("currency", 'PKR');
        });
    }
    
    public function loadAssets($view, $assetView)
    {
        \Yii::setAlias("@scms/plugin", "@scms");
        $pluginAssetClass = "isqr\\scms\\$assetView";
        call_user_func_array([$pluginAssetClass, 'register'], [$view]);
        
        foreach ($this->plugins as $id=>$params){
            \Yii::setAlias("@scms/plugin", "@scms/plugins/$id");
            $pluginAssetClass = "app\\scms\\plugins\\$id\\$assetView";
            
            if(class_exists($pluginAssetClass))
                call_user_func_array([$pluginAssetClass, 'register'], [$view]);
            else 
                \Yii::warning("The class '$assetView' is not exists in the current plugin '$id'.", "sapp");
        }
        \Yii::setAlias("@scms/plugin", null);
        
        $theme = SGlobal::getSetting("theme");
        $themePath = \Yii::getAlias("@scms/theme");
        $themeAssetClass = "app\\scms\\themes\\$theme\\$assetView";
         
        if(class_exists($themeAssetClass))
            $asset = call_user_func_array([$themeAssetClass, 'register'], [$view]);
        else 
            \Yii::warning("The class '$assetView' is not exists in the current theme.", "sapp");
    }
}