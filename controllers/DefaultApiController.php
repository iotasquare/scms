<?php
namespace isqr\scms\controllers;

use isqr\scms\components\SAccessControl;
use Yii;
use isqr\scms\components  \SController;
use isqr\scms\components\SEnum;
use isqr\scms\components\SGlobal;
use isqr\scms\models\SNotification;
use isqr\scms\models\SPage;
use isqr\scms\models\SUser;
use yii\helpers\Url;
use  isqr\scms\components\SApiController;
use yii\helpers\ArrayHelper;

class DefaultApiController extends  SApiController
{
    public function behaviors()
    {
        $arr = [
            'access' => [
                'class' => SAccessControl::className(),
                'rules' => [
                    [
        				'actions'=>['index'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ]
        ];
        return ArrayHelper::merge($arr, parent::behaviors());
	}
	
	public function actionIndex(){
		return $this->json("Welcome to Api.", [], parent::JSON_SUCCESS);
	}
}