<?php
namespace isqr\scms\components;

use yii\base\Model;
use yii\helpers\ArrayHelper;

abstract class SFormModel extends Model
{
    const EVENT_INIT = 'init';

    private $_configuration = [
        'rules' => [],
        'attributeLabels' => [],
        'hints' => []
    ];

    public function init()
    {
        if (method_exists($this, 'configure'))
            $this->_configuration = $this->configure();
        $this->trigger(self::EVENT_INIT);
        parent::init();
    }

    public function addConfiguration($configuration) {
        $this->_configuration = ArrayHelper::merge($this->_configuration, $configuration);
    }

    public function configure() {
        $rules =  [];

        $attributeLabels =  [];
        
        $hints =  [];

        return [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ];
    }

    public function rules()
    {
        return $this->_configuration['rules'];
    }
    
    public function attributeLabels()
    {
        return $this->_configuration['attributeLabels'];
    }
    
    public function hints()
    {
        return $this->_configuration['hints'];
    }  
}