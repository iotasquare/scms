<?php
namespace isqr\scms\components;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\helpers\Url;
use isqr\scms\models\SUserGroupPermission;
use isqr\scms\models\SUser;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

class SAccessControl extends AccessControl
{
    public function init()
    {
        parent::init();
    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action); 
    }

    protected function denyAccess($user)
    {
        if(SGlobal::isApiMode()) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        elseif ($user !== false && $user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
}