<?php
namespace isqr\scms\widgets\adminui\box;

use isqr\scms\SWidget;

class Box extends SWidget
{
    public $size, $boxType = 'default';

    public function init()
    {
        parent::init();
    }
    
    protected function typeDefault($content)
    {
        $boxCss = "box-default";
        return $this->render('default', [
            'boxCss' => $boxCss,
            'size' => $this->size,
            'content' => $content,
            'title' => $this->title
        ]);
    }

    protected function typeTabular($content)
    {
        $boxCss = "tabular table-hover table-responsive";
        return $this->render('default', [
            'boxCss' => $boxCss,
            'size' => $this->size,
            'content' => $content,
            'title' => $this->title
        ]);
    }

    protected function typeTab($content)
    {
        $boxCss = "tab";
        return $this->render('tab', [
            'boxCss' => $boxCss,
            'size' => $this->size,
            'content' => $content,
            'title' => $this->title
        ]);
    }

    protected function typeFilter($content)
    {
        $boxCss = "filter";
        return $this->render('filter', [
            'boxCss' => $boxCss,
            'size' => $this->size,
            'content' => $content,
            'title' => $this->title
        ]);
    }
}
?>