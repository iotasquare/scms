<?php 
use isqr\scms\components\SGlobal;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
?>
<?php $this->beginContent("@scms/views/layouts/main.php", ['scope'=>'user']); ?>
    <?= $content;?>
<?php $this->endContent(); ?>