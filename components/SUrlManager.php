<?php
namespace isqr\scms\components;

use Yii;
use yii\web\UrlManager;
use yii\helpers\ArrayHelper;

class SUrlManager extends UrlManager
{
	public $adminAlias = "admin";
	public $apiAlias = "api";
	
    public function init()
    {        
        $this->enableStrictParsing = true;
        $this->enablePrettyUrl = true;
        $this->suffix = '';
        $this->showScriptName = false;
        
        $adminAlias = $this->adminAlias;
        $apiAlias = $this->apiAlias;

        $domainPattern = "";
        $rules = [
            $domainPattern.'/<view:'.$adminAlias.'>' => 'scms/default-admin/index',
            $domainPattern.'/<view:'.$adminAlias.'>/<controller:\w+[\-\w+]*>' => '<controller>-admin',
            $domainPattern.'/<view:'.$adminAlias.'>/<controller:\w+[\-\w+]*>/<action:\w+[\-\w+]*>' => '<controller>-admin/<action>',
            $domainPattern.'/<view:'.$adminAlias.'>/<module:\w+>/<controller:\w+[\-\w+]*>/<action:\w+[\-\w+]*>' => '<module>/<controller>-admin/<action>',
            
            $domainPattern.'/<view:'.$apiAlias.'>' => 'scms/default-api/index',
            $domainPattern.'/<view:'.$apiAlias.'>/<controller:\w+[\-\w+]*>' => '<controller>-api',
            $domainPattern.'/<view:'.$apiAlias.'>/<controller:\w+[\-\w+]*>/<action:\w+[\-\w+]*>' => '<controller>-api/<action>',
            $domainPattern.'/<view:'.$apiAlias.'>/<module:\w+>/<controller:\w+[\-\w+]*>/<action:\w+[\-\w+]*>' => '<module>/<controller>-api/<action>',
            
            $domainPattern.'/' => 'scms/default/index',
            $domainPattern.'/init' => 'scms/default/init',
            $domainPattern.'/index' => 'scms/default/index',
            $domainPattern.'check75080' => 'scms/default/check75080',
            $domainPattern.'/profile' => 'scms/user/index',
            $domainPattern.'/profile/id/<id:\d+[\-\d+]*>' => 'scms/user/index',
            $domainPattern.'/profile/<username:\w+[\-\w+]*>' => 'scms/user/index',
            $domainPattern.'/signup' => 'scms/user/signup',
            $domainPattern.'/login' => 'scms/user/login',
            $domainPattern.'/logout' => 'scms/user/logout',
            $domainPattern.'/reset-password' => 'scms/user/reset-password',
            $domainPattern.'/<controller:\w+[\-\w+]*>' => '<controller>',
            $domainPattern.'/<controller:\w+[\-\w+]*>/<action:\w+[\-\w+]*>' => '<controller>/<action>',
            $domainPattern.'/<module:\w+>/<controller:\w+[\-\w+]*>/<action:\w+[\-\w+]*>' => '<module>/<controller>/<action>',

        ];
        $this->rules = ArrayHelper::merge($this->rules, $rules);
        parent::init();
    }

    public function createUrl($params)
    {        
        $paramView = isset($params['view'])?$params['view']:"";
        $view = Yii::$app->request->get("view");
        if($paramView != "")
            $params['view'] = $paramView;
        else
            $params['view'] = $view;

        if(stripos($params[0], "-admin") === false) // Removing view if it is not and admin controller
            unset($params['view']);

        return parent::createUrl($params);
    }
}
