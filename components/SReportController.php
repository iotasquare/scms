<?php
namespace isqr\scms\components;

use kartik\mpdf\Pdf;
use yii\helpers\Url;

abstract class SReportController extends SController
{
    protected abstract function getDataProvider($filterModel);
    protected abstract function getFilterModel();
    protected abstract function getReportTitle();

    public function actionIndex()
    { 
        $this->view->title = $this->getReportTitle();
        
        $filterModel = $this->getFilterModel();
        $filterModel->load(\Yii::$app->request->get());
        $dataProvider = $this->getDataProvider($filterModel);

        $pdf = \Yii::$app->request->get("pdf");
        if($pdf == 1) {
            return $this->_renderPDF($dataProvider, $filterModel);
        }
        else {
            $pdfUrl = Url::current(['pdf'=>1]);
            
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'filterModel' => $filterModel,
                'pdfUrl' => $pdfUrl
            ]);
        }
    }

    protected function _renderPDF($dataProvider, $filterModel){
        $dataProvider->setPagination(false);
        
        $content = $this->renderPartial('_report', [
            'dataProvider' => $dataProvider,
        ]);

        $header = 'Generated at: ' . date("r");
        $footer = 'Page {PAGENO}';

        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@scms/assets/css/pdf.css',
            'cssInline' => '',
            'options' => [
            ],
            'methods' => [
                'SetTitle' => $this->view->title,
                'SetHeader' => [$header],
                'SetFooter' => [$footer],
            ]
        ]);
        return $pdf->render();
    }
}