<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_user_devices".
 *
 * @property string $id
 * @property string $name
 * @property string $pin
 * @property string $ip
 * @property string $activated_time
 * @property string $last_login_time
 * @property string $last_ping_time
 * @property integer $active
 * @property integer $loggedin
 * @property string $user_id
 * @property string $_h
 * @property string $auth_code
 * @property string $location
 */
class IUserDevice extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_user_devices';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['name', 'pin'], 'required'],
            [['activated_time', 'last_login_time', 'last_ping_time'], 'safe'],
            [['active', 'loggedin', 'user_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['pin'], 'string', 'max' => 8],
            [['ip'], 'string', 'max' => 20],
            [['_h', 'auth_code'], 'string', 'max' => 100],
            [['location'], 'string', 'max' => 200],
            [['active'], 'default', 'value' => 0],
            [['_h'], 'unique'],
        ];
        $attributeLabels = [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'pin' => Yii::t('app', 'Pin'),
            'ip' => Yii::t('app', 'Ip'),
            'activated_time' => Yii::t('app', 'Activated Time'),
            'last_login_time' => Yii::t('app', 'Last Login Time'),
            'last_ping_time' => Yii::t('app', 'Last Ping Time'),
            'active' => Yii::t('app', 'Active'),
            'loggedin' => Yii::t('app', 'Loggedin'),
            'user_id' => Yii::t('app', 'User ID'),
            '_h' => Yii::t('app', 'H'),
            'auth_code' => Yii::t('app', 'Auth Code'),
            'location' => Yii::t('app', 'Location'),
        ];
        $hints = [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'pin' => Yii::t('app', 'Pin'),
            'ip' => Yii::t('app', 'Ip'),
            'activated_time' => Yii::t('app', 'Activated Time'),
            'last_login_time' => Yii::t('app', 'Last Login Time'),
            'last_ping_time' => Yii::t('app', 'Last Ping Time'),
            'active' => Yii::t('app', 'Active'),
            'loggedin' => Yii::t('app', 'Loggedin'),
            'user_id' => Yii::t('app', 'User ID'),
            '_h' => Yii::t('app', 'H'),
            'auth_code' => Yii::t('app', 'Auth Code'),
            'location' => Yii::t('app', 'Location'),
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }
    
}
