<?php
namespace isqr\scms\components;
use Yii;
abstract class SAdminSettingController extends SController
{
    protected final function process($model, $view){
        if ($model->load(Yii::$app->request->post())){
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', 'Changes are saved.');
                return $this->refresh();
            }
        }
        return $this->render('@scms/views/setting-admin/index', [
            'model' => $model,
            'view' => $view
        ]);
    } 
}