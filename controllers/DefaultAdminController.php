<?php
namespace isqr\scms\controllers;

use isqr\scms\components\SAdminController;
use isqr\scms\components\SController;
use isqr\scms\components\SGlobal;
use yii\db\Schema;
use yii\helpers\ArrayHelper;

class DefaultAdminController extends SController
{
    public function init()
    {
        parent::init();
        $this->view->title = SGlobal::getSetting("title");
    }

    public function actionIndex()
    {
    	$this->view->title = "Dashboard";
        return $this->render($this->getViewName());
    }
    
    public function actionCachePurge(){
        \Yii::$app->cache->flush();
        \Yii::$app->getSession()->setFlash('success', 'All cache is purged.');
        return $this->redirect(["index"]);
    }
}