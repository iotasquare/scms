<?php

namespace isqr\scms\models;

use Yii;
use isqr\scms\components\SActiveRecord;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use isqr\scms\components\SGlobal;
use yii\imagine\Image;

class SFile extends IFile
{
	public $file;
	private $_tag = '';
	public function configure()
    {
        $rules = [
            [['file'], 'required', 'on'=>[self::SCENARIO_INSERT]],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
        ];
        $attributeLabels = [
        ];
        $hints = [
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
	}

	public function beforeValidate()
    {
		try {
			$fileInfo = SGlobal::prepareFile($this, 'file', 'uploads');
			if($fileInfo != false){
				$this->filename = $fileInfo['filename'];
				$this->filepath = $fileInfo['filepath'];
				$this->original_name = $fileInfo['original_name'];
				$this->extension = $fileInfo['extension'];
				$this->mime_type = $fileInfo['mime_type'];
				$this->_tag = $fileInfo['tag'];
			}
		} catch (\Throwable $th) {
			$this->addError("file", $th->getMessage());
		}
		
		return parent::beforeValidate();
	}
	
	public function beforeSave($insert)
    {
		try {
			if($this->scenario == self::SCENARIO_INSERT) {
				if(!SGlobal::saveFile($this->_tag)){
					$this->addError("file", "Unable to save file on the server.");
					return false;
				}
			}
		}
		catch (\Throwable $th) {
			$this->addError("file", $th->getMessage());
		}
		return parent::beforeSave($insert);
	}
	
    public function getUrl($w = false, $h = false){
    	if($w != false || $h != false){
    		if($h == false) $h = 80;
    		if($w == false) $h = 80;
    		$completefilepath = $this->filepath."/".$this->filename;
    		$thumb = "-tb".$w ."x". $h."x.".$this->extension;
			$thumbFile = str_ireplace(".".$this->extension, $thumb, $completefilepath);
			if(is_file($completefilepath))
    			Image::thumbnail($completefilepath, $w, $w)->save($thumbFile, ['quality' => 90]);
    		return SGlobal::getUploadedFileBaseUrl()."/".trim($thumbFile,"/");
    	}
    	else {
        	return SGlobal::getUploadedFileBaseUrl()."/".trim($this->filepath,"/")."/".trim($this->filename,"/");
    	}
    }
    
	public static function getFileObject($id){
    	$file = SFile::find()
    	->andWhere(['id'=>$id])
    	->one();
    	return $file;
    }
    
	public static function getImageUrl($id, $w = false, $h = false){
		if(strstr($id, "/")){
			return SGlobal::getThumbnailUrl($id, $w, $h); //in case if url is saved
		}
    	$file = SFile::find()
    	->andWhere(['id'=>$id])
    	->one();
    	return $file->getUrl($w, $h);
    }
    
	protected function searchQuery(){
		$query = self::find();		
		
        $query->andFilterWhere([
            't.id' => $this->id,
        ]);
        $query->orderBy("id DESC");
        $query->andFilterWhere(['like', 'original_name', $this->original_name]);
		return $query;
	}
}
