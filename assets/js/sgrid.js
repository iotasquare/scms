!function ($, window, pluginName){
    var _sortable = [];
    var API = {
        destroy: function () {
        }
    };

    function _attachSortable(elm, sortUrl){
        var _gridid = elm.attr("id");
        var _doUpdate = false; // to avoid update function for running multiple times
        var _pickedItem = null;
        elm.find('table  tbody').sortable({
            connectWith: "tbody",
            revert: true,
            axis: "y",
            helper: function() { return '<div class="moving-placeholder"><i class="fa fa-arrow-left"></i> </div>'; },
            dropOnEmpty: true,
            items: "tr.sortable",
            containment: "#" + _gridid,
            placeholder: {
                element: function($currentItem) {
                    var col = $currentItem.find("td").length;
                    return $('<tr class="drop-placeholder"><td colspan="' + col + '">Drop here.</td></tr>')[0];
                },
                update: function(container, p) {
                    console.log(container.element);
                    if($(_pickedItem).hasClass("grouprows") && container.element.closest("tr.grouprows").length > 0) {
                        $("tr.drop-placeholder").addClass("invalid");
                        _doUpdate = false;
                    }
                    else {
                        $("tr.drop-placeholder").removeClass("invalid");
                        _doUpdate = true;
                    }
                    return;
                }
            },
            receive: function(ev, ui) {
                if(ui.item.hasClass("grouprows")) {
                    ui.sender.sortable("cancel");
                }
            },
            start: function (event, ui) {
                console.log("start");
                _doUpdate = true;
                _pickedItem = ui.item;
            },
            update: function (event, ui) {
                if(_doUpdate == true) {
                    var items = [];
                    $("#" + _gridid).find("> table > tbody > tr.sortable").each(function(k, v) {
                        var $elm = $(v);
                        var childs = [];
                        if($elm.hasClass("grouprows")) {
                            $elm.find("> td > table > tbody > tr.sortable").each(function(sk, sv) {
                                var $subelm = $(sv);
                                childs.push({
                                    key: $subelm.data("key"),
                                    children: []
                                });
                            });
                        }

                        items.push({
                            key: $elm.data("key"),
                            children: childs
                        });
                    });
                    //console.log(items);
                    var csrfTokenName = jQuery('meta[name=csrf-param]').attr('content');
                    var csrfToken = jQuery('meta[name=csrf-token]').attr('content');
                    
                    $.ajax({
                        'url': sortUrl,
                        'type': 'post',
                        'data': { 'items': items, csrfTokenName : csrfToken },
                        success: function(data){
                            //console.log(data);
                        },
                        error: function(data){
                            //console.log(data);
                        }
                    });
                }
                _doUpdate = false;
            },
            stop: function (event, ui) {
                console.log("stop");
                _doUpdate = false;
            },
        });
    }

    function SGrid(elm, options) {
        elm.find('tr.grouprows thead .ckbcol input[type="checkbox"]').change(function(){
            console.log(this);
            $(this).closest('tr.grouprows').find('tbody input[type="checkbox"]').prop("checked", this.checked);
        });
    
        elm.find('tr.grouprows tbody .ckbcol input[type="checkbox"]').change(function(){
            var list = $(this).closest('tr.grouprows tbody').find('input[type="checkbox"]');
            var all = true;
            for(var i=0; i < list.length; i++) {
                if(list[i].checked == false){
                    all = false;
                    break;
                }
            }
            $(this).closest('tr.grouprows').find('thead .ckbcol input[type="checkbox"]').prop("checked", all);
        });
        this.getSelected = function() {
            var list = elm.find('.ckbcol input[type="checkbox"]:checked');
            return list;
        }
        if(options.sortable)
            _attachSortable(elm, options.sortUrl);
    }

    $.fn[pluginName] = function(methodOrOptions) {
        var args = Array.prototype.slice.call(arguments, 1)
        var $t = $(this),
        object = $t.data(pluginName)
        
        if(object){
            return object
        }
        else {
            console.log("Making Grid Object");
            object = new SGrid($t, methodOrOptions)
            $t.data(pluginName, object);
            return object;
        }
    };

  }(jQuery, window, 'sgrid');
  