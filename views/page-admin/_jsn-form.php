<?php

use isqr\scms\models\SPage;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'page-update-form',
        'options' => [
            'method' => 'post',
            "autocomplete" => "off"
        ]
    ]); ?>
    
    <?= $form->field($model, 'name')->textInput(); ?>
    <?= $form->field($model, 'code')->textInput(); ?>
    <?= $form->field($model, 'content')->textarea(['rows' => 6 , 'class'=>'editor']) ?>
    <?= $form->field($model, 'status')->dropDownList(SPage::$defaultStatuses); ?>

    <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
    <?= Html::a('Cancel', '#', ['class' => 'btn btn-sm btn-default', 'data-dismiss'=>'modal']); ?>
    <?php ActiveForm::end(); ?>
</div>