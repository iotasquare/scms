<?php
namespace isqr\scms;

use Yii;
use yii\base\Module;

/**
 * Plugin is basically a module with some extra functions and loaded by scms app.
 * @author Shozab Hasan
 *
 */
abstract class SPlugin extends Module
{
    public abstract function load(SApp $app);
}

