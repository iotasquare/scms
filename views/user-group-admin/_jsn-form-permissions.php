<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use isqr\scms\models\SUserGroup;
use isqr\scms\components\SEnum;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SGlobal;
use app\scms\plugins\qltslms\models\Module;
use kartik\select2\Select2;
use isqr\scms\models\SUser;
use isqr\scms\models\SUserGroupLink;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\web\View;

?>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'module-update-form',
        'options' => [
            'method'=>'post'
        ]
    ]); ?>
    <?php $actionMatrix = Yii::$app->scmsApp->getConfig("restricted_access"); ?>
    <?php $restrictedActions = ArrayHelper::map($model->permissions, "action", "id"); ?>
    <input name="SUserGroup[permissions][]" type="hidden" value=""/>
    <table class="table table-bordered">
        <tr>
            <th width="100">Resource</th>
            <th>Action</th>
            <th>All</th>
        </tr>
        <?php foreach($actionMatrix as $k=>$v):?>
        <?php $all = true; ?>
        <tr class="perm-row perm-<?=$k?>">
            <td><?=$v['resource']?></td>
            <td>
            <?php foreach($v['permissions'] as $rk=>$rv):?>
            <?php $checked = isset($restrictedActions[$rk]);?>
            <?php $all = $all && $checked; ?>
            <label class="checkbox-inline">
                <input class="checkbox-perm checkbox-perm-<?=$k?>" name="SUserGroup[permissions][]" type="checkbox" value="<?=$rk?>" <?=$checked?"checked":""?> /> <?=$rv?>
            </label>
            <?php endforeach; ?>
            </td>
            <td>    
                <input class="checkbox-perm-all" type="checkbox" value="<?=$rk?>" <?=$all?"checked":""?> />
            </td>
        </tr>
        <?php endforeach; ?>
    </table>

        <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('Cancel', '#', ['class' => 'btn btn-sm btn-default', 'data-dismiss'=>'modal']); ?>
    <?php ActiveForm::end(); ?>
</div>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>

$('input.checkbox-perm-all').change(function(){
    $(this).closest("tr.perm-row").find("input.checkbox-perm").prop("checked", this.checked);
});

$('input.checkbox-perm').change(function(){
    var list = $(this).closest("tr.perm-row").find("input.checkbox-perm");
    var all = true;
    for(var i=0; i < list.length; i++) {
        if(list[i].checked == false){
            all = false;
            break;
        }
    }
    $(this).closest("tr.perm-row").find("input.checkbox-perm-all").prop("checked", all);
});
</script>
<?php JSRegister::end();?>