function initComponents(){
}
function ping(){
    ajax_post(baseurl + "/scms/user/ping", {}, function(status, message, url){
        if(!status){ //if device is not valid
            alert(message);
            window.location.href=url;
        }
    });
}
$(document).ready(function(){
    if(userauth == 1 && device_single_login_enable == 1) {
        ping();
        setInterval(ping, 15000);
    }
});