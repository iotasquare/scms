<?php
namespace isqr\scms\components;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use isqr\scms\components\SActiveRecord;
use yii\helpers\Inflector;

abstract class  SCrudController extends SController
{	
    public abstract function getModelClass();
    
	public function getObjectTitles()
    {
		return array(
				"singular" => "Object",
				"plural" => "Objects",
		);
    }

    public function behaviors()
    {
        $moduleId = $this->action->controller->module->id;
        $controllerId = Inflector::id2camel($this->action->controller->id, "-");
        $arr = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ],
                    'jsn-delete' => [
                        'post'
                    ]
                ]
            ],
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => [$moduleId . "::" . $controllerId . "::View"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'jsn-create'],
                        'roles' => [$moduleId . "::" . $controllerId . "::Create"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'jsn-update'],
                        'roles' => [$moduleId . "::" . $controllerId . "::Update"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete', 'jsn-delete'],
                        'roles' => [$moduleId . "::" . $controllerId . "::Delete"],
                    ],
                ]
            ]
        ];
        
        return ArrayHelper::merge($arr, parent::behaviors());
    }

    public function redirectOnSave($model, $ajax = false)
    {
    	if($ajax) return $this->renderJsn(true, "Saved.");
        else return $this->redirect(['update', 'id' => $model->id]);
    }
    
    /**
     * Override this method to set your custom search class.
     * @return string
     */
    public function getModelSeachClass(){
    	$searchClass = $this->getModelClass()."Search";
    	if(class_exists($searchClass))
        	return $this->getModelClass()."Search";
    	else 
    		return $this->getModelClass();
    }
    
    /**
     * Lists all models.
     * @return mixed
     */    
    public function actionIndex()
    {
        $class = $this->getModelSeachClass();        
        $searchModel = new $class;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
        $titles = $this->getObjectTitles();
        $this->view->title = Yii::t('app', $titles["plural"]);
		$this->view->params['breadcrumbs'][] = Yii::t('app', $this->view->title);
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
	public function actionJsnIndex()
    {        
        $class = $this->getModelSeachClass();        
        $searchModel = new $class;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
        $titles = $this->getObjectTitles();
        $this->view->title = Yii::t('app', $titles["plural"]);
		$this->view->params['breadcrumbs'][] = $this->view->title;
		
        return $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Creates a new Model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $class = $this->getModelClass();
        $model = new $class;
        $model->scenario = SActiveRecord::SCENARIO_INSERT;
    
        $saveResp = $this->saveModel($model);
        if($saveResp != false) return $saveResp;
    
		$titles = $this->getObjectTitles();
		$this->view->title = Yii::t('app', 'Create '.$titles["singular"]);
		$this->view->params['breadcrumbs'][] = ['label' => Yii::t('app', $titles["plural"]), 'url' => ['index']];
		$this->view->params['breadcrumbs'][] = $this->view->title;
		
		$viewname = $this->getViewName();
		if(!is_file($this->getViewPath()."/$viewname.php"))
			$viewname = "_form";
			
        return $this->render($viewname, [
            'model' => $model,
        ]);
    }
    
    /**
     * Creates a new Model ajax
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
	public function actionJsnCreate($predefine = array())
    {
        $class = $this->getModelClass();
        $model = new $class;
        $model->scenario = SActiveRecord::SCENARIO_INSERT;
        
        $saveResp = $this->saveModel($model, true);
        if($saveResp != false) return $saveResp;
    
        $post = Yii::$app->request->post();
        if(empty($post)){
			foreach ($predefine as $k=>$v){
	        	$model->{$k} = $v;
	        }
        }
        
		$titles = $this->getObjectTitles();
		$this->view->title = Yii::t('app', 'Create '.$titles["singular"]);
		$this->view->params['breadcrumbs'][] = ['label' => Yii::t('app', $titles["plural"]), 'url' => ['index']];
		$this->view->params['breadcrumbs'][] = $this->view->title;
		
		$viewname = $this->getViewName();
		if(!is_file($this->getViewPath()."/$viewname.php"))
			$viewname = "_jsn-form";
			
        return $this->renderAjax($viewname, [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'update' link.
     * @param string $id
     * @return mixed
     */   
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = SActiveRecord::SCENARIO_UPDATE;
    
        $saveResp = $this->saveModel($model);
        if($saveResp != false) return $saveResp;
    
		$titles = $this->getObjectTitles();
		$this->view->title = Yii::t('app', 'Edit '.$titles["singular"]);
		$this->view->params['breadcrumbs'][] = ['label' => Yii::t('app', $titles["plural"]), 'url' => ['index']];
		$this->view->params['breadcrumbs'][] = $this->view->title;
        
        $viewname = $this->getViewName();
		if(!is_file($this->getViewPath()."/$viewname.php"))
			$viewname = "_form";
			
        return $this->render($viewname, [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing model for ajax
     * If update is successful, the browser will be redirected to the 'update' link.
     * @param string $id
     * @return mixed
     */ 
	public function actionJsnUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = SActiveRecord::SCENARIO_UPDATE;
    
        $saveResp = $this->saveModel($model, true);
        if($saveResp != false) return $saveResp;
    
		$titles = $this->getObjectTitles();
		$this->view->title = \Yii::t('app', 'Edit '.$titles["singular"]);
		$this->view->params['breadcrumbs'][] = ['label' => \Yii::t('app', $titles["plural"]), 'url' => ['index']];
		$this->view->params['breadcrumbs'][] = $this->view->title;
		
        $viewname = $this->getViewName();
		if(!is_file($this->getViewPath()."/$viewname.php"))
			$viewname = "_jsn-form";
			
        return $this->renderAjax($viewname, [
            'model' => $model,
        ]);
    }
    
    /**
     * Preview
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $titles = $this->getObjectTitles();
        $this->view->title = \Yii::t('app', 'View '.$titles["singular"]);
        $this->view->params['breadcrumbs'][] = ['label' => \Yii::t('app', $titles["plural"]), 'url' => ['index']];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        
        $viewname = $this->getViewName();
        return $this->render($viewname, [
            'model' => $model,
        ]);
    }
    
    /**
     * Saves the model.
     * If update is successful, the browser will be redirected to the 'update' link.
     * @param string $id
     * @return mixed
     */
    public function saveModel($model, $ajax = false)
    {    
        $newRecord = $model->isNewRecord;
        if ($model->load(Yii::$app->request->post())){
            if($model->save()) {
            	if(!$ajax) Yii::$app->getSession()->setFlash('success', 'Changes are updated.');
                if($newRecord){
                	return $this->redirectOnSave($model, $ajax);
                }
                else { 
                    return $this->redirectOnSave($model, $ajax);
                }
                return true;
            }
            else
            {
               // var_dump($model->getErrors());die;
            }
            return false;
        }
        return false;
    }
    
    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' link.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->scenario = SActiveRecord::SCENARIO_DELETE;
        $model->delete();
    
        return $this->redirect(['index']);
    }

    public function actionJsnDelete($id)
    {
        $model = $this->findModel($id);
        $model->scenario = SActiveRecord::SCENARIO_DELETE;
        if($model->delete())
            return $this->renderJsn(true, "Deleted.");
    }
    
    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $class = $this->getModelClass();        
        $model = call_user_func(array($class, 'findOne'),['id'=>$id]);        
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested item does not exist.');
        }
    }
}