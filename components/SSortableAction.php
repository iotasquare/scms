<?php

namespace isqr\scms\components;

use yii\base\Action;
use yii\web\HttpException;
use yii\base\InvalidConfigException;

class SSortableAction extends Action {
    public $activeRecordClassName;
    public $subActiveRecordClassName;
    public $subKeyName = 'parent_id';
    public $orderColumn;

    public function init(){
        parent::init();
        if(!isset($this->activeRecordClassName)){
            throw new InvalidConfigException("You must specify the activeRecordClassName");
        }

        if(!isset($this->orderColumn)){
            throw new InvalidConfigException("You must specify the orderColumn");
        }
        if(!isset($this->subActiveRecordClassName)){
            $this->subActiveRecordClassName = $this->activeRecordClassName;
        }
    }
    public function run(){
        if(!\Yii::$app->request->isAjax){
            throw new HttpException(404);
        }
        
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $activeRecordClassName = $this->activeRecordClassName;
            $subActiveRecordClassName = $this->subActiveRecordClassName;
            foreach ($_POST['items'] as $i=>$item) {
                $model = $activeRecordClassName::findOne($item['key']);
                if(isset($item['children'])){
                    foreach ($item['children'] as $subi=>$subitem) {
                        $submodel = $subActiveRecordClassName::findOne($subitem['key']);
                        $submodel->updateAttributes([$this->subKeyName => $item['key'], $this->orderColumn => $subi]);
                    }
                }
                $arr = [$this->orderColumn => $i];

                if($model->hasAttribute($this->subKeyName))
                    $arr[$this->subKeyName] = null;
                    
                $model->updateAttributes($arr);
            }
        }
    }
}
