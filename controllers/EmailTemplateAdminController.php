<?php

namespace isqr\scms\controllers;

use isqr\scms\components\SCrudController;

class EmailTemplateAdminController extends  SCrudController
{
    public function getModelClass()
    {
        return 'isqr\scms\models\SEmailTemplate';
    }
    
	public function getObjectTitles()
    {
		return array(
				"singular" => "Email Template",
				"plural" => "Email Templates",
		);
    }
}
