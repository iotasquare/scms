<?php 
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->beginContent('@scms/views/layouts/admin/main.php'); ?>
		<div class="page-header">
			<div class="container-fluid">
				<h1></h1>
				<div class="actionbuttons">
					<?php $this->renderActionButtons(); ?>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<?= $content;?>
		</div>
<?php $this->endContent(); ?>
