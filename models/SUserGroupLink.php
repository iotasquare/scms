<?php

namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

class SUserGroupLink extends IUserGroupLink
{
    public $filter_group_id = '';

    public function rules()
    {
        $arr = [];
        return ArrayHelper::merge(parent::rules(), $arr);
    }

    public function attributeLabels()
    {
        $arr = [
            'user_id' => 'User',
        ];return ArrayHelper::merge(parent::attributeLabels(), $arr);
    }
    
    public function hints()
    {
        $arr = [];
        return ArrayHelper::merge(parent::hints(), $arr);
    }

    public function getUser(){
        return $this->hasOne(SUser::className(), ['id' => 'user_id']);
    }

    protected function searchQuery(){
        $query = self::find();
        $query->andFilterWhere( ['=','t.user_group_id', $this->filter_group_id] );
        return $query;
    }
    
}
