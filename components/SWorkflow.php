<?php 
namespace isqr\scms\components;

use Exception;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

abstract class SWorkflow extends Component {
    const STATE_START = 0;

    private static $_states = array(
        self::STATE_START => 'RFQ',
    );
    private static $_transitions;

    public static function applyAction($model, $event) {
        $transition = self::$_transitions[$model->state];
        
        if(!isset($transition[$event])) 
            throw new ForbiddenHttpException("Event '".$event."' failed. Current state is '" . self::$_states[$this->state]."'");
        
        $tx = Yii::$app->db->beginTransaction();
        try {
            $model->state = $transition[$event]['state'];
            $actions = $transition[$event]['actions'];
            foreach($actions as $k=>$v) {
                $tmp = ArrayHelper::merge($v, [
                    'model' => $model
                ]);
                $this->trigger($k, new SWorkflowEvent($tmp));
            }
            if(!$model->save()) 
                throw new Exception('Unable to save state.');
            $tx->commit();
        }
        catch(\Exception $ex) {
            $tx->rollBack();
            throw $ex;
        }
    }
}
?>
