<?php

namespace isqr\scms\controllers;

use Yii;
use isqr\scms\components\SAdminController;
use isqr\scms\models\SGeneralSettingForm;
use isqr\scms\models\SSocialSettingForm;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SAdminSettingController;

class SettingAdminController extends SAdminSettingController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
        ]);
    }

    public function actionGeneral()
    {
        $model = new SGeneralSettingForm();
        $this->view->title = "General Settings";
        return $this->process($model, "_".$this->getViewName());
    }
}
