<?php

namespace isqr\scms\controllers;

use Yii;
use isqr\scms\models\SUserGroupLink;
use isqr\scms\components\SActiveRecord;
use isqr\scms\components\SCrudController;

class UserGroupAdminController extends  SCrudController
{
    
    public function getModelClass()
    {
        return 'isqr\scms\models\SUserGroup';
    }    
	
	public function getObjectTitles()
    {
		return array(
            "singular" => "Group",
            "plural" => "Groups",
		);
    }

    public function actionJsnDeleteUserLink($id)
    {
        SUserGroupLink::findOne(['id'=>$id])->delete();
        return $this->renderJsn(true, 'deleted');
    }

    public function actionJsnAddUserLink($id)
    {
        $model = new SUserGroupLink();
        $model->scenario = SActiveRecord::SCENARIO_INSERT;
        $model->user_group_id = $id;
        $saveResp = $this->saveModel($model, true);
        if($saveResp != false) return $saveResp;
        return $this->renderAjax('_jsn-form-user-link', [
            'model' => $model,
        ]);
    }

    public function actionJsnSetPermissions($id)
    {
        $model = $this->findModel($id);
        $model->scenario = SActiveRecord::SCENARIO_UPDATE;
        $saveResp = $this->saveModel($model, true);
        if($saveResp != false) return $saveResp;
        return $this->renderAjax('_jsn-form-permissions', [
            'model' => $model,
        ]);
    }
    
}
