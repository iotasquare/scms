<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_tasks".
 *
 * @property string $id
 * @property string $cmd
 * @property string $last_start_time
 * @property string $last_reporting_time
 * @property string $last_end_time
 * @property integer $enabled
 */

 
class ITask extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $arr = [
            [['id', 'last_start_time'], 'required'],
            [['last_start_time', 'last_reporting_time', 'last_end_time', 'enabled'], 'integer'],
            [['id'], 'string', 'max' => 50],
            [['cmd'], 'string', 'max' => 250],
            [['enabled'], 'default', 'value' => 1],
        ];
        return ArrayHelper::merge(parent::rules(), $arr);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $arr = [
            'id' => 'ID',
            'cmd' => 'Cmd',
            'last_start_time' => 'Last Start Time',
            'last_reporting_time' => 'Last Reporting Time',
            'last_end_time' => 'Last End Time',
            'enabled' => 'Enabled',
        ];
        return ArrayHelper::merge(parent::attributeLabels(), $arr);
    }
    
    /**
     * @inheritdoc
     */
    public function hints()
    {
        $arr = [
            'id' => 'ID',
            'cmd' => 'Cmd',
            'last_start_time' => 'Last Start Time',
            'last_reporting_time' => 'Last Reporting Time',
            'last_end_time' => 'Last End Time',
            'enabled' => 'Enabled',
        ];
        return ArrayHelper::merge(parent::hints(), $arr);
    }
    
}
