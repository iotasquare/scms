<?php
namespace isqr\scms;

use Yii;
use yii\helpers\Url;
use isqr\scms\components\SGlobal;

final class Plugin extends SPlugin
{
    public function load(SApp $app){
        $config = [];

        $admin_menu = [];
        $admin_menu[] = array('label'=>'<i class="fa fa-tachometer-alt"></i><span>Dashboard</span>','url'=>['/scms/default-admin/index'],'visible'=>true, 'sort'=>0);
        $admin_menu['cms'] = array('label'=>'<i class="fa fa-desktop"></i><span>CMS</span>','url'=>'#','items'=>array(
            array('label'=>'<i class=""></i><span>Pages</span>','url'=>['/scms/page-admin/index'],'visible'=>true),
        	array('label'=>'<i class=""></i><span>Media Library</span>','url'=>['/scms/media-library-admin/index'],'visible'=>true),
        ),'visible'=>true, 'sort'=>500);
        $admin_menu['configurations'] = array('label'=>'<i class="fa fa-cog"></i><span>Configurations</span>','url'=>'#','items'=>array(
            array('label'=>'<i class=""></i><span>General Settings</span>','url'=>['/scms/setting-admin/general'],'visible'=>true),
            array('label'=>'<i class=""></i><span>Email Templates</span>','url'=>['/scms/email-template-admin/index'],'visible'=>true),
        ),'visible'=>true, 'sort'=>600);
        $admin_menu['users'] = array('label'=>'<i class="fa fa-users"></i><span>User Management</span>','url'=>'#','items'=>array(
            array('label'=>'<i class=""></i><span>Groups</span>','url'=>['/scms/user-group-admin/index'],'visible'=>true),
            array('label'=>'<i class=""></i><span>Users</span>','url'=>['/scms/user-admin/index'],'visible'=>true),
        ),'visible'=>true, 'sort'=>500);

        $admin_dashboard_items = [];
        $admin_dashboard_items['scms_total_active_users'] = [
        	'class' => 'isqr\scms\widgets\dashboard_active_users\TotalActiveUsers',
        	'sort' => 10,
        	'params' =>[
        		'title' => 'Active Users'		
        	]
		];

        $restricted_access = [];
        $restricted_access['scms_admin'] = [
            "resource" => "Admin Panel",
            "permissions" => [
                'scms::DefaultAdmin::*' => 'Administrative Access', 
                'scms::SettingAdmin::*' => 'System Settings', 
            ]
        ];
        
        $restricted_access['scms_page'] = [
            "resource" => "Page",
            "permissions" => [
                'scms::PageAdmin::Create' => 'Create', 
                'scms::PageAdmin::Update' => 'Update', 
                'scms::PageAdmin::Delete' => 'Delete'
            ]
        ];
        $restricted_access['scms_usergroup'] = [
            "resource" => "User Group",
            "permissions" => [
                'scms::UserGroupAdmin::Create' => 'Create', 
                'scms::UserGroupAdmin::Update' => 'Update', 
                'scms::UserGroupAdmin::Delete' => 'Delete', 
                'scms::UserGroupAdmin::View' => 'View'
            ]
        ];
        $restricted_access['scms_users'] = [
            "resource" => "User",
            "permissions" => [
                'scms::UserAdmin::Create' => 'Create', 
                'scms::UserAdmin::Update' => 'Update', 
                'scms::UserAdmin::Delete' => 'Delete', 
                'scms::UserAdmin::View' => 'View'
            ]
        ];

        $email_template_codes = [];
        $email_template_codes['user_welcome'] = [
            'name' => 'User welcome',
            'params' => [
                'quick_login_link' => 'Login Link',
                'quick_login_url' => 'Login Url',
            ]
        ];
        $email_template_codes['user_login_different_location'] = [
            'name' => 'User login from different location',
            'params' => [
                'user_profile_link' => 'User Profile Link',
                'user_profile_url' => 'User Profile Url',
                'current_location' => 'Current location'
            ]
        ];
        $email_template_codes['user_device_verification'] = [
            'name' => 'User device verification',
            'params' => [
                'activate_device_url' => 'Activation Url (Serves Image)',
                'verify_device_url' => 'Device Verification Url',
                'verify_device_button' => 'Device Verification Button'
            ]
        ];
        $email_template_codes['user_password_reset'] = [
            'name' => 'User reset password',
            'params' => [
                'recovery_link' => 'Recovery Link',
                'recovery_url' => 'Recovery Url',
            ]
        ];        
        $email_template_codes['user_force_password_reset'] = [
            'name' => 'User is asked to reset password',
            'params' => [
                'recovery_link' => 'Recovery Link',
                'recovery_url' => 'Recovery Url',
            ]
        ];

        $activity_codes = [];
        $activity_codes['user_login'] = [
            'template' => '{{user_link}} logged in.'
        ];

        $activity_codes['user_logout'] = [
            'template' => '{{user_link}} logged out.'
        ];

        $activity_codes['user_login_other_location'] = [
            'template' => '{{user_link}} logged in from a new location {{location}}'
        ];

        $activity_codes['user_profile_update'] = [
            'template' => '{{user_link}} updated the profile.'
        ];
        
        $activity_codes['user_change_password'] = [
            'template' => '{{user_link}} changed the password.'
        ];

        $activity_codes['user_account_recovery_attempt'] = [
            'template' => 'Someone has tried to recover account registered with email "{{email}}" from location "{{location}}".'
        ];

        return [
            'admin_menu' => $admin_menu,
            'admin_dashboard_widgets' => $admin_dashboard_items,
            'restricted_access' => $restricted_access,
            'email_template_codes' => $email_template_codes,
            'activity_codes' => $activity_codes,
        ];
    }
}

