<?php

namespace isqr\scms\controllers;

use Yii;
use isqr\scms\components\SGlobal;
use isqr\scms\components\SCrudController;
use isqr\scms\models\SUser;

class UserAdminController extends  SCrudController
{
    public function getModelClass()
    {
        $userModel = SGlobal::getAppParam("userModel", 'isqr\scms\models\SUser');
        return $userModel;
    }    
	
	public function getObjectTitles()
    {
		return array(
				"singular" => "User",
				"plural" => "Users",
		);
    }

    public function actionCheckAvailability($email) {
        $user = SUser::find()
        ->where(['email'=>$email])
        ->one();
        if($user != null) {
            if($user->deleted == 1)
                return $this->renderJsn(false, "The email is already registered as an existing user on the system but deleted.", [$user->id, true]);
            else 
                return $this->renderJsn(false, "The email is already registered as an existing user on the system.", [$user->id, false]);
        }
        return $this->renderJsn(true, "User can be created.");
    }

    public function actionReactivateUser($id) {
        $user = SUser::find()
        ->where(['id' => $id])
        ->one();
        if($user != null) {
            $user->deleted = 0;
            $user->deleted_user_id = 0;
            $user->deleted_time = null;
            if($user->save()){
                return $this->renderJsn(true, "User is activated now. You can search this user and modify it.");
            }
        }
        return $this->renderJsn(false, "Unable to activate this user.");
    }
}
