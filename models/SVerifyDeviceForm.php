<?php
namespace isqr\scms\models;
use yii;
use isqr\scms\components\SFormModel;
use isqr\scms\components\SGlobal;
use isqr\scms\models\SUser;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for Device Verification.
 *
 * @property SUser $_identity
 */
class SVerifyDeviceForm extends SFormModel
{
	public $code; 

	public function configure() {
        $rules =  [
			['code', 'string'],
        ];

        $attributeLabels =  [
            'code' => 'Verification Code',
        ];
        
        $hints =  [
            'code' => 'Enter the verification Code you recieved in email.',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
	}
	
	public function verify($device)
	{
		if($device == null) {
			$this->addError('code','Please open the link in browser from which you tried to login.');
			$this->addError('view_code',-3); 
			return false;
		}
		if($device->active == 0) {
			$this->addError('code','Device is not activated for verification. Please click on "Display Images" in the email.'); 
			$this->addError('view_code',-2); 
			return false;
		}
		$deviceVerificationActivationRequired = SGlobal::getAppParam("device_verification_activation_required", false);
		if($deviceVerificationActivationRequired == true) {
			if(!$device->isValid()){
				$this->addError('code','The verification code is expired. Please try again.');
				$this->addError('view_code',-1);
				return false;
			}
		}
		elseif($device->pin == $this->code) {
			$user = SUser::findOne($device->user_id);
			$device->createHashKey();
			if($device->save()){
				return $user->login($device);
			}
		}
		else
			$this->addError('code','The verification code is not valid.');
		return false;
	}
}