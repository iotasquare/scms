<?php
use yii\helpers\Url;
?>
<div class="<?=$css?>">
	<div class="title">
		<div class="tile-heading">Total Active Users</div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h2 class="pull-right"><?=$usersCount?></h2>
		</div>
		<div class="tile-footer"><a href="<?=Url::toRoute("/scms/user-admin/index")?>">View More</a></div>
	</div>
</div>