<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use isqr\scms\components\SGlobal;
use isqr\scms\components\SEnum;
use yii\base\Event;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableAjaxValidation' => false,
    'options' => [
    ]
])?>
<div class="row">
<div class="col-md-6">
	<?php $allowSignup = SGlobal::getAppParam("userAllowSignup", false);?>
	<?php if($allowSignup): ?>
	<h4><?=Yii::t('app', 'Don\'t have an account?');?> <small><a href="<?php echo Url::toRoute(['user/signup']);?>" style="color: #999999"><?=Yii::t('app', 'Create Account');?></a></small></h4>
	<?php endif;?>
	<p><?=Yii::t('app', 'If you are an existing user, login using your e-mail address and password.');?></p>
    
    <?= $form->field($model, 'email')->textInput(["placeholder"=>"Email"])->label(true)?>
    <?= $form->field($model, 'password')->passwordInput(["placeholder"=>"Password"])->label(true)?>
    <p><a href="<?php echo Url::toRoute(['user/reset-password']);?>"
		class="hint"><?=Yii::t('app', 'Have you forgotten your password?');?></a>
	</p>
    
    <p>
    <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn  btn-primary'])?> 
    </p>
</div>
</div>
<?php ActiveForm::end()?>