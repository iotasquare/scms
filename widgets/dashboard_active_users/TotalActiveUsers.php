<?php
namespace isqr\scms\widgets\dashboard_active_users;
use isqr\scms\SWidget;
use isqr\scms\components\SEnum;
use isqr\scms\models\SUser;

class TotalActiveUsers extends SWidget
{
    public $css = "col-md-3";
    
    protected function typeDefault()
    {        
        $usersCount = SUser::find()
        ->andWhere(["status" => SUser::USER_STATUS_ACTIVE])
        ->count("*");
        return $this->render($this->type, [
            'css' => $this->css,
            'usersCount' => $usersCount    
        ]);
    }
}
?>