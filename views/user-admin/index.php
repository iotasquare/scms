<?php

use isqr\scms\components\SGridFieldView;
use isqr\scms\widgets\adminui\actionbar\ActionBar;
use isqr\scms\widgets\adminui\block\Block;
use yii\grid\GridView;
use isqr\scms\models\SUserGroup;
use isqr\scms\components\SEnum;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\helpers\Url;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\web\View;
use yii\widgets\Pjax;
use isqr\scms\widgets\adminui\box\Box;
use isqr\scms\components\SGridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php $this->addActionPopupButton("Add User", ["jsn-create"], 'popup_create', "Add User", 'userUpdateOnSuccess', 'plus');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List', 'type'=>'tabular'])?>
<?php Pjax::begin(array("id"=>"pjx_list"));?>
<?php $groups = SUserGroup::find()->all();
?>
<?= SGridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
		'fullName',
		'email',
		[
			'header' => 'Groups',
			'value' => function($model){
				$Html = "";
				foreach($model->groups as $group){
					$Html .= '<a href="'. Url::toRoute(["/scms/user-group-admin/view", 'id'=>$group->id]) . '"><span class="label label-default">'.$group->name."</span></a> ";
				}
				return trim($Html);
			},
			'format' => 'raw',
		],
		[
            'attribute'=>'Status',
            'contentOptions'=>array('width'=>100)
        ],
		[
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'edit' => function ($url, $model) {
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>', "#", array(
						"onclick"=> "return showPopup('popup_user_create', 'Edit User', '".Url::toRoute(["jsn-update", 'id'=>$model->id])."', userUpdateOnSuccess);",
						"class" => ""
					));
                }
            ],
            'template' => '{edit} {delete}',
			'contentOptions'=>array('width'=>100, 'class'=>'text-center')
        ]
	],
]); ?>
<?php Pjax::end();?>
<?php Box::end();?>
</div>

<?php $this->beginBlock('searchform') ?>
	<?php $form = ActiveForm::begin(['method'=>'get']); ?>
		<?= $form->field($searchModel, 'filter_name')->textInput(); ?>
		<?= $form->field($searchModel, 'filter_email')->textInput(); ?>
		<?= $form->field($searchModel, 'filter_group_id')->dropDownList(ArrayHelper::map($groups, 'id', 'name'),
				['class'=>'form-control','prompt' => 'Any Group']) ?>
		<?= $form->field($searchModel, 'filter_status')->dropDownList(SUserGroup::$defaultStatuses,
                ['class'=>'form-control','prompt' => 'Any Status']) ?>
		<?= Html::submitButton('Apply', ['class' => 'btn btn-sm btn-primary']) ?>
		<?= Html::a('Clear', ['index'], ['class' => 'btn btn-sm btn-default']) ?>
	<?php ActiveForm::end(); ?>
<?php $this->endBlock() ?>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
function userUpdateOnSuccess(){
	$.pjax.reload({container:"#pjx_list"}); 
}
</script>
<?php JSRegister::end();?>