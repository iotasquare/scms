<?php
namespace isqr\scms\controllers;

use isqr\scms\components\SController;
use isqr\scms\models\SPage;
use yii\web\NotFoundHttpException;
use isqr\scms\components\SEnum;
use isqr\scms\components\SGlobal;
use isqr\scms\models\SUser;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class PageController extends SController
{
    public function behaviors()
    {
        $arr = [
            'access' => [
                'rules' => [	
					array(
						'actions' => ['view'],
						'allow' => true,
                        'roles' => ['?', '@'],
					),
                ],
            ]
		];
		
        return ArrayHelper::merge(parent::behaviors(), $arr);
    }
    
    public function actionView($code = 'home')
    {
        $page = SPage::find()
        ->andWhere(['code' => $code])->one();
        
        if ($page == null) {
            throw new NotFoundHttpException("The requested page is not found or published publically.", 404);
        }
        
        if($page->status != SPage::STATUS_ACTIVE && !SGlobal::isAdminUser())
            throw new HttpException(401,"The requested page is not published publically.", 401);
        
        $this->view->title = $page->name;
        $this->view->pageTitle = $page->name;
        return $this->render($this->getViewName(), array(
            'page' => $page
        ));
    }
}