<?php

use isqr\scms\components\SGridFieldView;
use isqr\scms\widgets\adminui\actionbar\ActionBar;
use isqr\scms\widgets\adminui\block\Block;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Html;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\web\View;
use isqr\scms\widgets\adminui\box\Box;
use app\scms\plugins\qltslms\models\Task;
use app\scms\plugins\qltslms\models\Resource;
use app\scms\plugins\qltslms\models\Course;
use app\scms\plugins\qltslms\models\Module;
use app\scms\plugins\qltslms\models\CourseUserLink;
use yii\data\ActiveDataProvider;
use isqr\scms\components\SGridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php $this->addActionButton("Back", ["index"], 'reply');?>
<?php $this->addActionPopupButton("Edit User", ["jsn-update", 'id'=>$model->id], 'popup_create', "Edit User", 'userCreateOnSuccess', 'pencil');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'Details'])?>
<?php Pjax::begin(array("id"=>"pjx_userdetail"));?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
		'id',
        'fullName',
        'Location',
        [
			'label' => 'Groups',
			'value' => function($model){
				$Html = "";
				foreach($model->groups as $group){
					$Html .= '<span class="label label-default">'.$group->name."</span> ";
				}
				return trim($Html);
			},
			'format' => 'raw',
		],
		[
            'label' => 'Created At',
            'value' => function($model){
                $html = "";
                $userName = '(not set)';
                if($model->createdByUser != null)
                    $userName = $model->createdByUser->name;
                $html .= date("d-m-Y H:i:s", strtotime($model->created_time)) . " by <strong>" . $userName ."</strong>";
                return $html;
            },
            'format' => 'raw'
        ],
        [
            'label' => 'Last Updated At',
            'value' => function($model){
                $html = "";
                $userName = '(not set)';
                if($model->lastUpdatedByUser != null)
                    $userName = $model->lastUpdatedByUser->name;
                $html .= date("d-m-Y H:i:s", strtotime($model->last_updated_time)) . " by <strong>" . $userName ."</strong>";
                return $html;
            },
            'format' => 'raw'
        ],
    ],
]);
?>
<?php Pjax::end();?>
<?php Box::end();?>
</div>
<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
function userCreateOnSuccess(){
	$.pjax.reload({container:"#pjx_userdetail"}); 
}
</script>
<?php JSRegister::end();?>