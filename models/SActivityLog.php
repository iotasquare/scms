<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SGlobal;
use app\scms\plugins\qltslms\models\Module;
use yii\console\Exception;
use isqr\scms\components\SEnum;

class SActivityLog extends IActivityLog
{
    public function configure()
    {
        $rules = [
        ];
        $attributeLabels = [
        ];
        $hints = [
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
    }

    public static function log($code, $model = []) {
        $user = SGlobal::getCurrentUser();
        $device = SGlobal::getCurrentDevice();
        $activity = new SActivityLog();
        $activity->code = $code;
        $activity->user_id = $user != null ? $user->id : '0';
        $activity->device_id = $device != null ? $device->id : null;
        $activity->activity_time = date("Y-m-d H:i:s");
        if(is_array($model)){
            $activity->json_params = json_encode($model, true);
        }
        elseif($model != null) {
            $activity->table_name = $model->getTableName();
            $activity->table_object_id = $model->id;
        }
        if(!$activity->save()) {
            print_r($activity->getErrors()); die;
        }
    }

    public function getUser()
    {
        return $this->hasOne(SUser::className(), ['id' => 'user_id']);;
    }

    public function getText() {
        $activity_codes = Yii::$app->scmsApp->getConfig("activity_codes");
        if(isset($activity_codes[$this->code]) && isset($activity_codes[$this->code]['template'])) {
            $template = $activity_codes[$this->code]['template'];

            if(is_callable($template)){
                $content = $template($this);
            }
            else {
                $content = $template;
            }

            if($this->json_params != null) {
                $params = json_decode($this->json_params, true);
            }
            else {
                $params = [];
            }
            if($this->user != null)
                $params['user_link'] = '<a target="_blank" href="'.$this->user->getProfileUrl().'">'.$this->user->getFullName().'</a>';

            foreach($params as $k=>$v) {
                $content = str_ireplace('{{'.$k.'}}', $v, $content);
            }
            
            return $content;
        }
        else {
            if($this->json_params != "")
                return $this->code . " => " . $this->json_params;
            else 
                return $this->code;
        }
    }
}
