<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_auth".
 *
 * @property string $id
 * @property string $user_id
 * @property string $source
 * @property string $source_id
 */
class IAuth extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $arr = [
            [['user_id', 'source', 'source_id'], 'required'],
            [['user_id'], 'integer'],
            [['source'], 'string', 'max' => 100],
            [['source_id'], 'string', 'max' => 200],
        ];
        return ArrayHelper::merge(parent::rules(), $arr);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $arr = [
            'id' => 'ID',
            'user_id' => 'User ID',
            'source' => 'Source',
            'source_id' => 'Source ID',
        ];
        return ArrayHelper::merge(parent::attributeLabels(), $arr);
    }
    
    /**
     * @inheritdoc
     */
    public function hints()
    {
        $arr = [
            'id' => 'ID',
            'user_id' => 'User ID',
            'source' => 'Source',
            'source_id' => 'Source ID',
        ];
        return ArrayHelper::merge(parent::hints(), $arr);
    }
    
}
