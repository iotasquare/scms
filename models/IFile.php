<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_files".
 *
 * @property string $id
 * @property string $original_name
 * @property string $filename
 * @property string $filepath
 * @property string $mime_type
 * @property string $extension
 */
class IFile extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_files';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['filepath', 'mime_type', 'extension'], 'required'],
            [['original_name', 'filename', 'filepath'], 'string', 'max' => 100],
            [['mime_type'], 'string', 'max' => 50],
            [['extension'], 'string', 'max' => 10],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'original_name' => 'Original Name',
            'filename' => 'Filename',
            'filepath' => 'Filepath',
            'mime_type' => 'Mime Type',
            'extension' => 'Extension',
        ];
        $hints = [
            'id' => 'ID',
            'original_name' => 'Original Name',
            'filename' => 'Filename',
            'filepath' => 'Filepath',
            'mime_type' => 'Mime Type',
            'extension' => 'Extension',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }
    
}
