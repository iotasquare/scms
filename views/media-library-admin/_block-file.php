<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\web\JsExpression;
?>
<a class="image" 
    style="background-image:url(<?= $model->getUrl();?>);" 
    onclick="window.parent.selectfile('<?= $model->id;?>', '<?= $model->getUrl();?>');"
></a>
<div class="caption text-center">
    <p style="word-wrap: break-word;"><?= $model->original_name;?></p>
    <div class="actionbuttons">
        <a class="btn btn-xs btn-default" href="#" onclick="return medialibrary_rename_file(<?=$model->id?>);"><i class="fa fa-edit"></i></a>
        <a class="btn btn-xs btn-default" href="<?= $model->getUrl();?>" target="_blank" data-pjax="0"><i class="fa fa-eye"></i></a>
        <?= Html::a("<i class='fa fa fa-trash'></i>", "#", array(
            "onclick"=> "return ajax_post('".Url::toRoute(["jsn-delete", 'id'=>$model->id])."', {}, mediaUpdateOnSuccess, 'Are you sure you want to delete this file?');",
            "class" => "btn btn-xs btn-default"
        ));?>
    </div>
</div>