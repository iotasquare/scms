<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use isqr\scms\components\SGlobal;
?>
<?php
$form = ActiveForm::begin([
    'id' => 'profileedit-form',
    'enableAjaxValidation' => false,
    'options' => [
        'enctype'=>'multipart/form-data'
    ]
])?>
    <div class="row">
        <div class="col-md-3 profile-picture">
            <div class="form-group">
                <img class="img-responsive  img-thumbnail "  src="<?php echo $model->getProfileImageUrl($this->context->appAsset); ?>" />
            </div>
        </div>
        <div class="col-md-6" style="padding-top: 20px">
            <?php echo $form->field($model,'file')->fileInput([])->hint('Please upload square image of dimension (400 x 400)');  ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'name');?>
        </div>
        <div class="col-md-6">
        <?= $form->field($model, 'surname');?>
        </div>
    </div>
    
    
    <?= $form->field($model, 'username')->textInput();?>
    
    <?= $form->field($model, 'intro')->textInput();?>
    <?= $form->field($model, 'about')->textArea(["rows"=>3]);?>
    <?= $form->field($model, 'gender')->inline()->radioList(array(0=>'Male',1=>'Female'));?>
    
    <?= $form->field($model, 'address')->textArea(["rows"=>2]);?>
    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'country');?>
        </div>
        <div class="col-md-6">
        <?= $form->field($model, 'city');?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'phone_number');?>
        </div>
        <div class="col-md-6">
        <?= $form->field($model, 'mobile_number');?>
        </div>
    </div>
	<?= Html::submitButton('Save', ['class' => 'btn btn-primary'])?>
<?php ActiveForm::end()?>