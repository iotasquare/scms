<?php
namespace isqr\scms\components;

use DateTime;
use DateTimeZone;
use isqr\scms\models\SEmailTemplate;
use Yii;
use isqr\scms\models\SSetting;
use isqr\scms\models\SUser;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;
use isqr\scms\models\STemplate;
use yii\imagine\Image;

abstract class SGlobal
{		
	public static function getYearList($start = 1940, $end = null, $order = "ASC"){
		$yearlist = array();
		if($end == null)
			$end = date("Y");
		
		if($order == "ASC"){
			$year = $end;
			for ($year; $year>=$start; $year--)
				$yearlist[$year] = $year;
		}
		else{
			$year = $start;
			for ($year; $year<=$end; $year++)
				$yearlist[$year] = $year;
		}
		
		return $yearlist;
	}
	
	public static function getMonthList($format = 'F'){
		$monthlist = array();
		$month = 1;
		for ($month = 1; $month<=12; $month++)
			$monthlist[$month] = date($format, mktime(0,0,0,$month));
		return $monthlist;
	}
	
	public static function getTimeList($stepInMinutes = 30, $format = 'h:i A'){
		$timelist = array();
		for ($time = 0; $time <= 24*60; $time += $stepInMinutes){
			$t = mktime(0,$time,0,0);
			$timelist[date("H:i:s", $t)] = date($format, $t);
		}
		return $timelist;
	}
	
	public static function getCountryList(){
		$countryList = array( "AF" => "Afghanistan", "AL" => "Albania", "DZ" => "Algeria", "AS" => "American Samoa", "AD" => "Andorra", "AO" => "Angola", "AI" => "Anguilla", "AQ" => "Antarctica", "AG" => "Antigua and Barbuda", "AR" => "Argentina", "AM" => "Armenia", "AW" => "Aruba", "AU" => "Australia", "AT" => "Austria", "AZ" => "Azerbaijan", "BS" => "Bahamas", "BH" => "Bahrain", "BD" => "Bangladesh", "BB" => "Barbados", "BY" => "Belarus", "BE" => "Belgium", "BZ" => "Belize", "BJ" => "Benin", "BM" => "Bermuda", "BT" => "Bhutan", "BO" => "Bolivia", "BA" => "Bosnia and Herzegovina", "BW" => "Botswana", "BV" => "Bouvet Island", "BR" => "Brazil", "BQ" => "British Antarctic Territory", "IO" => "British Indian Ocean Territory", "VG" => "British Virgin Islands", "BN" => "Brunei", "BG" => "Bulgaria", "BF" => "Burkina Faso", "BI" => "Burundi", "KH" => "Cambodia", "CM" => "Cameroon", "CA" => "Canada", "CT" => "Canton and Enderbury Islands", "CV" => "Cape Verde", "KY" => "Cayman Islands", "CF" => "Central African Republic", "TD" => "Chad", "CL" => "Chile", "CN" => "China", "CX" => "Christmas Island", "CC" => "Cocos [Keeling] Islands", "CO" => "Colombia", "KM" => "Comoros", "CG" => "Congo - Brazzaville", "CD" => "Congo - Kinshasa", "CK" => "Cook Islands", "CR" => "Costa Rica", "HR" => "Croatia", "CU" => "Cuba", "CY" => "Cyprus", "CZ" => "Czech Republic", "CI" => "C�te d�Ivoire", "DK" => "Denmark", "DJ" => "Djibouti", "DM" => "Dominica", "DO" => "Dominican Republic", "NQ" => "Dronning Maud Land", "DD" => "East Germany", "EC" => "Ecuador", "EG" => "Egypt", "SV" => "El Salvador", "GQ" => "Equatorial Guinea", "ER" => "Eritrea", "EE" => "Estonia", "ET" => "Ethiopia", "FK" => "Falkland Islands", "FO" => "Faroe Islands", "FJ" => "Fiji", "FI" => "Finland", "FR" => "France", "GF" => "French Guiana", "PF" => "French Polynesia", "TF" => "French Southern Territories", "FQ" => "French Southern and Antarctic Territories", "GA" => "Gabon", "GM" => "Gambia", "GE" => "Georgia", "DE" => "Germany", "GH" => "Ghana", "GI" => "Gibraltar", "GR" => "Greece", "GL" => "Greenland", "GD" => "Grenada", "GP" => "Guadeloupe", "GU" => "Guam", "GT" => "Guatemala", "GG" => "Guernsey", "GN" => "Guinea", "GW" => "Guinea-Bissau", "GY" => "Guyana", "HT" => "Haiti", "HM" => "Heard Island and McDonald Islands", "HN" => "Honduras", "HK" => "Hong Kong SAR China", "HU" => "Hungary", "IS" => "Iceland", "IN" => "India", "ID" => "Indonesia", "IR" => "Iran", "IQ" => "Iraq", "IE" => "Ireland", "IM" => "Isle of Man", "IL" => "Israel", "IT" => "Italy", "JM" => "Jamaica", "JP" => "Japan", "JE" => "Jersey", "JT" => "Johnston Island", "JO" => "Jordan", "KZ" => "Kazakhstan", "KE" => "Kenya", "KI" => "Kiribati", "KW" => "Kuwait", "KG" => "Kyrgyzstan", "LA" => "Laos", "LV" => "Latvia", "LB" => "Lebanon", "LS" => "Lesotho", "LR" => "Liberia", "LY" => "Libya", "LI" => "Liechtenstein", "LT" => "Lithuania", "LU" => "Luxembourg", "MO" => "Macau SAR China", "MK" => "Macedonia", "MG" => "Madagascar", "MW" => "Malawi", "MY" => "Malaysia", "MV" => "Maldives", "ML" => "Mali", "MT" => "Malta", "MH" => "Marshall Islands", "MQ" => "Martinique", "MR" => "Mauritania", "MU" => "Mauritius", "YT" => "Mayotte", "FX" => "Metropolitan France", "MX" => "Mexico", "FM" => "Micronesia", "MI" => "Midway Islands", "MD" => "Moldova", "MC" => "Monaco", "MN" => "Mongolia", "ME" => "Montenegro", "MS" => "Montserrat", "MA" => "Morocco", "MZ" => "Mozambique", "MM" => "Myanmar [Burma]", "NA" => "Namibia", "NR" => "Nauru", "NP" => "Nepal", "NL" => "Netherlands", "AN" => "Netherlands Antilles", "NT" => "Neutral Zone", "NC" => "New Caledonia", "NZ" => "New Zealand", "NI" => "Nicaragua", "NE" => "Niger", "NG" => "Nigeria", "NU" => "Niue", "NF" => "Norfolk Island", "KP" => "North Korea", "VD" => "North Vietnam", "MP" => "Northern Mariana Islands", "NO" => "Norway", "OM" => "Oman", "PC" => "Pacific Islands Trust Territory", "PK" => "Pakistan", "PW" => "Palau", "PS" => "Palestinian Territories", "PA" => "Panama", "PZ" => "Panama Canal Zone", "PG" => "Papua New Guinea", "PY" => "Paraguay", "YD" => "People's Democratic Republic of Yemen", "PE" => "Peru", "PH" => "Philippines", "PN" => "Pitcairn Islands", "PL" => "Poland", "PT" => "Portugal", "PR" => "Puerto Rico", "QA" => "Qatar", "RO" => "Romania", "RU" => "Russia", "RW" => "Rwanda", "RE" => "R�union", "BL" => "Saint Barth�lemy", "SH" => "Saint Helena", "KN" => "Saint Kitts and Nevis", "LC" => "Saint Lucia", "MF" => "Saint Martin", "PM" => "Saint Pierre and Miquelon", "VC" => "Saint Vincent and the Grenadines", "WS" => "Samoa", "SM" => "San Marino", "SA" => "Saudi Arabia", "SN" => "Senegal", "RS" => "Serbia", "CS" => "Serbia and Montenegro", "SC" => "Seychelles", "SL" => "Sierra Leone", "SG" => "Singapore", "SK" => "Slovakia", "SI" => "Slovenia", "SB" => "Solomon Islands", "SO" => "Somalia", "ZA" => "South Africa", "GS" => "South Georgia and the South Sandwich Islands", "KR" => "South Korea", "ES" => "Spain", "LK" => "Sri Lanka", "SD" => "Sudan", "SR" => "Suriname", "SJ" => "Svalbard and Jan Mayen", "SZ" => "Swaziland", "SE" => "Sweden", "CH" => "Switzerland", "SY" => "Syria", "ST" => "S�o Tom� and Pr�ncipe", "TW" => "Taiwan", "TJ" => "Tajikistan", "TZ" => "Tanzania", "TH" => "Thailand", "TL" => "Timor-Leste", "TG" => "Togo", "TK" => "Tokelau", "TO" => "Tonga", "TT" => "Trinidad and Tobago", "TN" => "Tunisia", "TR" => "Turkey", "TM" => "Turkmenistan", "TC" => "Turks and Caicos Islands", "TV" => "Tuvalu", "UM" => "U.S. Minor Outlying Islands", "PU" => "U.S. Miscellaneous Pacific Islands", "VI" => "U.S. Virgin Islands", "UG" => "Uganda", "UA" => "Ukraine", "SU" => "Union of Soviet Socialist Republics", "AE" => "United Arab Emirates", "GB" => "United Kingdom", "US" => "United States", "UY" => "Uruguay", "UZ" => "Uzbekistan", "VU" => "Vanuatu", "VA" => "Vatican City", "VE" => "Venezuela", "VN" => "Vietnam", "WK" => "Wake Island", "WF" => "Wallis and Futuna", "EH" => "Western Sahara", "YE" => "Yemen", "ZM" => "Zambia", "ZW" => "Zimbabwe");
		return $countryList;
	}
	
	public static function getTimezoneList(){
		$timezoneIdentifiers = \DateTimeZone::listIdentifiers();
	    $utcTime = new \DateTime('now', new \DateTimeZone('UTC'));
	 
	    $tempTimezones = array();
	    foreach ($timezoneIdentifiers as $timezoneIdentifier) {
	        $currentTimezone = new \DateTimeZone($timezoneIdentifier);
	 
	        $tempTimezones[] = array(
	            'offset' => (int)$currentTimezone->getOffset($utcTime),
	            'identifier' => $timezoneIdentifier
	        );
	    }
	 
	    // Sort the array by offset,identifier ascending
	    usort($tempTimezones, function($a, $b) {
			return ($a['offset'] == $b['offset'])
				? strcmp($a['identifier'], $b['identifier'])
				: $a['offset'] - $b['offset'];
	    });
	 
		$timezoneList = array();
	    foreach ($tempTimezones as $tz) {
			$sign = ($tz['offset'] > 0) ? '+' : '-';
			$offset = gmdate('H:i', abs($tz['offset']));
	        $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
				$tz['identifier'];
	    }
	 
	    return $timezoneList;
	}

	public static function timeAgo($date,$granularity=2) {
	    $date = strtotime($date);
	    $difference = time() - $date;
	    $periods = array('decade' => 315360000,
	        'year' => 31536000,
	        'month' => 2628000,
	        'week' => 604800, 
	        'day' => 86400,
	        'hour' => 3600,
	        'minute' => 60,
	        'second' => 1);
		$retval = '';
	    foreach ($periods as $key => $value) {
	        if ($difference >= $value) {
	            $time = floor($difference/$value);
	            $difference %= $value;
	            $retval .= ($retval ? ' ' : '').$time.' ';
	            $retval .= (($time > 1) ? $key.'s' : $key);
	            $granularity--;
	        }
	        if ($granularity == '0') { break; }
	    }
	    return $retval;      
	}

	/**
	 * We always save datetime in UTC timezone. 
	 */
	public static function getDateTimeForDB($time = null){
		if($time == null) 
			$time = time();
		$tz = date_default_timezone_get();
		date_default_timezone_set('UTC');
		$date = date("Y-m-d H:i:s", $time);
		date_default_timezone_set($tz);
	    return $date;
	}
	
	public static function getDateForDB($time = null){
		if($time == null) 
			$time = time();
		$tz = date_default_timezone_get();
		date_default_timezone_set('UTC');
		$date = date("Y-m-d", $time);
		date_default_timezone_set($tz);
	    return $date;
	}
	
	public static function getLocaleDateTimeString($time = null, $format = "d M, Y h:i A"){
		if($time == null) $time = time();
		date_default_timezone_set(SGlobal::getSetting("timezone", "UTC"));
		$date = date($format, $time);
		date_default_timezone_set('UTC');
	    return $date;
	}
	
	public static function createSlug($string){
	    $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return strtolower($slug);
	}
	
    public static function createUniqueId(){
		usleep(1); // Because we are making id on the basis of microseconds so we need to delay a microsecond.
		$m = explode(".", microtime(true));
		$seconds = $m[0];
		$microseconds = isset($m[1])?$m[1]:0;
		$uid = $seconds.str_pad($microseconds,4,'0', STR_PAD_LEFT).rand(111,999).rand(1,9)."";
		return $uid;
	}

	public static function generatePIN($digits = 4){
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
	}
	
	public static function getExcerpt($content, $length, $trailing='...'){
		$content = strip_tags($content);
	    $traillength = strlen($trailing);	    
	    $contentlength = strlen($content);
	    $sublength = $length - $traillength;
	    if($sublength < $contentlength)
	    {
            $pos = strrpos($content, ' ', -($contentlength - $sublength));
            $excerpt = substr($content, 0, $pos);
            return $excerpt.$trailing;
	    }
	    
        return $content;
	}
	
	public static function getRefinedContent($content){
		return strip_tags($content, "<strong> <b> <u> <span> <em> <i>");
	}
	
	public static function extractDataByKeys($keys, $haystack){
		$tmp = array();
		foreach ($keys as $v){
			if(isset($haystack[$v]))
				$tmp[$v] = $haystack[$v];
			else
				$tmp[$v] = '';
		}
		return $tmp;
	}
	
	public static function copyDirectory($src, $dst) {
	    $dir = opendir($src);
	    @mkdir($dst);
	    while(false !== ( $file = readdir($dir)) ) {
	        if (( $file != '.' ) && ( $file != '..' )) {
	            if ( is_dir($src . '/' . $file) ) {
	                self::copyDirectory($src . '/' . $file,$dst . '/' . $file);
	            }
	            else {
	                copy($src . '/' . $file,$dst . '/' . $file);
	            }
	        }
	    }
	    closedir($dir);
	}
	
	public static function getFilePath($filename){
	    return \Yii::getAlias("@webroot")."/".$filename;
	}

	public static function getMediaPath() {
		return SGlobal::getAppParam("media_path", "media");
	}

	/**
	 * Add following to scms configuration
	 * 'uploaded_file_class' =>'app\scms\plugins\plugin\components\DOSpace',
     * 'uploaded_file_base_url' =>'https://cdn.dev.example.com',
	 */
	public static function getUploadedFile($model, $attributes) {
		$class = SGlobal::getAppParam("uploaded_file_class", "yii\web\UploadedFile");
		$file = $class::getInstance($model, $attributes);
		return $file;
	}
	
	public static function getUploadedFileBaseUrl() {
		return SGlobal::getAppParam("uploaded_file_base_url", Url::base(true))."/public";
	}

	private static $_files = array();
	public static function prepareFile($model, $fileModelName, $saveBasePath = '', $prefix = "") {
		$tag = SGlobal::createUniqueId();
	    $file = self::getUploadedFile($model, $fileModelName);
	    if($file != null){
	    	$mime_type = mime_content_type($file->tempName);
			$take_out_ext = explode(".", $file->name);
	        $ext = end($take_out_ext);
	        $filename = SGlobal::createUniqueId().".{$ext}";
	        $ym = strtolower(date('Y').date('M'));
	        if($prefix!="")
	        	$filename = $prefix."_".$filename;
	        $path = SGlobal::getMediaPath()."/$saveBasePath/$ym/";
	        
	        if(!is_dir($path))
	            mkdir($path, 0775, true);
	        self::$_files[$tag] = ["file" => $file, "path" => $path.$filename, "filename" => $filename];
	        $model->{$fileModelName} = $path.$filename;
	        
	        return array(
				"tag" => $tag,
				"filename" => $filename,
				"filepath" => $path,
				"extension" => $ext,
				"mime_type" => $mime_type,
				"original_name" => $file->baseName,
	        );
	    }
	    return false;
	}
	
	public static function saveFile($tag){
	    if(!isset(self::$_files[$tag]["file"]))
	        return false;
	    $file = self::$_files[$tag]["file"];
	    if($file != null){
	       $path = self::$_files[$tag]["path"];
	       return $file->saveAs($path) !== false;
		}
		return false;
	}
	
	public static function saveFileByData($model, $fileModelName, $saveBasePath = '', $prefix = "", $fileData, $ext){
        $filename = SGlobal::createUniqueId().".{$ext}";
        $ym = strtolower(date('Y').date('M'));
        if($prefix!="")
        	$filename = $prefix."_".$filename;
        $path = SGlobal::getMediaPath()."/$saveBasePath/$ym/";
        
        if(!is_dir($path))
            mkdir($path, 0775, true);
                    
        $model->{$fileModelName} = $path.$filename;
        file_put_contents($path.$filename, $fileData);
        return array(
			"filename" => $filename,
			"filepath" => $path,
			"extension" => $ext,
        );
	}
	
	/*
	@depreciated We will remove it. SFile model should be used instead.
	*/
	public static function getUrl($file){
	    if(Yii::$app->request->baseUrl != "/" && stripos($file, Yii::$app->request->baseUrl) === 0){
	        $file = str_ireplace(Yii::$app->request->baseUrl."/", "", $file);
	    }
	    
	    $url = Url::base(true)."/".$file;
	    return $url ;
	}

	public static function isCurrentPage($route){
		$routeCurrent = "/" . Yii::$app->requestedRoute;
		return $routeCurrent == $route;
	}
	
	/*
	@depreciated We will remove it. SFile model should be used instead.
	*/
	public static function getFilePathFromUrl($url){
		$baseurl = Url::base(true);
		$file = false;
		if(stripos($url, $baseurl) === 0){
	        $file = str_ireplace($baseurl, "", $url);
	        $file = trim($file, "/");
	    }
		return $file;
	}
	
	/*
	@depreciated We will remove it. SFile model should be used instead.
	*/
	public static function getThumbnailUrl($url, $w = 80, $h = 80){
	    $file = self::getFilePathFromUrl($url);
	    if($file === false) return "";
		$take_out_ext = explode(".", $file);
		$ext = end($take_out_ext);
	    $thumb = "-tb".$w ."x". $h."x.".$ext;
	    $thumbFile = str_ireplace(".".$ext, $thumb, $file);
	    if(!file_exists($thumbFile)){
	       Image::thumbnail($file, $w, $w)
	       ->save($thumbFile, ['quality' => 80]);
	    }
	    return Url::base(true)."/".$thumbFile;
	}

	/*
	@depreciated We will remove it. SFile model should be used instead.
	*/
	public static function resizeAndSaveImage($url, $w = 400, $h = 400){ 
		$tmp_url = Url::base(true)."/".$url;
		$tmp_url = self::getFilePathFromUrl($tmp_url);
		$thumbnail = Image::thumbnail($tmp_url, $w, $h);
		$thumbnail->save($tmp_url);
		return $url;
	}
	
	public static function makeSingleLine($html){
	    return str_ireplace(["\n","\r"], "", $html);
	}
	
	public static function isCheckMode()
	{
	    if(isset(Yii::$app->session['checking']))
	       if(Yii::$app->session['checking'] = 1)
                return true;
	    return false;
	}
	
	public static function isApiMode()
	{
		if(Yii::$app->request->get("view") == Yii::$app->urlManager->apiAlias)
			return true;
		return false;
	}

	public static function isAdminMode()
	{
		if(Yii::$app->request->get("view") == Yii::$app->urlManager->adminAlias)
			return true;
		return false;
	}

	public static function isEmbedded()
	{
		if(Yii::$app->request->get("embed") == "1")
			return true;
		return false;
	}
	/*----------------------------- User Functions -------------------------------*/
	public static function getSiteStatus()
	{
		return self::getSetting("status");
	}
	
	public static function getCurrentUser()
	{
	    return SUser::getCurrentUser();
	}

	public static function getCurrentDevice()
	{
	    return SUser::getCurrentDevice();
	}
	
	public static function isLoggedin() 
	{
		$user = SUser::getCurrentUser();
	    return $user != null;
	}

	public static function isMe($userId)
	{
		$user = SUser::getCurrentUser();
		
		if($user != null && $user->id == $userId){
			return true;
		}
	    return false;
	}
	
	public static function isAdminUser()
	{
		$user = SGlobal::getCurrentUser();
		if($user != null && $user->super_user == 1)
			return true;
		return self::isAllowed("scms::DefaultAdmin::*");
	}

	public static function isAllowed($permissionName) {
		return Yii::$app->user->can($permissionName);
    }
	
	public static function getClientIP(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	/*----------------------------- Settings Functions -------------------------------*/
	
	public static function getAppParam($name, $defaultValue = "")
	{
		$val = \Yii::$app->scmsApp->getParam($name);
		if($val == null) return $defaultValue;
	    return $val;
	}
	
	private static $_settings = null;
	private static $_defaultValues = [
	    'title' => 'My Site',
		'logo' => '',
	    'home_page_id' => null,
	    'theme' => 'basic',
	    'theme_options_json' => '',
	    "type" => "website",
		"timezone" => "UTC",
		"autologin_enable" => 1,
		"autologin_cookieduration" => 1440,
		"status" => 1
	];
	
	public static function getSetting($name, $defaultValue = null)
	{
	    if(self::$_settings == null){
	        $settings = SSetting::find()
			->all();
			self::$_settings = [];
	        foreach ($settings as $v){
	            self::$_settings[$v->name] = $v;
	        }
	    }
	    $value = isset(self::$_settings[$name])?self::$_settings[$name]->value:null;
	
	    if($value == null && $defaultValue == null)
	        $value = isset(self::$_defaultValues[$name])?self::$_defaultValues[$name]:null;
		elseif($value == null && $defaultValue != null)
			return $defaultValue;
		
        return $value;
	}
	
	public static function setSetting($name, $value)
	{
	    if(isset(self::$_settings[$name])){
	        self::$_settings[$name]->value = $value;
	        return self::$_settings[$name]->save();
	    }
	    else{
	        self::$_settings[$name] = new SSetting();
	        self::$_settings[$name]->name = $name;
	        self::$_settings[$name]->value = $value;
	        return self::$_settings[$name]->save();
	    }
	}
	
	public static function getThemeOptionValue($type, $id)
	{
	    $theme_options_json = SGlobal::getSetting("theme_options_json");
	    $selectedOptions = json_decode($theme_options_json, true);
	    if(!isset($selectedOptions[$type][$id]))
	    {
	        \Yii::error("$id is not found in $type");
	        return "";
	    }
	    return $selectedOptions[$type][$id];
	}
	
	public static function getThemes()
	{	
	    $path = \Yii::getAlias('@scms/themes');
		$themeDirs = array_filter(glob($path."/*"), 'is_dir');
	    $themes = [];
	    foreach ($themeDirs as $dir){
	        $configfile = $dir."/config.json";
	        $assetsDir = $dir."/assets";
	        $previewDir = $dir."/previews";
	        if(file_exists($configfile)){
	            $json = file_get_contents($configfile);
	            $themeData = json_decode($json, true);
	            if(json_last_error() == 0){
	                Yii::$app->assetManager->publish($previewDir);
	                Yii::$app->assetManager->publish($assetsDir);
	                $themeData['preview_default'] = Yii::$app->assetManager->getPublishedUrl($previewDir)."/default.jpg" ;
	                $themes[$themeData['id']] = $themeData;
	            }
	            else
	                \Yii::error(json_last_error_msg()." in theme '$dir' config.json", 'sapp');
	        }
	        else
	            \Yii::error("Config file missing in theme '$dir'", 'sapp');
	    }
	    
	    return $themes;
	}
	
	public static function getThemeOptions(){
	    $theme = self::getSetting("theme");
	    
	    $path = \Yii::getAlias('@app/scms/themes/'.$theme.'/config.json');
	    if(!is_file($path)){
	        $data = '{"positions": [{"id": "homepage-top-banner","name": "Home Page Top Banner","type": "banner"}],"color_schemes": [{"id": "default","name": "Yellow"}]}';
	        file_put_contents($path, $data);
	    }
	
	    $json = file_get_contents($path);
	    $list = json_decode($json, true);
	    return $list;
	}
	
	public static function getPositions($type){
	    $themeOptions = self::getThemeOptions();
	    $positions = [];
	    foreach ($themeOptions['positions'] as $position){
	        if($position['type'] == $type)
	            $positions[$position['id']] = $position['name'];
	    }
	    return $positions;
	}
	
	public function getLayouts(){
	    return [['id'=>'default', 'name'=>'Default']];
	}
	
	public static function isBootstrap4() {
		return Yii::$app->params['bsVersion'] == 4;
	}

	/**
	 * It will register the theme asset file in the view. Theme asset file must be exists with
	 * name "Asset.php" and have proper namespace containing theme id or plugin id.
	 * @param $view
	 * @return mixed
	 */
	public static function loadAssets($view, $assetView = SEnum::ASSETVIEW_FRONT)
	{
	   \Yii::$app->scmsApp->loadAssets($view, $assetView);
	}

	public static function getAppAssetUrl()
	{
	    return \Yii::$app->assetManager->getPublishedUrl("@scms/assets");
	}
	
	public static function getThemeAssetUrl($id)
	{
	    return \Yii::$app->assetManager->getPublishedUrl("@scms/themes/$id/assets");
	}
	
	public static function getPluginAssetUrl($id)
	{
        return \Yii::$app->assetManager->getPublishedUrl("@scms/plugins/$id/assets");
	}

    public static function getActionButton($title, $route, $faIcon = false) {
		$buttonLabel = $faIcon!==false ? '<i class="fa fa-'.$faIcon.'"></i> ' . $title : $title;
		
		$url = Url::toRoute($route);
		return Html::a(trim($buttonLabel), $url, array("class" => "btn btn-light action_button"));
    }

    public static function getActionPopupButton($title, $route, $popupId, $popupTitle, $onSuccess, $faIcon = false) {
		$buttonLabel = $faIcon!==false ? '<i class="fa fa-'.$faIcon.'"></i> ' . $title : $title;
		
		$url = Url::toRoute($route);
		return Html::a(trim($buttonLabel), '#', array(
			"onclick"=> "return showPopup('$popupId', '$popupTitle', '$url', $onSuccess);",
			"class" => "btn btn-light action_button"
		));
    }

}