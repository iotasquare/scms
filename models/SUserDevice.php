<?php

namespace isqr\scms\models;

use isqr\scms\components\SGlobal;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SUserDevice extends IUserDevice
{
    public static function getDevice($user, $name = 'website') {

        $cookies = Yii::$app->request->cookies;
        $_d = $cookies->getValue('_d', '');
        $_h = $cookies->getValue('_h', '');

        $device = SUserDevice::find()
        ->andWhere(['id' => $_d])
        ->andWhere(['_h' => $_h])
        ->andWhere(['user_id'=>$user->id])
        ->one();
        
        $ip = SGlobal::getClientIP();
        $DataFromIP = null;
        if(SGlobal::getAppParam("device_location_enable", false)){
            $DataFromIP = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
            if(isset($DataFromIP->bogon) && $DataFromIP->bogon == true){
                $DataFromIP->city = 'unknown';
                $DataFromIP->country = 'unknown';
            }
        }

        if($device == null) {
            $device = new SUserDevice();
            $device->id = SGlobal::createUniqueId();
            $device->user_id = $user->id;
            $device->created_time = date("Y-m-d H:i:s");
            $device->active = 0;
        }
        $device->name = $name;
        $device->ip = $ip;
        $device->pin = SGlobal::generatePIN(6);
        $device->loggedin = 0;
        if($DataFromIP != null)
            $device->location = $DataFromIP->city . ", " . $DataFromIP->country;

        if($device->save()) {
            return $device;
        }
        return false;
    }

    public function isAuthenticated() {
		$ip = SGlobal::getClientIP();
        if($ip == $this->ip) {
            return $this->active == 1 && $this->_h != null;
        }
        return false;
    }

    public function isValid() {
		$ip = SGlobal::getClientIP();
        if($ip == $this->ip) {
            $currtime = time();
            if($currtime - strtotime($this->activated_time) < 15 && $this->active !== true) {
                return true;
            }
        }
        return false;
    }

    public function activate() {
        $ip = SGlobal::getClientIP();
        Yii::info("Activating Device: ClientIp=$ip, DeviceIP=$this->ip");
        $this->active = 1;
        $this->activated_time = date("Y-m-d H:i:s");
        $this->save();
        return true;
    }

    public function createHashKey() {
        $this->_h = \Yii::$app->security->generatePasswordHash($this->id.time()."scmsx".rand(111,999));
    }

    public function createAuthCode() {
        $this->auth_code = \Yii::$app->security->generateRandomString(64)."-".time();
    }

    public function verifyAuthCode(){
        $parts = explode("-", $this->auth_code);
        $time = $parts[count($parts)-1];
        $currtime = time();
        if($currtime - intval($time) < 60*5) {
            $this->auth_code = null;
            $this->update();
            return true;
        }
        return false;
    }
}
