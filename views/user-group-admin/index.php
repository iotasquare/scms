<?php

use isqr\scms\components\SGridFieldView;
use isqr\scms\widgets\adminui\actionbar\ActionBar;
use isqr\scms\widgets\adminui\block\Block;
use yii\grid\GridView;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\widgets\Pjax;
use isqr\scms\widgets\adminui\box\Box;
use yii\web\View;
use yii\bootstrap\Html;
use yii\helpers\Url;
use isqr\scms\components\SGridView;
use isqr\scms\models\SUserGroup;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php $this->addActionPopupButton("Add Group", ["jsn-create"], 'popup_create', "Add Group", 'groupUpdateOnSuccess', 'plus');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List', 'type'=>'tabular'])?>
<?php Pjax::begin(array("id"=>"pjx_list"));?>
<?= SGridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
	    [
			'attribute' => 'name',
            'contentOptions'=>array('width'=>200)
		],
	    [
			'attribute' => 'description',
		],
		[
            'attribute'=>'Status',
            'contentOptions'=>array('width'=>100)
        ],
	    [
            'class' => 'isqr\scms\components\SActionColumn',
            'buttons' => [
				'permissions' => function ($url, $model) {
					return Html::a('<span class="glyphicon fa fa-key"></span>', "#", array(
						"onclick"=> "return showPopup('popup_user_create', 'Edit Permissions', '".Url::toRoute(["jsn-set-permissions", 'id'=>$model->id])."', groupUpdateOnSuccess);",
						"class" => ""
					));
				},
				
                'edit' => function ($url, $model) {
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>', "#", array(
						"onclick"=> "return showPopup('popup_create', 'Edit Group', '".Url::toRoute(["jsn-update", 'id'=>$model->id])."', groupUpdateOnSuccess);",
						"class" => ""
					));
                }
            ],
			'template' => '{permissions} {edit} {delete}',
			'contentOptions'=>array('width'=>100, 'class'=>'text-center')
        ]
	],
]); ?>
<?php Pjax::end();?>
<?php Box::end();?>
</div>

<?php $this->beginBlock('searchform') ?>
	<?php $form = ActiveForm::begin(['method'=>'get']); ?>
		<?= $form->field($searchModel, 'filter_name')->textInput(); ?>
		<?= $form->field($searchModel, 'filter_description')->textarea(); ?>
		<?= $form->field($searchModel, 'filter_status')->dropDownList(SUserGroup::$defaultStatuses,
                ['class'=>'form-control','prompt' => 'Any Status']) ?>
		<?= Html::submitButton('Apply', ['class' => 'btn btn-sm btn-primary']) ?>
		<?= Html::a('Clear', ['index'], ['class' => 'btn btn-sm btn-default']) ?>
	<?php ActiveForm::end(); ?>
<?php $this->endBlock() ?>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
function groupUpdateOnSuccess(){
	$.pjax.reload({container:"#pjx_list"}); 
}
</script>
<?php JSRegister::end();?>