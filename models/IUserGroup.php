<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_user_groups".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 */
class IUserGroup extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_user_groups';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
        $hints = [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }
    
}
