var log = function(obj){
	if(console != undefined)
		console.log(obj);
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function makeslug(text)
{
	return text.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');		
}

function attach_editor(){
	console.log("attach_editor");
	tinymce.remove();
	$list = $('.editor');
	for(var i = 0; i < $list.length; i++){
		tinymce.init({
		    target: $list[i],
		    theme: "modern",
		    height: 200,
		    menubar : false,
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor  code"
			],
			toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
			toolbar2: "| image media | link unlink anchor  | forecolor backcolor | pagebreak code",
			image_advtab: false ,
			
			relative_urls: false,
			setup: function (editor) {
		        editor.on('change', function () {
		            editor.save();
		        });
		    }
		});
	}
}

function attach_codegen(){
	var $obj = $('.codegen')
	var $target = $('#'+$obj.data('codegen-target'));
	var $url = $obj.data('preurl');
	
	if($target.prop('readonly') == true){
		$target.wrap( '<div class="input-group"></div>' );		
		$target.parent().prepend('<span class="input-group-addon">'+$url+'</span>');
		$target.parent().append('<span class="input-group-btn"><button class="btn btn-default" type="button"><i class="glyphicon glyphicon-pencil"></i></button></span>');
		$button = $target.parent().find('button');
		
		$button.click(function(e){
			if($target.prop('readonly') == false){
				$target.prop('readonly', true);
				$(this).html('<i class="glyphicon glyphicon-pencil"></i>');
			}
			else{
				$target.prop('readonly', false);
				$(this).html('<i class="glyphicon glyphicon-ok"></i>');
			}
		});
	}
	$('.codegen').change(function(){
		var target = '#'+$(this).data('codegen-target');
		if($(target).prop('readonly') == false)
			$(target).val(makeslug($(this).val()));
	});
}

function attach_fileselector(){
	var url = baseurl + "/scms/media-library-admin/index?view=admin&embed=1";
	
	window.selectfile = function(id, url){
		$('#'+window.fileSelectorInfo.modalId).modal("hide");

		if(window.fileSelectorInfo.textfield != "")
			$('#'+window.fileSelectorInfo.textfield).val(url);
		if(window.fileSelectorInfo.imagefield != "")
			$('#'+window.fileSelectorInfo.imagefield).attr('src', url);
		if(window.fileSelectorInfo.onselect != undefined)
			eval(window.fileSelectorInfo.onselect + "('" + id + "','" + url + "')");
		return false;
	}
	
	$('.fileselector').each(function(k,v){
		$target = $(v);
		var inputField = $target.is('input');
		if(inputField){
			$target.wrap( '<div class="input-group"></div>' );
			$target.parent().append('<span class="input-group-btn"><button class="btn btn-default btn-remove" type="button"><span class="fa fa-remove"></span></button><button class="btn btn-default btn-browse" type="button"><span class="fa fa-folder-open"></span> Browse</button></span>');
			$buttonBrowse = $target.parent().find('button.btn-browse');
			$buttonRemove = $target.parent().find('button.btn-remove');
			$target.prop('readonly', true);
			
			$buttonRemove.on('click', function(e){
				e.preventDefault();
				$target = $(this);
				if(inputField)
					$target = $(this).parent().parent().parent().find('input');
				var fieldid = $target.attr('id');
				$("#"+fieldid).val("");
				var imagefield = $target.data('image-field');
				if(imagefield != '') {
					$("#"+imagefield).attr("src", );
					$("#"+imagefield).hide();
				}
			});
		}
		else {
			$buttonBrowse = $target;
		}
		
		$buttonBrowse.on('click', function(e){
			e.preventDefault();
			$target = $(this);
			if(inputField)
				$target = $(this).parent().parent().parent().find('input');
			var textfield = $target.attr('id');
			var imagefield = $target.data('image-field');
			var onselect = $target.data('onselect');
			var modalId = "modal-filemanager";
			
			window.fileSelectorInfo = {
	            'imagefield' : imagefield,
	            'onselect' : onselect,
	            'modalId' : modalId,
	            'textfield' : textfield
			};

			var html = '';
			if($('#'+modalId).length >= 0)
				$('#'+modalId).remove();
			
			var body = '<iframe id="popupframe" src="'+url+'" ></iframe>';
			html  = '<div id="'+modalId+'" class="modal fade filemanager" role="dialog">';
			html += '  <div class="modal-dialog modal-lg">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + 'Media Library' + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + body + '</div>';
			html += '    </div>';
			html += '  </div>';
			html += '</div>';
			$('body').append(html);
			$('#popupframe').css('height', $(window).innerHeight() - 150);
			$('#'+modalId).modal();
		});		
	});
}

function menusidebar(){
	
	function setSidebarStatus(opened){
		localStorage.setItem('sb_open', opened);
		if (opened) {
			$('#container').removeClass("sidebar-open");
			$('#navtogglebutton .navtogglebutton_anim').removeClass('open');
		} else {
			$('#container').addClass("sidebar-open");
			$('#navtogglebutton .navtogglebutton_anim').addClass('open');
		}
	}
	
	setSidebarStatus(localStorage.getItem('sb_open') == "true");

	$('#navtogglebutton').on('click', function() {
		if ($('#container').hasClass('sidebar-open')) {
			setSidebarStatus(true);
		} else {
			setSidebarStatus(false);
		}
	});
	
	// Menu
	$('#menu').find('li').has('ul').children('a').on('click', function() {
		if ($('#container').hasClass('sidebar-open')) {
			//$('#menu').find('li').has('ul').removeClass('active');
			//$(this).parent('li').toggleClass('active');
			$(this).parent('li').toggleClass('active');//.children('ul').collapse('toggle');
			$(this).parent('li').siblings().removeClass('active');//.children('ul').collapse('hide');
		}
	});
}

function searchsidebar(){
	function setSidebarStatus(opened){
		localStorage.setItem('search_open', opened);
		if (opened) {
			$('#container').removeClass("search-open");
		} else {
			$('#container').addClass("search-open");
		}
	}

	if($('#searchform_closebutton').length > 0) {
		setSidebarStatus(localStorage.getItem('search_open') == "true");

		$('#searchform_closebutton').on('click', function() {
			if ($('#container').hasClass('search-open')) {
				setSidebarStatus(true);
			} else {
				setSidebarStatus(false);
			}
		});
	}
}

function showGlobalLoadingOverlay(){
	$("#globalloading-overlay").show();
}

function hideGlobalLoadingOverlay(){
	$("#globalloading-overlay").hide();
}

function initComponents(){
	attach_editor();
	attach_fileselector();
}

$(document).ready(function(){
	menusidebar();
	searchsidebar();
	initComponents();
});
