<?php
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Url;
	use yii\helpers\Html;
	use isqr\scms\components\SGlobal;
    use yii\web\View;
    use isqr\scms\widgets\jsregister\JSRegister;
?>

<div class="row">
	<div class="col-md-12">
	<h4 >Already Registered? <small><a href="<?=Url::toRoute("/scms/user/login")?>" style="color: #999999">Login </a></small></h4>
	<p>Fill out the following form in order to register with us . We will save the details you provide in order to facilitate your purchases on our website.</p>
	<?php
	$form = ActiveForm::begin([
	    'id' => 'signup-form',
	    'enableAjaxValidation' => false,
		'layout' => 'horizontal',
		'class' => 'form-horizontal',   
		'fieldConfig' => [
			'horizontalCssClasses' => [
				'label' => 'col-sm-3',
				'wrapper' => 'col-sm-9',
			],
		],
	    'options' => [
	    ]
	])?>
		<h4>Your Personal Details</h4>
	    <div class="row">
	        <div class="col-md-6">
	        <?= $form->field($model, 'name')->textInput(["placeholder"=>"Name"])->label(true)?>
	        </div>
	        <div class="col-md-6">
	        <?= $form->field($model, 'surname')->textInput(["placeholder"=>"Surname"])->label(true)?>
	        </div>
	    </div>
	   
	    <div class="row">
	        <div class="col-md-6">
	        <?= $form->field($model, 'email')->textInput(["placeholder"=>"Email"])->label(true)?>
	        </div>
	        <div class="col-md-6">
	        <?= $form->field($model, 'phone_number')->textInput(["placeholder"=>"Phone No."])->label(true)?>
	        </div>
	    </div>
		<div class="row">
			<div class="col-md-6">
    			<?= $form->field($model, 'gender')->inline()->radioList(array(0=>'Male',1=>'Female'));?>
    		</div>
    	</div>
	    
	    <h4>Your Location</h4>
	    <div class="row">
	        <div class="col-md-6">
	        <?= $form->field($model, 'address')->textarea(["placeholder"=>"Address"])->label(true)?>
	        </div>
	        <div class="col-md-6">
	        <?= $form->field($model, 'city')->textInput(["placeholder"=>"City"])->label(true)?>
	        </div>
	    </div>
	    
	    <h4>Your Password</h4>
	    <div class="row">
	        <div class="col-md-6">
	        <?= $form->field($model, 'password')->passwordInput(["placeholder"=>"Password"])->label(true)?>
	        </div>
	        <div class="col-md-6">
			<?= $form->field($model, 'confirm_password')->hiddenInput()->label(false)?>
	        </div>
	    </div>
	    <p class="hint text-danger">Note: By pressing Register button, you agree to our terms of use and privacy policy.</p>
	    
	    <?= Html::submitButton('Register', ['class' => 'btn btn-primary'])?>
	<?php ActiveForm::end()?>
	</div>
</div>


<?php JSRegister::begin(["position" => View::POS_READY]); ?>
	<script>
		$('#suser-password').on('input', function(e){
			$('#suser-confirm_password').val(e.target.value)
		})
	</script>
<?php JSRegister::end(); ?>