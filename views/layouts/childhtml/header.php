<?php
use yii\helpers\Html;
use isqr\scms\components\SGlobal;
use isqr\scms\widgets\menu\Menu;
use yii\helpers\Url;
?>
<header class="navbar navbar-expand-md navbar-dark bg-dark">
	<?php $logo = SGlobal::getSetting("logo");?>
	<?php if(trim($logo) != ""):?>
		<a class="navbar-brand" id="logo" href="<?php echo URL::base(true); ?>">
			<img src="<?=$logo;?>" alt="<?php echo Html::encode(SGlobal::getSetting("title")); ?>" />
		</a>
	<?php else:?>
		<a class="navbar-brand" href="<?=Url::base(true);?>"><?= Html::encode(SGlobal::getSetting("title"));?></a>
	<?php endif;?>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topnav" aria-controls="topnav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	<div id="topnav" class="collapse navbar-collapse">
		<ul class="navbar-nav ml-auto">
			<?php if(SGlobal::isLoggedin()):?>
				<?php if(SGlobal::isAdminUser()):?>
				<li class="nav-item">
					<a class="nav-link" href="<?=Url::toRoute('/admin');?>" target="_blank">Admin Panel</a>
				</li>
				<?php endif; ?>
				<li class="nav-item">
					<a class="nav-link" href="<?=Url::toRoute('/scms/user/logout');?>">Logout</a>
				</li>
			<?php else:?>
				<li class="nav-item">
					<a class="nav-link" href="<?=Url::toRoute('/scms/user/login');?>">Login</a>
				</li>
			<?php endif;?>
		</ul>
	</div>
</header>