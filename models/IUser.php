<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_users".
 *
 * @property string $id
 * @property string $name
 * @property string $surname
 * @property string $middle_name
 * @property string $username
 * @property string $email
 * @property integer $gender
 * @property string $password_enc
 * @property string $activation_key
 * @property string $auth_key
 * @property string $image_profile
 * @property string $image_cover
 * @property string $phone_number
 * @property string $address
 * @property string $city
 * @property string $country
 * @property string $intro
 * @property string $about
 * @property string $timezone
 * @property integer $terms_accepted
 * @property integer $super_user
 * @property integer $force_change_password
 */
class IUser extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_users';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['name', 'email', 'password_enc'], 'required'],
            [['gender', 'terms_accepted', 'super_user', 'force_change_password'], 'integer'],
            [['auth_key', 'about'], 'string'],
            [['name', 'surname', 'middle_name', 'username', 'city', 'country'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 255],
            [['password_enc', 'activation_key'], 'string', 'max' => 128],
            [['image_profile', 'image_cover'], 'string', 'max' => 200],
            [['phone_number', 'timezone'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 100],
            [['intro'], 'string', 'max' => 250],
            [['gender', 'terms_accepted', 'force_change_password'], 'default', 'value' => 0],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];
        $attributeLabels = [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Gender'),
            'password_enc' => Yii::t('app', 'Password Enc'),
            'activation_key' => Yii::t('app', 'Activation Key'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'image_profile' => Yii::t('app', 'Image Profile'),
            'image_cover' => Yii::t('app', 'Image Cover'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'intro' => Yii::t('app', 'Intro'),
            'about' => Yii::t('app', 'About'),
            'timezone' => Yii::t('app', 'Timezone'),
            'terms_accepted' => Yii::t('app', 'Terms Accepted'),
            'super_user' => Yii::t('app', 'Super User'),
            'force_change_password' => Yii::t('app', 'Force Change Password'),
        ];
        $hints = [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Gender'),
            'password_enc' => Yii::t('app', 'Password Enc'),
            'activation_key' => Yii::t('app', 'Activation Key'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'image_profile' => Yii::t('app', 'Image Profile'),
            'image_cover' => Yii::t('app', 'Image Cover'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'intro' => Yii::t('app', 'Intro'),
            'about' => Yii::t('app', 'About'),
            'timezone' => Yii::t('app', 'Timezone'),
            'terms_accepted' => Yii::t('app', 'Terms Accepted'),
            'super_user' => Yii::t('app', 'Super User'),
            'force_change_password' => Yii::t('app', 'Force Change Password'),
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }
    
}
