<?php
namespace isqr\scms\controllers;

use Exception;
use Yii;
use isqr\scms\components\SController;
use isqr\scms\components\SGlobal;
use isqr\scms\jobs\SEmailJob;
use isqr\scms\models\SLoginForm;
use isqr\scms\models\SPasswordChangeForm;
use isqr\scms\models\SPasswordResetForm;
use isqr\scms\models\SNotification;
use isqr\scms\models\SUser;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use isqr\scms\models\SVerifyDeviceForm;
use isqr\scms\models\SUserDevice;
use yii\web\ForbiddenHttpException;
use isqr\scms\models\SActivityLog;
use isqr\scms\models\SEmailTemplate;
use yii\web\GoneHttpException;
use yii\web\HttpException;

class UserController extends SController
{	
	const LOGIN_DEVICE_PASS = 1;
	const LOGIN_DEVICE_FAIL = 2;
	const LOGIN_DEVICE_VERIFICATION_REQUIRED = 3;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    array(
                        'actions' => ['login', 'login-with-key', 'external', 'index', 'activate-device', 'verify-device', 'signup', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ),
                    array(
        				'actions'=>array('index', 'login-with-key','login-success', 'activate-device', 'verify-device', 'signup', 'login', 'logout', 'edit', 'change-password', 'me', 'ping'),
                        'allow' => true,
        				'roles'=>array('@'),
					),
                ],
            ]
        ];
	}
	
	public function actions()
    {
        return [
            'external' => [
                'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this, 'externalSuccessCallback']
            ],
        ];
	}

	public function beforeAction($action)
    {   
		if($action->id == "external") {
			header("access-control-allow-origin: *");
			$this->enableCsrfValidation = false;
			$request = Yii::$app->getRequest();
			
			if(isset($_REQUEST['code'])){
				$_GET['code'] = $_REQUEST['code'];
			}
			if(isset($_REQUEST['id_token'])){
				$_GET['id_token'] = $_REQUEST['id_token'];
			}
		}

        return parent::beforeAction($action);
	}
	
	public function init()
	{
	    parent::init();
		$this->layout = "@scms/views/layouts/user";
	}
	
	public function actionMe()
	{
		return $this->actionIndex(0, "me");
	}
	
	public function actionIndex($id = 0, $username = "me")
	{
		if($id != 0){
			$user = SUser::find()->where(["id" => $id])->one();
			if($user==null) throw new NotFoundHttpException("Profile does not exist.");
		}
		elseif ($username!="me"){
			$user = SUser::find()->where(["username" => $username])->one();
			if($user==null) throw new NotFoundHttpException("Profile does not exist.");
		}
		else {
			$user = SGlobal::getCurrentUser();
			if($user==null)
				return $this->redirect(['/scms/user/login']);
		}
		if(SGlobal::isMe($user->id))
			$this->view->title = Yii::t('app', "My Profile");		
		else {
			if(SGlobal::getAppParam("userAllowPublicProfile") == false && !SGlobal::isAdminUser())
				throw new UnauthorizedHttpException("You are not authorized to view this profile.");
			
			$this->view->title = $user->getFullName();
		}
		$this->view->params['breadcrumbs'][] = $this->view->title;
		
		if(trim($user->about) != ""){
			$this->view->setMetaTag("description", $user->about);
			$this->view->setMetaTag("og:description", $user->about);
		}
		
		if(trim($user->image_profile) != ""){
			$this->view->setMetaTag("og:image", $user->getProfileImageUrl());
		}
		$this->view->setMetaTag("og:title", $this->view->title);
		
		$this->layout = "@scms/views/layouts/useraccount";
		return $this->render("index", array(
			'user'=>$user,
		));
	}
	
	public function actionEdit()
	{
		$user = SGlobal::getCurrentUser();
		if($user == null) {
			echo 'You are not authorized to view this page.';
			return;
		}
		
		if(isset($_POST['SUser']))
		{
			$user->attributes = $_POST['SUser'];
			if (Yii::$app->request->isPost) {
			    $user->saveProfileImage();
			    if($user->save()){
			        $this->redirect(Url::toRoute(['/scms/user/index', 'id'=>$user->id]));
			    }
			}
		}
		
		$this->view->title = Yii::t('app', "Edit Profile");
		$this->view->params['breadcrumbs'][] = ['label'=>'My Account', 'url'=>'/scms/user/index'];
		$this->view->params['breadcrumbs'][] = $this->view->title;
		
		
		$this->layout = "@scms/views/layouts/useraccount";
		return $this->render($this->getViewName(), array(
			'model'=>$user,
		));
	}
	
	public function actionSignup()
	{	
		if(isset($_REQUEST['redirect_to'])) {
			Yii::$app->session->set('login_redirect_to', $_REQUEST['redirect_to']);
		}
        
		if(!Yii::$app->user->isGuest){
			return $this->redirect(Url::toRoute(['/scms/user/login-success']));
		}

		if(SGlobal::getAppParam("userAllowSignup") !== true)
			throw new UnauthorizedHttpException();
		
		$user = new SUser();
		if(isset($_POST['SUser']))
		{
		    $user->id = SGlobal::createUniqueId();
			$user->attributes = $_POST['SUser'];
			$password = $user->password;
			$user->setPassword($user->password);
			$user->created_time = date('Y-m-d H:i:s.u');
			
			if(SGlobal::getAppParam("userAutoApproveOnSignup") === true)
				$user->status = SUser::USER_STATUS_ACTIVE;
			else 
				$user->status = SUser::USER_STATUS_INACTIVE;
			
            if($user->save())
			{
				return $this->_login($user);
			}
		}

		$this->view->title = Yii::t('app', "Register");
		$this->view->params['breadcrumbs'][] = $this->view->title;
		return $this->render($this->getViewName(), array(
			'model'=>$user,
		));
	}
	
    public function actionLoginSuccess(){
		$rt = Yii::$app->session->get('login_redirect_to');
		Yii::$app->session->remove('login_redirect_to');
		$user = SUser::getCurrentUser();
		$device = SUser::getCurrentDevice();
		$redirectAfterLogin = Url::toRoute([SGlobal::getAppParam("redirect_after_login_url", "/scms/default/index")]);
		
		if($user->force_change_password == 1) {
			$redirectAfterLogin = Url::toRoute(['/scms/user/change-password', 'scn' => SPasswordChangeForm::SCENARIO_CHANGEPASSWORD]);
		}
		
		if($device != null){
			$device->createAuthCode();
			if($device->update())
				if($rt!= null){
					$rt = urldecode($rt);
					$rt .= ((parse_url($rt, PHP_URL_QUERY) ? '&' : '?') . "code=" . $device->auth_code);
					return $this->redirect($rt);
				}
				else
					return $this->redirect($redirectAfterLogin);
			else {
				throw new HttpException("Unable to create auth code.");
			}
		}
		else {
			return $this->redirect($redirectAfterLogin);
		}
	}

	public function externalSuccessCallback($client)
    {
		$deviceLoggingEnable = SGlobal::getAppParam("device_logging_enable", false);
		$deviceVerificationEnable = SGlobal::getAppParam("device_verification_enable", false);

		if($deviceLoggingEnable == \false) {
			throw new GoneHttpException("Device logging is disabled. External authentication will not work.");
		}

		$user = SUser::getUserUsingAuth($client);
		if($user != null) {
			return $this->_login($user, true);
		}
		throw new GoneHttpException("Unable to login.");
	}

	private function _loginWithDevice($user, $verifyDevice){
		$device = SUserDevice::getDevice($user);
		if($device === true) {
			$user->login($device);
			return self::LOGIN_DEVICE_PASS;
		}
		elseif($device instanceof SUserDevice) {
			$cookies = Yii::$app->response->cookies;
			$cookies->add(new \yii\web\Cookie(['name' => '_d', 'value' => $device->id]));
			
			if($verifyDevice && !$device->isAuthenticated()){
				$subject = "Device Verification";
				$verify_device_url = Url::toRoute(["/scms/user/verify-device", 'id'=>$device->id, 'code'=>$device->pin], true);
				$activate_device_url = Url::toRoute(["/scms/user/activate-device", 'id'=>$device->id], true);
				$verify_device_button = '<a href="'.$verify_device_url.'"><img src="'.$activate_device_url.'" /></a>';

				$body = '<p>Here is the verification code to verify your device. </p>{{verify_device_code}}<p> If this message has landed in your junk mail folder, we suggest that you move the message to your inbox or permanently set our address as a safe/secure sender on your email account.</p><br/> <br/> {{signature}}';					
				
				$template = SEmailTemplate::get("user_device_verification", [
					'to_email' => $user->email,
					'to_name' => $user->getFullName(),
					'subject' => $subject,
					'content' => $body,
					'activate_device_url' => $activate_device_url,
					'verify_device_code' => $device->pin,
				]);
				Yii::$app->queue->push(new SEmailJob([
					'to_email' => $user->email,
					'to_name' => $user->getFullName(),
					'from_email' => $template->from_email,
					'from_name' => $template->from_name,
					'subject' => $template->subject,
					'content' => $template->content,
				]));
				$deviceVerificationActivationRequired = SGlobal::getAppParam("device_verification_activation_required", false);
				if($deviceVerificationActivationRequired == false) {
					$device->active = 1;
					$device->activated_time = date("Y-m-d H:i:s");
					$device->createHashKey();
					$device->save();
				}
				return self::LOGIN_DEVICE_VERIFICATION_REQUIRED;
			}
			else {
				$device->active = 1;
				$device->activated_time = date("Y-m-d H:i:s");
				$device->createHashKey();
				$device->save();
				$cookies->add(new \yii\web\Cookie(['name' => '_h', 'value' => $device->_h]));
				$user->login($device);
				return self::LOGIN_DEVICE_PASS;
			}
		}
		return self::LOGIN_DEVICE_FAIL;
	}
	
	protected function _login($user, $external = false, $force = false) {
		$deviceLoggingEnable = SGlobal::getAppParam("device_logging_enable", false);
		$deviceVerificationEnable = SGlobal::getAppParam("device_verification_enable", false);
		$url = Url::toRoute(['/scms/user/login-success']);

		if($deviceLoggingEnable) {
			$resp = $this->_loginWithDevice($user, $deviceVerificationEnable && !$force);
			if($resp == self::LOGIN_DEVICE_VERIFICATION_REQUIRED) {
				$url = Url::toRoute(['/scms/user/verify-device']);
			}
			elseif($resp == self::LOGIN_DEVICE_PASS){
				$url = Url::toRoute(['/scms/user/login-success']);
			}
			else {
				throw new Exception("We are unable to verify device. Please try again or contact customer support.");
			}
		}
		else {
			$user->login();
		}
		
		if($external == true) {
			Yii::$app->user->returnUrl = $url;
			return;
		}
		else {
			return $this->redirect($url);
		}
	}

    public function actionLogin($verify_device = 0)
	{
		if(isset($_REQUEST['redirect_to'])) {
			Yii::$app->session->set('login_redirect_to', $_REQUEST['redirect_to']);
		}

		if(!Yii::$app->user->isGuest){
			return $this->redirect(Url::toRoute(['/scms/user/login-success']));
		}
			
		$deviceVerificationEnable = SGlobal::getAppParam("device_verification_enable", false);

		$model = new SLoginForm();
		if(isset($_POST['SLoginForm']) && $model->load(Yii::$app->request->post()) && $model->validate())
		{
			$model->attributes = $_POST['SLoginForm'];
			if(($user = $model->isValid()) !== false) {
				return $this->_login($user);
			}
		}
		
		$this->view->title = Yii::t('app', "Login");
		$this->view->params['breadcrumbs'][] = $this->view->title;
		return $this->render('login', array(
			'model'=>$model,
			'success' => $verify_device,
			'deviceVerificationEnable' => $deviceVerificationEnable
		));
	}

	public function actionLoginWithKey($key, $email)
	{
		$user = SUser::find()
		->andWhere(['activation_key' => $key])
		->andWhere(['email' => $email])
		->one();

		if($user == null) {
			throw new UnauthorizedHttpException("Key is not valid.");
		}
		$resp = $this->_login($user, false, true);
		return $resp;
	}

	public function actionActivateDevice($id)
	{
		$device = SUserDevice::find()
		->andWhere(['id' => $id])
		->one();
		
		if($device !== null) {
			$device->activate();
		}
		$data = file_get_contents(Yii::getAlias("@scms/assets")."/images/button-verify.png");
		header("Content-Type: image/jpeg");
		echo $data;
		exit();
	}

	public function actionVerifyDevice()
	{	
		$cookies = Yii::$app->request->cookies;
		
		$_d = $cookies->getValue('_d', '');
		$model = new SVerifyDeviceForm();
		
		if(isset($_POST['SVerifyDeviceForm']) && $model->load(Yii::$app->request->post()) && $model->validate())
		{
			$model->attributes = $_POST['SVerifyDeviceForm'];

			$device = SUserDevice::find()
			->andWhere(['id' => $_d])
			->one();

			if($model->verify($device)) {
				$cookies = Yii::$app->response->cookies;
				$cookies->add(new \yii\web\Cookie(['name' => '_d', 'value' => $device->id]));
				$cookies->add(new \yii\web\Cookie(['name' => '_h', 'value' => $device->_h]));

				return $this->redirect(Url::toRoute(["/"]));
			}
			else {
				$error = $model->getErrors();
				throw new ForbiddenHttpException($error['code'][0]);
			}
		}

		$this->view->title = Yii::t('app', "Device Verification");
		$this->view->params['breadcrumbs'][] = $this->view->title;
		return $this->render('verify-device', array(
			'model'=>$model
		));
	}
	
	public function actionLogout()
	{
		$user = SGlobal::getCurrentUser();
		$user->logout();
	    $this->redirect(Url::base(true));
	}
	
	public function actionResetPassword($scn = SPasswordResetForm::SCENARIO_DEFAULT)
	{
		if(isset($_REQUEST['redirect_to'])) {
			Yii::$app->session->set('login_redirect_to', $_REQUEST['redirect_to']);
		}
		
		$form = new SPasswordResetForm();
		$form->scenario = $scn;

		if(isset($_POST['SPasswordResetForm']))
		{	
			if ($form->load(Yii::$app->request->post()) && $form->validate()) 
			{	
				if($form->scenario == SPasswordResetForm::SCENARIO_CHANGEPASSWORD) {
					if($form->updatePassword()){
						//Disable Force set password after setting password by user
						SUser::disableForcePassword($_POST['SPasswordResetForm']['email']);
						
						Yii::$app->session->setFlash('submit','Your password has been changed.');
						return $this->redirect(Url::toRoute(["/scms/user/login"]));
					}
				}
				else if($form->sendInstructions()) {
					// Password Recovery Attempt Logged here...
					SActivityLog::log("user_account_recovery_attempt", ['email' => $_POST['SPasswordResetForm']['email']]);
					
					Yii::$app->session->setFlash('submit', Yii::t('app', 'Please check your mailbox for your login credentials.'));
					$this->view->title = Yii::t('app', "Help is on its way!");
				}
			}
		}
		
		if($form->scenario == SPasswordResetForm::SCENARIO_CHANGEPASSWORD) {
			$form->activation_key = Yii::$app->request->get("key");
			$form->email = Yii::$app->request->get("email");
			$user = SUser::find()
			->andWhere(['activation_key' => $form->activation_key])
			->andWhere(['email' => $form->email])
			->one();
			if($user == null) {
				throw new UnauthorizedHttpException("Key is not valid.");
			}
		}

		$this->view->title = Yii::t('app', "Reset Password"); 
		$this->view->params['breadcrumbs'][] = $this->view->title;
		return $this->render($this->getViewName(), array(
		    'model'=>$form,
		));
	}

	public function actionChangePassword($scn = SPasswordChangeForm::SCENARIO_VALIDATEDCHANGEPASSWORD)
	{
		$form = new  SPasswordChangeForm();
		$form->scenario =  $scn;

		if(isset($_POST['SPasswordChangeForm']))
		{	
			if ($form->load(Yii::$app->request->post()) && $form->validate()) 
			{	
			    $model = SGlobal::getCurrentUser();
			    $model->setPassword($form->password);
				$model->force_change_password = 0;
				if($model->update()){
					Yii::$app->session->setFlash('submit','Your password has been changed.');
				}
				else {
					var_dump($model->getErrors()); die;
				}
			}
		}
		$this->view->title = Yii::t('app', "Change Password");
		$this->view->params['breadcrumbs'][] = ['label'=>Yii::t('app', 'My Account'), 'url'=>'/scms/user/index'];
		$this->view->params['breadcrumbs'][] = $this->view->title;
		return $this->render($this->getViewName(), array(
		    'model'=>$form,
		));
	}

	public function actionPing() {
		
        date_default_timezone_set('UTC');
		$singleLoginEnable = SGlobal::getAppParam("device_single_login_enable", false);
		if(!$singleLoginEnable) {
            return $this->renderJsn(true, "Allowed.", []);
        }

		$currentDevice = SGlobal::getCurrentDevice();
		if($currentDevice != null) {
			$currentDevice->last_ping_time = date("y-m-d H:i:s");
			$currentDevice->update();

			if($currentDevice->loggedin == 1) {
				return $this->renderJsn(true, "Allowed", []);
			}
		}

        $url = Url::toRoute("/scms/user/logout");
        return $this->renderJsn(false, Yii::t("app", "Your session has ended on this browser as the system has monitored another login at the same time."), [$url]);
    }
}