<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta name="language" content="<?= Yii::$app->charset ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<link rel="shortcut icon" href="<?php echo Url::base(true); ?>/themes/default/images/favicon.png" type="image/x-icon" />
	
	<title><?php echo Html::encode($this->title); ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
	<?php echo $content;?>   
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
