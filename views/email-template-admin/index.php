<?php

use isqr\scms\components\SEnum;
use isqr\scms\components\SGridView;
use isqr\scms\models\SPage;
use isqr\scms\widgets\adminui\box\Box;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

?>

<?php $this->addActionPopupButton("Add Template", ["jsn-create"], 'popup_create', "Add Template", 'updateOnSuccess', 'plus');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List', 'type'=>'tabular'])?>
<?php Pjax::begin(array("id"=>"pjx_list"));?>
<?= SGridView::widget([
	'dataProvider' =>  $dataProvider,
	'pjaxId' => 'pjx_list',
	'columns' => [ 
		[
			'header'=>'Template',
            'value'=>function($smodel){
				$template_codes = Yii::$app->scmsApp->getConfig("email_template_codes");
				$html = "";
				if(isset($template_codes[$smodel->code]))
					$html .= '<div style="margin-top:0px;">'.$template_codes[$smodel->code]['name'].'</div>';

				$html .= '<div class="label label-default">'.$smodel->code . '</div>';
				return $html;
			},
            'contentOptions'=>array('width'=>200),
			'format' => 'raw'
        ],
		[
            'header'=>'Sender',
            'value'=>function($smodel){
				if($smodel->from_email == "")
					return "(default)";
				return $smodel->from_name . " <".$smodel->from_email.">";
			},
            'contentOptions'=>array('width'=>200)
        ],
		[
			'attribute'=>'subject',
            'contentOptions'=>array('width'=>200)
        ],
		[
			'class' => 'yii\grid\ActionColumn',
			'buttons' => [
				'edit' => function ($url, $model) {
					return Html::a("<span class='glyphicon glyphicon-pencil'></span>", "#", array(
						"onclick"=> "return showPopup('popup_create', 'Edit Template', '" . Url::toRoute(["jsn-update", 'id'=>$model->id]) . "', updateOnSuccess);",
						"class" => ""
					));
				}
			],
            'template' => '{edit} {delete}',
            'contentOptions'=>array('width'=>120, 'class'=>'text-right')
        ]
	],
]);
?>
<?php Pjax::end();?>
<?php Box::end();?>
</div>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
function updateOnSuccess(){
	$.pjax.reload({container:"#pjx_list"}); 
}
</script>
<?php JSRegister::end();?>