<?php 
/* @var $this isqr\scms\components\SView */
?>
<?php $this->beginContent("@scms/views/layouts/main.php", ['scope'=>'user']); ?>
	<?= $this->render('@scms/views/layouts/childhtml/header');?>
    <div class="container mt-4 mb-4">
        <div class="row">
        	<div class="col-md-12">
                <h1><?=$this->title;?></h1>
                <div><?= $content;?></div>
			</div>
		</div>
	</div>
	<?= $this->render('@scms/views/layouts/childhtml/footer');?>
<?php $this->endContent(); ?>