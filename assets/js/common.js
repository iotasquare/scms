var success = 1;
var error = 0;

function ajax_post(url, data, onMessage, confirmText = undefined){
	var agree = false;
	if(confirmText != undefined)
		agree = confirm(confirmText);
	else
		agree = true;
	
	if(agree){
		var csrfToken = $('meta[name="csrf-token"]').attr("content");
		var formData = data;
		if(formData instanceof FormData == false) {
			formData = new FormData();
			for (var key in data) {
				formData.append(key, data[key]);
			}
		}
		formData.append('_csrf', csrfToken);

		$.ajax({
			type: 'post',
			cache: false,
			data: formData,
			url: url,
			processData: false,
      		contentType: false,
			complete: function(resp){
				if(resp.responseText.startsWith("fn(")){
	        		if(onMessage != undefined){
		        		var messageFunc = resp.responseText.replace("fn(", "onMessage(");
						eval(messageFunc);
	        		}
	        	}
			},
		});
	}
	return false;
}

function ajax_get(url, onMessage){
	$.ajax({            
		type: 'get',
		cache: false,
		url: url,
			complete: function(resp){
			if(resp.responseText.startsWith("fn(")){
				if(onMessage != undefined){
					var messageFunc = resp.responseText.replace("fn(", "onMessage(");
					eval(messageFunc);
				}
			}
		},
	});
	return false;
}

function _ajaxContainer(elmId, url, onMessage) {
	var loadingHtml = '<div id="'+elmId+'_loader" style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; background: #eeeeeeab; "><div style="text-align:center; width:100%; position: absolute; top: 50%; transform: translateY(-50%);"><i class="fa fa-refresh fa-spin fa-2x"></i> <div class="perc" style="margin-top: 10px;"></div></div></div>';
	$('#' + elmId).find('.ajax-content').append(loadingHtml);
	function showProgress(evt){
		if(evt.lengthComputable){
			var perc = (evt.loaded / evt.total * 100);
			$('#' + elmId).find('.perc').html( Math.round(perc) + "%");
		}
		else
			$('#' + elmId).find('.perc').html("");
	}
		
	function filterHtml(html){
		var $html = $(html);
		var finalHtml = "";
		$html.each(function(k,v){
			if(v.nodeType == 1){
				if(v.tagName.toLowerCase() == "script" ){
					var newsrc = v.src.replace(baseurl, "");
					if($("script[src='" + newsrc + "']").length == 0 
							&& newsrc.indexOf("/front.js") == -1 
							&& newsrc.indexOf("/admin.js") == -1
							&& newsrc.indexOf("/common.js") == -1){
						finalHtml += v.outerHTML;
					}
					else if(newsrc == "")
						finalHtml += v.outerHTML;
					else 
						console.log(v.outerHTML + " ignored.");
				}
				else if(v.tagName.toLowerCase() == "link" && $(v).attr("href") != undefined){
					var newhref = $(v).attr("href");
					var newhref = newhref.replace(baseurl, "");
					if($("link[href='" + newhref + "']").length == 0 
							&& newhref.indexOf("front.css") == -1 
							&& newhref.indexOf("admin.css") == -1){
						$('head').append(v);
					}
					else
						console.log(v.outerHTML + " ignored.");
				}
				else finalHtml += v.outerHTML;
			}
			else finalHtml += v.textContent;
		});
		return finalHtml;
	}

	function onAjaxResponse(resp) {
		if(resp.responseText.startsWith("fn(")) {
			if(onMessage != undefined){
				var messageFunc = resp.responseText.replace("fn(", "onMessage(");
				eval(messageFunc);
			}
			setTimeout(function(){
				$('#' + elmId).modal('hide');
			}, 500)
		}
		else {
			$('#' + elmId).find('.ajax-content').html(filterHtml(resp.responseText));
			$('#' + elmId).find('form').submit(onPopupFormSubmit);
			$('#' + elmId).on('pjax:success', function(event, resp, status, xhr, options) {
				if(resp.startsWith("fn(")) {
					if(onMessage != undefined){
						var messageFunc = resp.replace("fn(", "onMessage(");
						eval(messageFunc);
					}
					$('#' + elmId).modal('hide');
				}
			});
			initComponents();
		}
	}
	
	function onPopupFormSubmit(e) {
		e.preventDefault();
	    e.stopImmediatePropagation();
	    var $form = $(this),
        data = $form.data('yiiActiveForm');
	    if(data != undefined && data.validated){
			var formData = new FormData($(this)[0]);
			$('#' + elmId).find('.ajax-content').append(loadingHtml);
			$.ajax({
		        url: $(this).attr('action'),
		        type: 'post',
		        data: formData,
				async: true,
				cache: false,
		        contentType: false,
				processData: false,
				xhr: function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", showProgress, false);
					xhr.addEventListener("progress", showProgress, false);
				   return xhr;
				},
		        complete: function(resp){
					onAjaxResponse(resp);
		        },
		    }); 
	    }
	    return false;
	};
	$.ajax({
		url: url,
		type: 'get',
		async: true,
		cache: false,
		contentType: false,
		processData: false,
		xhr: function() {
			var xhr = new window.XMLHttpRequest();
		   	xhr.addEventListener("progress", showProgress, false);
		   return xhr;
		},
		complete: function(resp){
			onAjaxResponse(resp);
		}
	});
}

function showAjaxContent(elmId, url, onMessage, options) {
	if(options == undefined)
		options = {};

	var html = "";
	html += '<div class="ajax-content"></div>';
	$("#" + elmId).append(html);
	
	_ajaxContainer(elmId, url, onMessage);

    return false;
}

/**
 * @param elmId must be same for same form otherwise it will cause issue in submission.
 * @param title
 * @param url
 * @param onMessage
 * @returns
 */
function showPopup(elmId, title, url, onMessage, options){
	if($("#" + elmId).length > 0) 
		$("#" + elmId).remove();
	if(options == undefined)
		options = { 
			'model-size' : 'modal-lg'
		};

	var html = "";
	html += '<div id="' + elmId + '" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">';
	html += '  <div class="modal-dialog ' + options['model-size'] + '">';
	html += '    <div class="modal-content">';
	if(options['header'] == undefined){
		html += '      <div class="modal-header">';
		html += '        <h4 class="modal-title">' + title + '</h4>';
		html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		html += '      </div>';
	}
	else if(options['header'] != false) {
		html += '      <div class="modal-header">' + options['header'] + '</div>';
	}
	html += '      <div class="modal-body ajax-content" style="min-height: 90px;"></div>';
	if(options['footer'] != undefined && options['header'] != false) {
		html += '      <div class="modal-footer">' + options['footer'] + '</div>';
	}
	html += '    </div>';
	html += '  </div>';
	html += '</div>';
	$('body').append(html);

	if(options['dismissible'] == false){
		$('#' + elmId).attr('data-backdrop', 'static');
		$('#' + elmId).attr('data-keyboard', 'false');
	}

	$('#' + elmId).modal();
	
	$('#' + elmId).on('hidden.bs.modal', function () {
		$("#" + elmId).remove();
	});
	
	_ajaxContainer(elmId, url, onMessage);

    return false;
}

function showStaticPopup(popupId, title, content, footer){
	if($("#" + popupId).length > 0) 
		$("#" + popupId).remove();
	
	var body = "Hello";
	html  = '<div id="' + popupId + '" class="modal fade" role="dialog">';
	html += '  <div class="modal-dialog">';
	html += '    <div class="modal-content">';
	html += '      <div class="modal-header">';
	html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	html += '        <h4 class="modal-title">' + title + '</h4>';
	html += '      </div>';
	html += '      <div class="modal-body">'+content+'</div>';
	if(footer != undefined) {
		html += '      <div class="modal-footer">'+footer+'</div>';
	}
	html += '    </div>';
	html += '  </div>';
	html += '</div>';
	$('body').append(html);
	$('#' + popupId).modal();
	
    return false;
}

$(document).ready(function(){
	if($.pjax!=undefined)
		$.pjax.defaults.timeout = 5000;
});
