<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use isqr\scms\models\SUser;
use isqr\scms\components\SGlobal;
use isqr\scms\widgets\adminui\AdminMenu;
?>
<?php $user = SGlobal::getCurrentUser();?>
<nav id="sidebar">
	<?= AdminMenu::widget([		
    	'itemOptions'=>['class'=>'treeview'],
    	'activateItems'=>true,
    	'activateParents'=>true,
        'options'=>['class'=>'menu', 'id' => 'menu'],
    	'submenuTemplate' =>  '<ul class="treeview-menu">{items}</ul>',
    	'encodeLabels' => false
    	]);
	?>
</nav>