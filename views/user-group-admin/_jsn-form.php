<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use isqr\scms\models\SUserGroup;
use isqr\scms\components\SEnum;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SGlobal;
use isqr\scms\widgets\select2\Select2;
?>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'group-update-form',
        'options' => [
            'method'=>'post'
        ]
    ]); ?>
    
    <?= $form->field($model, 'name')->textInput(); ?>
    <?= $form->field($model, 'description')->textarea(); ?>
    <?= $form->field($model, 'status')->dropDownList(SUserGroup::$defaultStatuses); ?>
    
    <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
    <?= Html::a('Cancel', '#', ['class' => 'btn btn-sm btn-default', 'data-dismiss'=>'modal']); ?>
    <?php ActiveForm::end(); ?>
</div>