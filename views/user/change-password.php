<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use isqr\scms\components\SGlobal;
use isqr\scms\models\SPasswordChangeForm;

?>
<div class="row">
<div class="col-md-6">
	<?php if(Yii::$app->session->hasFlash('submit')): ?>
		<?php echo Yii::$app->session->getFlash('submit'); ?><br/><br/>
		<?php echo Html::a('Ok', Url::base(true), array('class'=>'btn btn-primary')); ?>
	<?php else: ?>
		<?php
		$form = ActiveForm::begin([
		    'id' => 'resetpassword-form',
		    'enableAjaxValidation' => false,
		    'options' => [
		        'class' => ''
		    ]
		])?>
		<?php if($model->scenario == SPasswordChangeForm::SCENARIO_VALIDATEDCHANGEPASSWORD):?>
	    <?= $form->field($model, 'currentpassword')->passwordInput();?>
		<?php endif; ?>
	    <?= $form->field($model, 'password')->passwordInput();?>
	    <?= $form->field($model, 'confirmpassword')->passwordInput();?>
	    <?= Html::submitButton('Change', ['class' => 'btn btn-primary'])?>
		<?php ActiveForm::end()?>
	<?php endif; ?>
</div>
</div>