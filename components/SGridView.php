<?php

namespace isqr\scms\components;

use Closure;

use yii\base\InvalidConfigException;
use yii\grid\GridView;
use yii\helpers\Json;
use yii\grid\GridViewAsset;
use yii\helpers\Html;

class SGridView extends GridView {
    public $sortable = false;
    public $selectable = false;

    private $checkboxHtml = '';
    private $sorthandle = '';
    
    public $sortUrl;
    public $group = false;
    public $pjaxId = "";

    public $groupRelationAttribute = "parent_id";
    public $groupDataAttribute = 'subTasks';
    public $groupTitleAttribute = 'name';
    public $groupAction = [];

    public $header = true;

    public static $c = 1;

    public function init(){
        $this->id = "w".time().SGridView::$c++;
        $this->options['id'] = $this->id;
        parent::init();
        
        GridViewAsset::register($this->view);

        $this->options['class'] = 'sgrid-view';
        $this->tableOptions['class'] = 'sortcontainer';

        if($this->selectable) {
            $this->options['class'] .= " selectable";
            $this->checkboxHtml = '<input data-id="{{id}}" type="checkbox"/>';
        }
        
        if($this->sortable){
            $this->options['class'] .= " sortable";
            $this->sorthandle = '<i class="sorthandle fa fa-arrows-v" aria-hidden="true" style="font-size:14px; line-height: 20px;"></i>';
        }

        $this->pager['firstPageLabel'] = 'First';
        $this->pager['lastPageLabel'] = 'Last';
    }

    public function renderFilters()
    {
        if ($this->filterModel !== null) {
            $cells = [];
            foreach ($this->columns as $k=>$column) {
                if($k == 0) $column->filterOptions = ["colspan" => 2];
                $cells[] = $column->renderFilterCell();
            }

            return Html::tag('tr', implode('', $cells), $this->filterRowOptions);
        }

        return '';
    }

    public function renderTableHeader()
    {
        if($this->header == false) return "";
        if($this->header !== true){
            $contentHead = Html::tag('tr', $this->header, $this->headerRowOptions);
            $content = "<thead class=\"main\">\n" . $contentHead . "\n</thead>";
            return $content;
        } 

        $cells = [];
        if($this->sortable == true || $this->selectable == true) {
            $cells[] = '<td style="width: 2px;"></td>';
        }
        foreach ($this->columns as $k=>$column) {
            $cells[] = $column->renderHeaderCell();
        }
        $contentHead = Html::tag('tr', implode('', $cells), $this->headerRowOptions);
        $content = "<thead class=\"main\">\n" . $contentHead . "\n</thead>";

        $contentFilter = $this->renderFilters();

        $content .= "<thead class=\"filters\">\n" . $contentFilter . "\n</thead>";

        return $content;
    }

    public function renderTableFooter()
    {
        $cells = [];
        $cells[] = '<td></td>';
        foreach ($this->columns as $column) {
            /* @var $column Column */
            $cells[] = $column->renderFooterCell();
        }
        $content = Html::tag('tr', implode('', $cells), $this->footerRowOptions);
        if ($this->filterPosition === self::FILTER_POS_FOOTER) {
            $content .= $this->renderFilters();
        }

        return "<tfoot>\n" . $content . "\n</tfoot>";
    }

    public function renderTableBody()
    {
        $models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        $colspan = count($this->columns);
        if($this->group) $colspan ++;
        foreach ($models as $index => $model) {
            if($this->group != false && $model->hasAttribute($this->groupRelationAttribute))
                if($model->{$this->groupRelationAttribute} != null) continue;
            $key = $keys[$index];
            $subrows = [];
            if($this->group != false && $model->{$this->group} == true) {
                foreach ($model->{$this->groupDataAttribute} as $subindex => $submodel) {
                    $subrows[] = $this->renderTableRow($submodel, $submodel->id, $subindex, false);
                }
                $grouprow = $this->renderTableRow($model, $model->id, $index, true);

                $html = '
                <tr class="sortable grouprows" data-key="' . $model->id . '">
                    <td colspan="'.$colspan.'" class="groupcol">
                        <table>
                            <thead>' . $grouprow . '</thead>
                            <tbody class="sortcontainer">' . implode("\n", $subrows) . '</tbody>
                        </table>
                    </td>
                </tr>';
                
                $rows[] = $html;
            }
            else {
                $rows[] = $this->renderTableRow($model, $model->id, $index, false);
            }
        }

        if (empty($rows) && $this->emptyText !== false) {
            $colspan++;
            return "<tbody>\n<tr><td colspan=\"$colspan\">" . $this->renderEmpty() . "</td></tr>\n</tbody>";
        }
        ///\print_r(implode("\n", $rows));die;
        return implode("\n", $rows);
    }

    public function renderTableRow($model, $key, $index, $grouprow = false)
    {
        $cells = [];

        if($this->sortable == true || $this->selectable == true) {
            $cells[] = '<td class="ckbcol">' . $this->sorthandle . \str_ireplace("{{id}}", $model->id, $this->checkboxHtml).'</div>';
        }
        
        /* @var $column Column */
        foreach ($this->columns as $column) {
            $cells[] = $column->renderDataCell($model, $key, $index);
        }

        if ($this->rowOptions instanceof Closure) {
            $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
        } else {
            $options = $this->rowOptions;
        }

        $options['data-key'] = is_array($key) ? json_encode($key) : (string) $key;
        if(!isset($options['class'])) 
            $options['class'] = "";
        if(!$grouprow)
            $options['class'] .= ' sortable';
        else
            $options['class'] .= ' grouprow-header';
        
        return Html::tag('tr', implode('', $cells), $options);
    }

    public function run(){
        foreach($this->columns as $column){
			if(property_exists($column, 'enableSorting'))
                $column->enableSorting = !$this->sortable;
            if(isset($column->contentOptions['width'])) {
                $column->headerOptions['width'] = $column->contentOptions['width'];
                $column->filterOptions['width'] = $column->contentOptions['width'];
            }
        }
        parent::run();

        /*if(!isset($_REQUEST['_pjax'])) {
            $this->view->registerJs("$('#".$this->pjaxId."').on('pjax:beforeReplace', function() {
                detachSGrid('$this->id');
            });");
        }*/

        $this->view->registerJs("jQuery('#$this->id').sgrid({
            'sortable' : " . ($this->sortable ? 'true' : 'false') . ",
            'sortUrl' : '$this->sortUrl'
        });");
    }
}
