<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_user_group_permissions".
 *
 * @property string $id
 * @property string $user_group_id
 * @property string $action
 */
class IUserGroupPermission extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_user_group_permissions';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['user_group_id', 'action'], 'required'],
            [['user_group_id'], 'integer'],
            [['action'], 'string', 'max' => 150],
            [['user_group_id', 'action'], 'unique', 'targetAttribute' => ['user_group_id', 'action'], 'message' => 'The combination of User Group ID and Action has already been taken.'],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'user_group_id' => 'User Group ID',
            'action' => 'Action',
        ];
        $hints = [
            'id' => 'ID',
            'user_group_id' => 'User Group ID',
            'action' => 'Action',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }
    
}
