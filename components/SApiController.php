<?php
namespace isqr\scms\components;

use Yii;
use yii\helpers\Url;
use yii\web\View;
use yii\web\UnauthorizedHttpException;
use isqr\scms\models\SUser;
use yii\web\User;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

abstract class SApiController extends SController
{
    public function behaviors()
    {
        $arr = [
            'access' => [
                'class' => SAccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
        return ArrayHelper::merge($arr, parent::behaviors());
    }
    
    public function init()
    {
        parent::init();
        header("access-control-allow-origin: *");
        $this->enableCsrfValidation = false;
        
        $_h = \Yii::$app->request->post("_h", "");
        if($_h != "") {
            $user = SUser::findIdentityByAccessToken($_h);
            if($user == null)
                Yii::$app->user->logout();
            else
                Yii::$app->user->setIdentity($user);
        }
        else {
            Yii::$app->user->logout();
        }
    }
}

?>