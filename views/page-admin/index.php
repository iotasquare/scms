<?php

use isqr\scms\components\SEnum;
use isqr\scms\components\SGridView;
use isqr\scms\models\SPage;
use isqr\scms\widgets\adminui\box\Box;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

?>

<?php $this->addActionPopupButton("Add Page", ["jsn-create"], 'popup_create', "Add Page", 'pageUpdateOnSuccess', 'plus');?>

<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List', 'type'=>'tabular'])?>
<?php Pjax::begin(array("id"=>"pjx_list"));?>
<?= SGridView::widget([
	'dataProvider' =>  $dataProvider,
	'pjaxId' => 'pjx_list',
	'columns' => [ 
		'name',
		[
            'attribute'=>'Status',
            'contentOptions'=>array('width'=>100)
        ],
		[
			'class' => 'yii\grid\ActionColumn',
			'buttons' => [
				'edit' => function ($url, $model) {
					return Html::a("<span class='glyphicon glyphicon-pencil'></span>", "#", array(
						"onclick"=> "return showPopup('popup_create', 'Edit Page', '" . Url::toRoute(["jsn-update", 'id'=>$model->id]) . "', pageUpdateOnSuccess);",
						"class" => ""
					));
				}
			],
            'template' => '{edit} {delete}',
            'contentOptions'=>array('width'=>120, 'class'=>'text-right')
        ]
	],
]);
?>
<?php Pjax::end();?>
<?php Box::end();?>
</div>

<?php $this->beginBlock('searchform') ?>
	<?php $form = ActiveForm::begin(['method'=>'get']); ?>
		<?= $form->field($searchModel, 'filter_name')->textInput(); ?>
		<?= $form->field($searchModel, 'filter_status')->dropDownList(SPage::$defaultStatuses,
                ['class'=>'form-control','prompt' => 'Any Status']) ?>
		<?= Html::submitButton('Apply', ['class' => 'btn btn-sm btn-primary']) ?>
		<?= Html::a('Clear', ['index'], ['class' => 'btn btn-sm btn-default']) ?>
	<?php ActiveForm::end(); ?>
<?php $this->endBlock() ?>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
function pageUpdateOnSuccess(){
	$.pjax.reload({container:"#pjx_list"}); 
}
</script>
<?php JSRegister::end();?>