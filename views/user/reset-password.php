<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use isqr\scms\models\SPasswordResetForm;
?>
<?php
$form = ActiveForm::begin([
    'id' => 'resetpassword-form',
    'enableAjaxValidation' => false, 
	'fieldConfig' => [
		'horizontalCssClasses' => [
			'label' => 'col-sm-3',
			'wrapper' => 'col-sm-9',
		],
	],
    'options' => [
    ]
])?>
<div class="row">
<div class="col-md-6">
	
    <?php if(Yii::$app->session->hasFlash('submit')): ?>
    	<h4><?=Yii::t('app', 'Help is on its way!')?></h4>
		<p><?= Yii::$app->session->getFlash('submit'); ?></p>
		<?= Html::a(Yii::t('app', 'Ok'), Url::toRoute("/scms/user/login"), array('class'=>'btn btn-primary')); ?>
    <?php else:?>
    <h4><?=Yii::t('app', 'Relax!')?></h4>
	<p><?=Yii::t('app', 'If you\'ve forgotten your password, enter your e-mail address and we\'ll send you an e-mail telling you how to recover it.');?></p>
	<?php if($model->scenario == SPasswordResetForm::SCENARIO_CHANGEPASSWORD):?>
		<?= $form->field($model, 'password')->passwordInput();?>
		<?= $form->field($model, 'confirmpassword')->passwordInput();?>
		<?= $form->field($model, 'activation_key')->hiddenInput()->label(false)?>
		<?= $form->field($model, 'email')->hiddenInput()->label(false)?>
	<?php else:?>
		<?= $form->field($model, 'email')->textInput(["placeholder"=>Yii::t('app', "Email")])->label(true)?>
	<?php endif;?>
	<?= Html::submitButton(Yii::t('app', 'Reset'), ['class' => 'btn  btn-primary'])?>
    <?php endif; ?>
</div>
</div>
<?php ActiveForm::end()?>