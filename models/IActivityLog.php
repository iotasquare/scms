<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_activity_log".
 *
 * @property string $id
 * @property string $code
 * @property string $activity_time
 * @property integer $user_id
 * @property string $device_id
 * @property string $table_name
 * @property integer $table_object_id
 * @property string $json_params
 */
class IActivityLog extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_activity_log';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['code', 'activity_time', 'user_id'], 'required'],
            [['activity_time'], 'safe'],
            [['user_id', 'table_object_id'], 'integer'],
            [['json_params'], 'string'],
            [['code', 'table_name'], 'string', 'max' => 100],
            [['device_id'], 'string', 'max' => 50],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'code' => 'Code',
            'activity_time' => 'Activity Time',
            'user_id' => 'User ID',
            'device_id' => 'Device ID',
            'table_name' => 'Table Name',
            'table_object_id' => 'Table Object ID',
            'json_params' => 'Json Params',
        ];
        $hints = [
            'id' => 'ID',
            'code' => 'Code',
            'activity_time' => 'Activity Time',
            'user_id' => 'User ID',
            'device_id' => 'Device ID',
            'table_name' => 'Table Name',
            'table_object_id' => 'Table Object ID',
            'json_params' => 'Json Params',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
    }
    
}
