<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cms_pages".
 *
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $content
 */
class IPage extends \isqr\scms\components\SActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_pages';
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $rules = [
            [['code', 'name'], 'required'],
            [['content'], 'string'],
            [['code', 'name'], 'string', 'max' => 100],
        ];
        $attributeLabels = [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'content' => 'Content',
        ];
        $hints = [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'content' => 'Content',
        ];

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
    }
    
}
