<?php
namespace isqr\scms\components;

use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\View;
use yii\web\UnauthorizedHttpException;

abstract class SController extends Controller
{
	const JSON_ERROR = 0;
    const JSON_SUCCESS = 1;
    
    public $appAsset = null;
    
    public function behaviors()
    {
        $moduleId = $this->action->controller->module->id;
        $controllerId = Inflector::id2camel($this->action->controller->id, "-");
        return [
            'access' => [
                'class' => SAccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ["*", $moduleId . "::*", $moduleId . "::" . $controllerId . "::*"] // Allowing all actions for wildcard rules
                    ],
                ]
            ]
        ];
    }
    
    public function init()
    {   
        $this->layout = '@scms/views/layouts/page';
        
        if(SGlobal::isAdminMode())
            $this->layout = '@scms/views/layouts/admin/page';
        
        
        if(SGlobal::isEmbedded()) {
            if(SGlobal::isAdminMode())
                $this->layout = '@scms/views/layouts/admin/embed';
            else 
                $this->layout = '@scms/views/layouts/embed';
        }
        
        \Yii::$app->session['baseUrl'] = Url::base(true);
        $this->view->registerJs("var baseurl = '".Url::base(true)."';", View::POS_HEAD);
        
        $user = SGlobal::getCurrentUser();
        $this->view->registerJs("var userauth =".($user == null ? 0 : 1), View::POS_HEAD);

        $device_single_login_enable = SGlobal::getAppParam("device_single_login_enable", false);
        $this->view->registerJs("var device_single_login_enable =".($device_single_login_enable == false ? 0 : 1), View::POS_HEAD);
        
        $this->view->registerLinkTag([
            'rel' => 'shortcut icon',
            'type' => 'image/x-icon',
            "href" => SGlobal::getSetting("favicon"),
        ]);
    	
        $request = Yii::$app->getRequest();
        if ($request->enableCsrfValidation) {
        	$this->view->registerMetaTag(['name' => 'csrf-param', 'content' => $request->csrfParam]);
        	$this->view->registerMetaTag(['name' => 'csrf-token', 'content' => $request->getCsrfToken()]);
        }
        
        parent::init();
    }
    
    public function beforeAction($action)
    {   
        // For checking mode
        $checking = false;
        $checking = isset(\Yii::$app->params['checking'])?\Yii::$app->params['checking']:false;
        if($checking)
            \Yii::$app->session['checking'] = 1;
            
        if(SGlobal::isAdminUser()){
            \Yii::$app->session['checking'] = 1;
        }

        if(parent::beforeAction($action)){

            if(SGlobal::isAdminMode())   
                $this->appAsset = SGlobal::loadAssets($this->view, SEnum::ASSETVIEW_ADMIN);
            else
                $this->appAsset = SGlobal::loadAssets($this->view, SEnum::ASSETVIEW_FRONT);

            $site_status = SGlobal::getSiteStatus();
            $checkMode = SGlobal::isCheckMode();
            $adminUser = SGlobal::isAdminUser();
            if($site_status == SEnum::SITE_STATUS_MAINTENANCE && $action->id != "error" && !$adminUser){
                throw new HttpException(503, "Site is down for maintenance!", 503);
            }
            return true;
        }
    
        return false;
    }

    public function render($view, $params = [])
    {
        if(Yii::$app->request->isAjax)
            return parent::renderAjax($view, $params);
        else
            return parent::render($view, $params);
    }
    
	public function renderJsn($success = true, $message = "", $params = [])
    {
		$str = "".($success?"1":"0").",'$message',";
		if(is_array($params)) {
            for ($i = 0; $i<count($params); $i++) {
                $str .= json_encode($params[$i]).",";
            }
        }
        elseif(trim($params) != "") {
            $str .= json_encode($params).",";
        }
		$str = trim($str, ",");
		
        return "fn($str);";
    }
    
    public final function getViewName()
    {
        return $this->action->id;
    }
    
    public function ajaxRefresh(){
        return '<script type="text/javascript">window.location.reload();</script>';
    }
    
    public function ajaxRedirect($url){
        return "<script type=\"text/javascript\">window.location.href = '".$url."';</script>";
    }
       
    public function json($message, $data=array(), $code = self::JSON_SUCCESS)
    {
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $arr = array();
        $arr['code'] = (int)$code;
        $arr['message'] = $message;
        $arr['error'] = $code == self::JSON_ERROR?1:0;
        if(!empty($data)) $arr['data'] = $data;    
        return $arr;
    }
}

?>