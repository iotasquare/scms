<?php

/**
 * @copyright Copyright &copy; Shozab Hasan, shozab@isqr.io, 2018
 * @package scms
 * @version 2.0.1
 */

namespace isqr\scms;

use isqr\scms\components\SEnum;
use isqr\scms\components\SGlobal;

class AdminAsset extends SAsset
{
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    
    public function assetType(){
        return SEnum::ASSETTYPE_PLUGIN;
    }
    
    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        if(SGlobal::isBootstrap4()) {
            $this->depends[] = 'yii\bootstrap4\BootstrapAsset';
            $this->depends[] = 'yii\bootstrap4\BootstrapPluginAsset';
        }
        else {
            $this->depends[] = 'yii\bootstrap\BootstrapAsset';
            $this->depends[] = 'yii\bootstrap\BootstrapPluginAsset';
        }
        parent::init();
        $this->css[] = 'thirdparty/font-awesome/css/font-awesome.min.css';
        $this->css[] = 'thirdparty/font-awesome-5/css/all.min.css';
        $this->css[] = 'css/sgrid.css';
        $this->css[] = 'css/admin.css';
        
        $this->js[] = 'thirdparty/jqueryui/jquery-ui.min.js';
        $this->js[] = 'thirdparty/tinymce/tinymce.min.js';
        $this->js[] = 'thirdparty/moment.min.js';
        $this->js[] = 'js/sgrid.js';
        $this->js[] = 'js/common.js';
        $this->js[] = 'js/admin.js';
    }
}