<?php

use isqr\scms\components\SGridView;
use isqr\scms\widgets\adminui\box\Box;
use isqr\scms\widgets\jsregister\JSRegister;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;
use yii\widgets\Pjax;
?>
<style>
	#media-library-files{
		padding: 15px;
	}
	#media-library-files .file-item {
		width: 150px;
		display: inline-block;
		margin: 0px 0px 10px 10px;
		padding: 4px;
		vertical-align: top;
		height: 230px;
		position: relative;
    	border-radius: 0;
	}

	#media-library-files .file-item .image{
		width: 140px;
		height: 140px;
		background-position: center;
		background-size: cover;
		display: block;
		border: 1px solid #EEE;
		cursor: pointer;
		text-decoration: none;
	}
	
	#media-library-files .file-item .actionbuttons{
		position: absolute;
		bottom: 10px;
		left: 0;
		text-align: center;
		right: 0;
	}
</style>
<?php $this->addActionCustomButton(Html::a('<i class="fa fa-plus"></i> Add Media', "#", array(
	"onclick"=> "return medialibrary_uploadfile();",
	"class" => "action_button"
))); ?>
<input id="medialibrary_file" type="file" style="display: none;"/>
<div class="row">
<?php Box::begin(['size'=>12, 'title'=>'List', 'type'=>'tabular'])?>
<?php Pjax::begin(array("id"=>"pjx_media-library-list", 'enablePushState'=>false));?>
<?=ListView::widget([
	'id'=>'media-library-files',
	'dataProvider' => $dataProvider,
	'layout'=>'{summary}<div class="row">{items}</div>{pager}',
	'itemView' => '_block-file',
	'itemOptions' => ['class' => 'file-item thumbnail']
]);?>
<?php Pjax::end();?>
<?php Box::end();?>
</div>

<?php JSRegister::begin(["position" => View::POS_HEAD]);?>
<script>
function medialibrary_uploadfile(){
	jQuery("#medialibrary_file").click();
	return false;
}

function medialibrary_rename_file(id){
	var newname = prompt("Please enter new name", "");
	var url = "<?=Url::toRoute(["/scms/media-library-admin/jsn-rename"])?>";
	if (newname != null && newname != "") {
		var data = {
			id:id,
			name:newname
		}
		ajax_post(url, data, mediaUpdateOnSuccess);
	}
	return false;
}

function mediaUpdateOnSuccess(){
	$.pjax.reload({container:"#pjx_media-library-list"});
}
</script>
<?php JSRegister::end();?>

<?php JSRegister::begin(["position" => View::POS_READY]);?>
<script>
jQuery("#medialibrary_file").change(function(){
	var url = '<?=Url::toRoute('jsn-create');?>';
	var jform = new FormData();
	jform.append('SFile[file]','');
	jform.append('SFile[file]',$('#medialibrary_file').get(0).files[0]);

	ajax_post(url, jform, function(success, message) {
		mediaUpdateOnSuccess();
	});
});
</script>
<?php JSRegister::end();?>