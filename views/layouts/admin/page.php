<?php 
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->beginContent('@scms/views/layouts/admin/main.php'); ?>
	<div id="container" class="">
    	<?= $this->render('@scms/views/layouts/admin/childhtml/header');?>
    	<?= $this->render('@scms/views/layouts/admin/childhtml/sidebar');?>
    	<div id="content">
			<?php if($this->title) :?>
				<div class="page-header">
					<div class="container-fluid">
						<h1><?= $this->title;?></h1>
						<?= Breadcrumbs::widget([
								'homeLink' => [ 
									'label' =>'<i class="fa fa-home"></i> ' . Yii::t('yii', 'Home'),
									'url' => Url::toRoute('/scms/default-admin/index'),
									'encode' => false,
								],
								'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
								'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
								'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>',
							]) 
						?>
						<div class="actionbuttons">
							<?php $this->renderActionButtons(); ?>
						</div>
					</div>
				</div>
			<?php endif;?>
			<div class="container-fluid">
		    	<?= $content;?>
			</div>
		</div>
		<?php if(isset($this->blocks['searchform'])): ?>
		<div id="searchform" class="searchform">
			<div id="searchform_closebutton" class="closebutton"></div>
			<div class="form">
				<h3>Search</h3>
				<?= $this->blocks['searchform'];?>
			</div>
		</div>
		<?php endif; ?>
    	<?= $this->render('@scms/views/layouts/admin/childhtml/footer');?>
	</div>
<?php $this->endContent(); ?>
