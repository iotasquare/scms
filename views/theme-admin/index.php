<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Themes');
$this->params['breadcrumbs'][] = Yii::t('app', 'Themes');
?>
<div class="theme-index">
    <?= $this->render('_form', [
        'theme' => $theme,
        'selectedOptions'=>$selectedOptions,
        'themelist' => $themelist 
    ]) ?>
</div>
