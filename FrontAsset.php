<?php

/**
 * @copyright Copyright &copy; Shozab Hasan, shozab.com, 2014
 * @package yii2-scms
 * @version 1.0.0
 */

namespace isqr\scms;

use yii\web\AssetBundle;
use isqr\scms\components\SEnum;
use isqr\scms\components\SGlobal;
use Yii;

class FrontAsset extends SAsset
{
    public $depends = [
        'yii\web\JqueryAsset',
    ];
 
    public function assetType(){
        return SEnum::ASSETTYPE_PLUGIN;
    }
    
    public function init()
    {
        if(SGlobal::isBootstrap4()) {
            $this->depends[] = 'yii\bootstrap4\BootstrapAsset';
            $this->depends[] = 'yii\bootstrap4\BootstrapPluginAsset';
        }
        else {
            $this->depends[] = 'yii\bootstrap\BootstrapAsset';
            $this->depends[] = 'yii\bootstrap\BootstrapPluginAsset';
        }
        parent::init();
        
        $this->css[] = 'thirdparty/font-awesome/css/font-awesome.min.css';
        $this->css[] = 'thirdparty/font-awesome-5/css/all.min.css';
        $this->css[] = 'css/sgrid.css';
        $this->css[] = 'css/front.css';
        
        $this->js[] = 'thirdparty/jqueryui/jquery-ui.min.js';
        $this->js[] = 'thirdparty/moment.min.js';
        $this->js[] = 'js/sgrid.js';
        $this->js[] = 'js/common.js';
        $this->js[] = 'js/front.js';
    }
}