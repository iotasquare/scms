<?php use yii\helpers\Url;
use yii\helpers\Html;
use \isqr\scms\models\SUser;
use isqr\scms\components\SGlobal;
?>
<?php $user = SGlobal::getCurrentUser();?>
<header id="header" class="navbar navbar-static-top">
	<div class="navbar-header">
		<div id="navtogglebutton" class="navtogglebutton">
			<div class="navtogglebutton_anim">
				<span></span>
			  	<span></span>
			  	<span></span>
		  	</div>
		</div>
		<a id="navtitle" href="<?= Url::toRoute('/scms/default-admin/index');?>" class="navtitle"><?php echo SGlobal::getSetting('title'); ?></a>
	</div>
    <ul class="navbar-menu">
    	<li class="nav-item">
    		<a href="<?=Url::toRoute(["/scms/default/index", 'view'=>'front']);?>" target="_blank" class="nav-link"><span class="hidden-xs hidden-sm hidden-md"></span> <i class="fa fa-globe fa-lg"></i></a>
		</li>
    	<li class="nav-item">
    		<a href="<?=Url::toRoute(["/scms/user/logout", 'view'=>'front']);?>"  class="nav-link"><span class="hidden-xs hidden-sm hidden-md">Logout</span> <i class="fa fa-sign-out fa-lg"></i></a>
		</li>
    </ul>
</header>
