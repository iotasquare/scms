<?php
namespace isqr\scms\models;
use yii;
use yii\base\Model;
use isqr\scms\models\SUser;
use isqr\scms\components\SGlobal;

/**
 * This is the model class for Login Form.
 *
 * @property SUser $_identity
 */
class SSettingForm extends Model
{
	public function init(){
	    foreach ($this->attributes as $k=>$v)
	        $this->{$k} = SGlobal::getSetting($k);
	    parent::init();
	}
	
	public function save()
	{
	    $transaction = \Yii::$app->db->beginTransaction();	    
	    foreach ($this->attributes as $k=>$v)
	        SGlobal::setSetting($k, $v==null?"":$v);
	    $transaction->commit();
		return true;
	}
}