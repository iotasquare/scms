<?php

namespace isqr\scms\controllers;

use isqr\scms\components\SCrudController;

class PageAdminController extends  SCrudController
{
    public function getModelClass()
    {
        return 'isqr\scms\models\SPage';
    }
    
	public function getObjectTitles()
    {
		return array(
				"singular" => "Page",
				"plural" => "Pages",
		);
    }
}
