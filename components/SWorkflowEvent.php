<?php 
namespace isqr\scms\components;

use yii\base\Event;

class SWorkflowEvent extends Event
{
    public $description;
    public $department;
    public $model;
    public function __construct($config = [])
    {
        parent::__construct($config);
    }
}
