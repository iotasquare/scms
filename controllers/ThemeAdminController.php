<?php

namespace isqr\scms\controllers;

use Yii;
use isqr\scms\components\SController;
use isqr\scms\components\SGlobal;
use yii\helpers\ArrayHelper;

class ThemeAdminController extends SController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
        ]);
    }

    /**
     * Lists all SSetting models.
     * @return mixed
     */
    public function actionIndex($change = "")
    {   
        $themelist = SGlobal::getThemes();
        
        if($change == "")
            $id = SGlobal::getSetting("theme");
        else
            $id = $change;
        
        if(!isset($themelist[$id])){
            $id = "basic";
        }
        
        $theme = $themelist[$id];
        $selectedOptions = json_decode(SGlobal::getSetting("theme_options_json"), true);
        if($selectedOptions == null) 
            $selectedOptions = [
                'positions' => []
            ];
        
        if(Yii::$app->request->post()){
            $obj = [];
            if(isset($_POST['positions'])){
                $obj['positions']= $_POST['positions'];
            }
            $isSaved = false;
            if($change != "")
                $isSaved = SGlobal::setSetting("theme", $change);
            
            $isSaved = SGlobal::setSetting("theme_options_json", json_encode($obj));
            
            if($isSaved){
                Yii::$app->getSession()->setFlash('success', 'Changes are updated.');
                return $this->redirect(['index']);
            }
            else
                Yii::$app->getSession()->setFlash('failed', 'Changes are failed to be updated.');
        }
        
        return $this->render('index', [
            'themelist' => $themelist,
            'theme' => $theme,
            'selectedOptions' => $selectedOptions
        ]);
    }
}
