<?php $this->beginContent("@scms/views/layouts/main.php", ['scope'=>'user']); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
				<?= $content;?>
			</div>
		</div>
	</div>
<?php $this->endContent(); ?>