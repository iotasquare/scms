<?php
namespace isqr\scms;

use Yii;
use yii\base\Widget;
use yii\helpers\Inflector;

abstract class SWidget extends Widget
{

    const TYPE_DEFAULT = 'default';

    public $type = self::TYPE_DEFAULT;

    public $title = '';

    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();
        
        $funcName = "type" . Inflector::camelize($this->type);
        if (! method_exists($this, $funcName)) {
            $this->type = self::TYPE_DEFAULT;
            $funcName = "type" . Inflector::camelize($this->type);
        }
        
        return $this->$funcName($content);
    }
    
}

