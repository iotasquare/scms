<?php use yii\helpers\StringHelper;
use yii\helpers\Url;
$size = isset($size)?$size:12;?>
<div class="col-md-<?=$size?>">
	<div class="box <?=$boxCss?>">
		<?php if(trim($title) !== ""):?>
		<div class="box-header">			
			<h3 class="box-title"><?=$title ?></h3>
		</div>
		<?php endif;?>
		<div class="box-body">
			<?=$content;?>
		</div>
	</div>
</div>