<?php
namespace isqr\scms\models;

use Yii;
use yii\helpers\ArrayHelper;

class SPage extends IPage
{
    public $filter_name;
    public function configure()
    {
        $rules = [];
        $rules[] = [['filter_name'], 'safe'];

        $attributeLabels = [];
        $attributeLabels["filter_name"] = "Name";
        
        $hints = [];
        $hints["filter_name"] = "Title";

        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => $hints
        ]);
    }

    protected function searchQuery(){
		$query = parent::searchQuery();
		
        $query->andFilterWhere([
            't.name' => $this->filter_name,
        ]);
        
		return $query;
	}
}
