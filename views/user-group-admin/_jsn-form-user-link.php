<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use isqr\scms\models\SUserGroup;
use isqr\scms\components\SEnum;
use yii\helpers\ArrayHelper;
use isqr\scms\components\SGlobal;
use app\scms\plugins\qltslms\models\Module;
use kartik\select2\Select2;
use isqr\scms\models\SUser;
use isqr\scms\models\SUserGroupLink;

?>
<div class="form">
    <?php $form = ActiveForm::begin([
        'id' => 'module-update-form',
        'options' => [
            'method'=>'post'
        ]
    ]); ?>
    <?php 
        $subqueryUsers = "(SELECT user_id FROM ".SUserGroupLink::tableName()." AS cl 
        WHERE cl.user_group_id = $model->user_group_id)";
        $users = SUser::find()
        ->andWhere("id NOT IN $subqueryUsers")
        ->all(); 
    ?>
    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($users,'id','name'),
        'options' => ['placeholder' => 'Users', 'multiple' => false],
        'pluginOptions' => [
            'tags' => false
        ]
    ]);?>
    <?= $form->field($model, 'user_group_id')->hiddenInput()->label(false); ?>

    <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
    <?= Html::a('Cancel', '#', ['class' => 'btn btn-sm btn-default', 'data-dismiss'=>'modal']); ?>
    <?php ActiveForm::end(); ?>
</div>