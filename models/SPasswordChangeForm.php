<?php
namespace isqr\scms\models;

use isqr\scms\components\SFormModel;
use yii\base\Model;
use isqr\scms\components\SGlobal;
use yii\helpers\ArrayHelper;

class SPasswordChangeForm extends SFormModel
{
	public $currentpassword;
	public $confirmpassword;
	public $password;
	const SCENARIO_CHANGEPASSWORD = "changepassword";
	const SCENARIO_VALIDATEDCHANGEPASSWORD = "validatedchangepassword";
	
	public function configure() {
        $rules =  [
			[['password', 'confirmpassword'], 'required', 'message'=>'{attribute} is required.', 'on' => [self::SCENARIO_CHANGEPASSWORD, self::SCENARIO_VALIDATEDCHANGEPASSWORD]],
			['password', 'string', 'min'=>6, 'max'=>64, 'tooShort'=>'Password is too short (minimum is 6 characters)', 'on' => [self::SCENARIO_CHANGEPASSWORD, self::SCENARIO_VALIDATEDCHANGEPASSWORD]],
			['confirmpassword', 'isMatched', 'on' => [self::SCENARIO_CHANGEPASSWORD, self::SCENARIO_VALIDATEDCHANGEPASSWORD]],
			[['currentpassword'], 'required', 'message'=>'{attribute} is required.', 'on' => [self::SCENARIO_VALIDATEDCHANGEPASSWORD]],
			['currentpassword', 'isValid', 'on' => self::SCENARIO_VALIDATEDCHANGEPASSWORD],
        ];

        $attributeLabels =  [
			'currentpassword' => 'Current Password',
			'password' => 'Password',
			'confirmpassword' => 'Confirm Password',
        ];
        
        $hints =  [
        ];
        
        return ArrayHelper::merge(parent::configure(), [
            'rules' => $rules,
            'attributeLabels' => $attributeLabels,
            'hints' => []
        ]);
	}

	public function isMatched($attribute, $params) {
		if(!$this->hasErrors()) {
			if($this->password != $this->confirmpassword)
				$this->addError("confirmpassword", "Password mismatched.");
			else
			return true;				
		}
	}

	public function isValid($attribute, $params) {
		$user = SGlobal::getCurrentUser();
		if(!$user->validatePassword($this->currentpassword)) {
			$this->addError("currentpassword", "Please enter correct password.");
			return false;	
		}
		else
			return true;			
	}
}